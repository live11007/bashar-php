<?php
App::import('Vendor', 'tcpdf/tcpdf');
App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel/Classes/PHPExcel.php'));

class rest_rollingscissor extends RestBaseClass {

    public $uses = array(
        'Customer', 'Cmspage', 'Category', 'Appointment', 'AppointmentDetail', 'AppointmentComboService', 'OrderService', 'OrderPackage', 'AppointmentPackage', 'PackageService', 'AppointmentService', 'Service', 'ComboService', 'Schedular', 'Combo', 'User', 'Package', 'Contactemail', 'Governorate', 'Transaction', 'Area', 'Product', 'Businessrule', 'Cart', 'User', 'Order', 'Emailtemplate', 'OrderData', 'CustomerAddress', 'Area', 'Vantrack', 'VanLog', 'AppointmentRatingsQuestion', 'RatingReview'
    
	
	var $helpers = array('CSV','PhpExcel');
	
	
	
	
	
  
	
	function salesreport($api_token = '', $start_date = '', $end_date = '', $payment_method = '', $order_status = '', $booked_from = '', $service_type = '', $admin_id = '', $deviceid = '', $device_type = '') {
		
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');
		
        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }
		
		#pr($error_response);
		#die('here');
		
        $response = array()
        $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
		
		$objPHPExcel = new PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
						   ->setLastModifiedBy("Maarten Balliauw")
						   ->setTitle("PHPExcel Test Document")
						   ->setSubject("PHPExcel Test Document")
						   ->setDescription("Test document for PHPExcel, generated using PHP classes.")
						   ->setKeywords("office PHPExcel php")
						   ->setCategory("Test result file");
		
		$objPHPExcel->setActiveSheetIndex(0)
		  ->setCellValue('A1', 'Order Number')
		  ->setCellValue('B1', 'Order Date')
		  ->setCellValue('C1', 'Order Amount')
		  ->setCellValue('D1', 'Payment Method')
		  ->setCellValue('E1', 'Customer Name')
		  ->setCellValue('F1', 'Mobile')
		  ->setCellValue('G1', 'Email')
		  ->setCellValue('H1', 'Area')
		  ->setCellValue('I1', 'Electricity')
		  ->setCellValue('J1', 'Parking')
		  ->setCellValue('K1', 'Order Status')
		  ->setCellValue('L1', 'Booked From')
		  ->setCellValue('M1', 'Service Type');
		
		
		if($admin_id)
			$usrInfo = $this->User->find('first', array('conditions' => array('User.id' => $admin_id), 'fields' => array('User.email')));
		#pr($usrInfo['User']['email']); die;
		
		$conditions = $srvcId = array();
		
		if($start_date && $end_date)
			$conditions = array('Order.created BETWEEN ? and ?' => array(date('Y-m-d H:i:s', strtotime($start_date)), date('Y-m-d H:i:s', strtotime($end_date))));
			
		if($payment_method)
			$conditions = array_merge($conditions, array('Order.payment_method' => $payment_method));
			
		if($order_status)
			$conditions = array_merge($conditions, array('Order.status' => $order_status));
			
		if($booked_from == 'mobileapp')
			$conditions = array_merge($conditions, array('Order.admin_id' => 0));
		else if($booked_from == 'adminapp')
			$conditions = array_merge($conditions, array('Order.admin_id !=' => 0));
		
		#pr($conditions); #die;
		
		#$this->Order->recursive = 0;
		$orders = $this->Order->find('all', array('conditions' => $conditions));
		#pr($orders); die;
		
		
        if (count($orders) <= 0) {
            $response['status'] = 1;
            $response['message'] = 'No records found';
            $response['message_ar'] = 'No records found';
            return $this->fail_safe_return($response);
        }
		
		
		$i = 2;
		foreach($orders as $k => $v):
			#pr($v);
			
			
			// Logic to check Service type = G / Y / Both
			$srvcId = array();
			$apptDetails = $this->AppointmentService->find('all', array('fields' => array(), 'conditions' => array('AppointmentService.appointment_id' => $v['Order']['appointment_id'])));
			#pr($apptDetails);
			
			foreach($apptDetails as $k1 => $v1):
				$srvcId[] = $v1['AppointmentService']['service_id'];
			endforeach;
			#pr($srvcId);
			
			$key = array_search (20, $srvcId);
			
			if($key === 0 && count($srvcId) > 1)
				$stype = 'Both';
			else if($key === 0 && count($srvcId) < 2)
				$stype = 'Young Gentleman';
			else
				$stype = 'Gentleman';
			
			
			$this->CustomerAddress->recursive = -1;
			$ordersAddrs = $this->CustomerAddress->find('all', array('fields' => array(), 'conditions' => array('CustomerAddress.id' => $v['Order']['customer_address_id'])));
			#pr($ordersAddrs);
			$this->Area->recursive = -1;
			$ordersArea = $this->Area->find('all', array('fields' => array(), 'conditions' => array('Area.id' => $v['Order']['area_id'])));
			if(!empty($ordersArea)){
				$ordersArea = $ordersArea[0]['Area']['areaname'];
			}else{
				$ordersArea ="";
			}
			/*pr($ordersArea);
			die();*/
			
			if($v['Order']['status'] == 1)
				$orst = 'Created';
			else if($v['Order']['status'] == 2)
				$orst = 'Cancelled';
			else if($v['Order']['status'] == 3)
				$orst = 'Completed';
			else
				$orst = 'Deleted';
			
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$i, $v['Order']['id'])
				->setCellValue('B'.$i, date('d.m.Y', strtotime($v['Order']['created'])))
				->setCellValue('C'.$i, $v['Order']['total'])
				->setCellValue('D'.$i, ($v['Order']['payment_method'] == 1 ? 'Cash on delivery' : 'Knet'))
				->setCellValue('E'.$i, $v['Customer']['f_name'].' '.$v['Customer']['l_name'])
				->setCellValue('F'.$i, $v['Customer']['mobile'])
				->setCellValue('G'.$i, $v['Customer']['email'])
				->setCellValue('H'.$i, $ordersArea)
				->setCellValue('I'.$i, @$ordersAddrs[0]['CustomerAddress']['electric'])
				->setCellValue('J'.$i, @$ordersAddrs[0]['CustomerAddress']['parking'])
				->setCellValue('K'.$i, $orst)
				->setCellValue('L'.$i, ($v['Order']['admin_id'] > 0 ? 'Admin app' : 'Mobile app'))
				->setCellValue('M'.$i, $stype);
			
			$i++;
			
		endforeach;
		
		#die;
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		#$objWriter->save( $folderToSaveXls . '/RS_Sales_Report_'.date('his').'.xlsx' );
		#echo SITE_ROOT;
		#echo '<br>';
		$objWriter->save( SITE_ROOT . '/RS_Sales_Report.xlsx' );
		#die('here!');
		#$this->autoRender = false;
		
		
		// Email sending logic goes here
		$message = 'Hi,
					<br /><br />
					Thank you for reports request.
					<br /><br />
					Rolling Scissor TEAM.
					';
					
		$email = !empty($usrInfo['User']['email']) ? $usrInfo['User']['email'] : 'adityabhai@gmail.com';
		$attachment1 = '';
		$attachment2 = '';
		
		// email configuration
		$Email = new CakeEmail('smtp');
		$mailSend = $Email->from(array('no-reply@rollingscissors.com' => 'Rolling Scissor App'))
			->sender('generalmanager@rollingscissors.com', 'Rolling Scissor App')
			->to(array($email))
			->bcc(array('aditya.simplifiedinformatics@gmail.com', 'yms80@live.com', 'generalmanager@rollingscissors.com', 'partner1@rollingscissors.com'))
			->subject('RS Reports')
			->emailFormat('both')
			->attachments(array(SITE_ROOT . '/RS_Sales_Report.xlsx'))
			->send($message);
			
		/*$mailSend = $Email->from(array('no-reply@rollingscissors.com' => 'Rolling Scissor App'))
			->sender('alpesh.jscope@gmail.com', 'Rolling Scissor App')
			->to(array($email))
			->bcc(array('dhaval.jscope@gmail.com', 'bhavin.jscope@gmail.com'))
			->subject('RS Reports')
			->emailFormat('both')
			->attachments(array(SITE_ROOT . '/RS_Sales_Report.xlsx'))
			->send($message);
			
		$u_email = 'alpesh.jscope@gmail.com';
		$mailSend = $Email->from(array('no-reply@rollingscissors.com' => 'Rolling Scissor App'))
			->sender('generalmanager@rollingscissors.com', 'Rolling Scissor App')
			->to(array($u_email))
			->bcc(array('dhaval.jscope@gmail.com', 'bhavin.jscope@gmail.com'))
			->subject('RS Reports')
			->emailFormat('both')
			->attachments(array(SITE_ROOT . '/RS_Sales_Report.xlsx'))
			->send($message);*/	
			
		if($mailSend){
			
			$response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
			$response['message'] = $GLOBALS["Webservice"]["messages"]['report_email_sent'];
			$response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['report_email_sent'];
			$response['data'] = '';
			
        } else {
			
			$response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
			$response['message'] = $GLOBALS["Webservice"]["messages"]['data_defination_error'];
			$response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['data_defination_error'];
			$response['data'] = '';
			
        }
        return $this->fail_safe_return($response);
		
    }

    function salesreport_draft($api_token = '', $start_date = '', $end_date = '', $payment_method = '', $order_status = '', $booked_from = '', $service_type = '', $admin_id = '', $deviceid = '', $device_type = '') {
		
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');
		
        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }
		
		#pr($error_response);
		#die('here');
		
        $response = array();
        $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
		
		$objPHPExcel = new PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
						   ->setLastModifiedBy("Maarten Balliauw")
						   ->setTitle("PHPExcel Test Document")
						   ->setSubject("PHPExcel Test Document")
						   ->setDescription("Test document for PHPExcel, generated using PHP classes.")
						   ->setKeywords("office PHPExcel php")
						   ->setCategory("Test result file");
		
		$objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
		$logo = $this->webroot.'files/rslogo.png';
        $objDrawing->setPath($logo);
        $objDrawing->setCoordinates('E1');
        $objDrawing->setOffsetX(210);
		$objDrawing->setOffsetY(10);
        $objDrawing->setHeight(80);
        $objDrawing->setWidth(150);
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
		$objWorksheet = $objPHPExcel->getActiveSheet();
        $objWorksheet->mergeCells('A1:M3');
		
		$objPHPExcel->setActiveSheetIndex(0)
		  ->setCellValue('A4', 'Total Booking');
		$objPHPExcel->setActiveSheetIndex(0)
		  ->setCellValue('B4', 'Manual Booking');
		$objPHPExcel->setActiveSheetIndex(0)
		  ->setCellValue('C4', 'Mobile App Bookings');
		$objPHPExcel->setActiveSheetIndex(0)
		  ->setCellValue('D4', 'Total Amount');
		$objPHPExcel->setActiveSheetIndex(0)
		  ->setCellValue('E4', 'Most used Service');
		$objPHPExcel->getActiveSheet()->getStyle("A4:E4")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A4:E4")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle("A4:E4")->getFill()->getStartColor()->setARGB('d8d8d8');
		
		
		$objPHPExcel->setActiveSheetIndex(0)
		  ->setCellValue('A7', 'Order Number')
		  ->setCellValue('B7', 'Order Date')
		  ->setCellValue('C7', 'Order Amount')
		  ->setCellValue('D7', 'Payment Method')
		  ->setCellValue('E7', 'Customer Name')
		  ->setCellValue('F7', 'Mobile')
		  ->setCellValue('G7', 'Email')
		  ->setCellValue('H7', 'Area')
		  ->setCellValue('I7', 'Electricity')
		  ->setCellValue('J7', 'Parking')
		  ->setCellValue('K7', 'Order Status')
		  ->setCellValue('L7', 'Booked From')
		  ->setCellValue('M7', 'Service Type');
		$objPHPExcel->getActiveSheet()->getStyle("A7:M7")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A7:M7")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle("A7:M7")->getFill()->getStartColor()->setARGB('d8d8d8');
		
		
		if($admin_id)
			$usrInfo = $this->User->find('first', array('conditions' => array('User.id' => $admin_id), 'fields' => array('User.email')));
		#pr($usrInfo['User']['email']); die;
		
		$conditions = $srvcId = array();
		
		if($start_date && $end_date)
			$conditions = array('Order.created BETWEEN ? and ?' => array(date('Y-m-d H:i:s', strtotime($start_date)), date('Y-m-d H:i:s', strtotime($end_date))));
			
		if($payment_method)
			$conditions = array_merge($conditions, array('Order.payment_method' => $payment_method));
			
		if($order_status)
			$conditions = array_merge($conditions, array('Order.status' => $order_status));
			
		if($booked_from == 'mobileapp')
			$conditions = array_merge($conditions, array('Order.admin_id' => 0));
		else if($booked_from == 'adminapp')
			$conditions = array_merge($conditions, array('Order.admin_id !=' => 0));
		
		#pr($conditions); #die;
		
		#$this->Order->recursive = 0;
		$orders = $this->Order->find('all', array('conditions' => $conditions));
		#pr($orders); die;
		
		
        if (count($orders) <= 0) {
            $response['status'] = 1;
            $response['message'] = 'No records found';
            $response['message_ar'] = 'No records found';
            return $this->fail_safe_return($response);
        }
		
		
		$i = 8;
		$total_order_amount = 0;
		$total_booking = 0;
		$total_mobile_booking = 0;
		$total_manual_booking = 0;
		$appointment_ids=array();
		foreach($orders as $k => $v):
			#pr($v);
			$appointment_ids[]=$v['Order']['appointment_id'];
			
			// Logic to check Service type = G / Y / Both
			$srvcId = array();
			$apptDetails = $this->AppointmentService->find('all', array('fields' => array(), 'conditions' => array('AppointmentService.appointment_id' => $v['Order']['appointment_id'])));
			#pr($apptDetails);
			
			foreach($apptDetails as $k1 => $v1):
				$srvcId[] = $v1['AppointmentService']['service_id'];
			endforeach;
			#pr($srvcId);
			
			$key = array_search (20, $srvcId);
			
			if($key === 0 && count($srvcId) > 1)
				$stype = 'Both';
			else if($key === 0 && count($srvcId) < 2)
				$stype = 'Young Gentleman';
			else
				$stype = 'Gentleman';
			
			#echo 'key='.$key.'<br>';
			#Young Gentleman
			#Gentleman
			#Both
			// Logic to check Service type = G / Y / Both
			
			#pr($v);
			$this->CustomerAddress->recursive = -1;
			$ordersAddrs = $this->CustomerAddress->find('all', array('fields' => array(), 'conditions' => array('CustomerAddress.id' => $v['Order']['customer_address_id'])));
			#pr($ordersAddrs);
			$this->Area->recursive = -1;
			$ordersArea = $this->Area->find('all', array('fields' => array(), 'conditions' => array('Area.id' => $v['Order']['area_id'])));
			if(!empty($ordersArea)){
				$ordersArea = $ordersArea[0]['Area']['areaname'];
			}else{
				$ordersArea ="";
			}
			/*pr($ordersArea);
			die();*/
			
			if($v['Order']['status'] == 1)
				$orst = 'Created';
			else if($v['Order']['status'] == 2)
				$orst = 'Cancelled';
			else if($v['Order']['status'] == 3)
				$orst = 'Completed';
			else
				$orst = 'Deleted';
			
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$i, $v['Order']['id'])
				->setCellValue('B'.$i, date('d.m.Y', strtotime($v['Order']['created'])))
				->setCellValue('C'.$i, $v['Order']['total'])
				->setCellValue('D'.$i, ($v['Order']['payment_method'] == 1 ? 'Cash on delivery' : 'Knet'))
				->setCellValue('E'.$i, $v['Customer']['f_name'].' '.$v['Customer']['l_name'])
				->setCellValue('F'.$i, $v['Customer']['mobile'])
				->setCellValue('G'.$i, $v['Customer']['email'])
				->setCellValue('H'.$i, $ordersArea)
				->setCellValue('I'.$i, @$ordersAddrs[0]['CustomerAddress']['electric'])
				->setCellValue('J'.$i, @$ordersAddrs[0]['CustomerAddress']['parking'])
				->setCellValue('K'.$i, $orst)
				->setCellValue('L'.$i, ($v['Order']['admin_id'] > 0 ? 'Admin app' : 'Mobile app'))
				->setCellValue('M'.$i, $stype);
			
			$i++;

			$total_booking = $total_booking+1;
			if($v['Order']['admin_id'] > 0)
				$total_manual_booking = $total_manual_booking+1;
			else
				$total_mobile_booking = $total_mobile_booking+1;

			$total_order_amount = $total_order_amount+$v['Order']['total'];
			
		endforeach;

		#pr($appointment_ids);
		$data = $this->AppointmentService->find('all', array(
					'joins' => array(
						array(
							'alias' => 'Service',
							'table' => 'services',
							#'type' => 'RIGHT JOIN',
							'foreignKey' => false,
							'conditions' => array('Service.id = AppointmentService.service_id'),
						),
					),
				    'conditions' => array('AppointmentService.appointment_id' => $appointment_ids),
				    'fields' => array(
				        'AppointmentService.service_id','Service.name',
				        'COUNT(AppointmentService.service_id) AS value_occurrence'
				    ),
				    'group' => 'AppointmentService.service_id',
				    'order' => array('value_occurrence' => 'DESc'),
				    'limit' => 1
				));
		if(!empty($data))
			$mostUseService = $data[0]['Service']['name'];
		else
			$mostUseService="";

		/*$apptDetails = $this->AppointmentService->find('all', 
			array('fields' => array('service_id'), 
				'conditions' => array('AppointmentService.appointment_id' => $appointment_ids)
			));
				pr($apptDetails);
				die();
		die();*/

		$objPHPExcel->setActiveSheetIndex(0)
		  ->setCellValue('A5', $total_booking);
		$objPHPExcel->setActiveSheetIndex(0)
		  ->setCellValue('B5', $total_manual_booking);
		$objPHPExcel->setActiveSheetIndex(0)
		  ->setCellValue('C5', $total_mobile_booking);
		$objPHPExcel->setActiveSheetIndex(0)
		  ->setCellValue('D5', $total_order_amount);
		$objPHPExcel->setActiveSheetIndex(0)
		  ->setCellValue('E5', $mostUseService);
		
		#die;
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		#$objWriter->save( $folderToSaveXls . '/RS_Sales_Report_'.date('his').'.xlsx' );
		#echo SITE_ROOT;
		#echo '<br>';
		$objWriter->save( SITE_ROOT . '/RS_Sales_Report.xlsx' );
		#die('here!');
		#$this->autoRender = false;
		
		
		// Email sending logic goes here
		$message = 'Hi,
					<br /><br />
					Thank you for reports request.
					<br /><br />
					Rolling Scissor TEAM.
					';
					
		$email = !empty($usrInfo['User']['email']) ? $usrInfo['User']['email'] : 'adityabhai@gmail.com';
		$attachment1 = '';
		$attachment2 = '';
		
		
		/*$Email = new CakeEmail('smtp');
		
		$u_email = 'vishal.jscope@gmail.com';
		$mailSend = $Email->from(array('no-reply@rollingscissors.com' => 'Rolling Scissor App'))
			->sender('generalmanager@rollingscissors.com', 'Rolling Scissor App')
			->to(array($u_email))
			->bcc(array('dhaval.jscope@gmail.com', 'bhavin.jscope@gmail.com'))
			->subject('RS Reports')
			->emailFormat('both')
			->attachments(array(SITE_ROOT . '/RS_Sales_Report.xlsx'))
			->send($message); */
			
		// email configuration	
		$Email = new CakeEmail('smtp');
		$mailSend = $Email->from(array('no-reply@rollingscissors.com' => 'Rolling Scissor App'))
			->sender('generalmanager@rollingscissors.com', 'Rolling Scissor App')
			->to(array($email))
			->bcc(array('aditya.simplifiedinformatics@gmail.com', 'yms80@live.com', 'generalmanager@rollingscissors.com', 'partner1@rollingscissors.com'))
			->subject('RS Reports')
			->emailFormat('both')
			->attachments(array(SITE_ROOT . '/RS_Sales_Report.xlsx'))
			->send($message);		
			
		if($mailSend){
			
			$response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
			$response['message'] = $GLOBALS["Webservice"]["messages"]['report_email_sent'];
			$response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['report_email_sent'];
			$response['data'] = '';
			
        } else {
			
			$response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
			$response['message'] = $GLOBALS["Webservice"]["messages"]['data_defination_error'];
			$response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['data_defination_error'];
			$response['data'] = '';
			
        }
        return $this->fail_safe_return($response);
		
    }
	
	
	function customerreport($api_token = '', $start_date = '', $end_date = '', $electricity = '', $parking = '', $admin_id = '', $deviceid = '', $device_type = '') {
		
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');
		
        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }
		
		#pr($error_response);
		#die('here');
		
        $response = array();
        #$response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
		
		$folderToSaveXls = $_SERVER['DOCUMENT_ROOT'].'rollingscissor/admin/app/webroot/';
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
						   ->setLastModifiedBy("Maarten Balliauw")
						   ->setTitle("PHPExcel Test Document")
						   ->setSubject("PHPExcel Test Document")
						   ->setDescription("Test document for PHPExcel, generated using PHP classes.")
						   ->setKeywords("office PHPExcel php")
						   ->setCategory("Test result file");
		
		$objPHPExcel->setActiveSheetIndex(0)
		  ->setCellValue('A1', 'Customer ID')
		  ->setCellValue('B1', 'Customer Name')
		  ->setCellValue('C1', 'Mobile')
		  ->setCellValue('D1', 'Email')
		  ->setCellValue('E1', 'Area')
		  ->setCellValue('F1', 'Total Appointments')
		  ->setCellValue('G1', 'Total Payment')
		  ->setCellValue('H1', 'Electricity')
		  ->setCellValue('I1', 'Parking Roof');
		
		if($admin_id)
			$usrInfo = $this->User->find('first', array('conditions' => array('User.id' => $admin_id), 'fields' => array('User.email')));
		#pr($usrInfo['User']['email']); die;
		
		
		$conditions = array();
		
		if($start_date && $end_date)
			$conditions = array('Customer.created BETWEEN ? and ?' => array(date('Y-m-d H:i:s', strtotime($start_date)), date('Y-m-d H:i:s', strtotime($end_date))));
		if($electricity)
			$conditions = array_merge($conditions, array('CustomerAddress.electric' => $electricity));
			
		if($parking)
			$conditions = array_merge($conditions, array('CustomerAddress.parking' => $parking));
		#pr($conditions); #die;
		
		
		$orders = $this->Customer->find('all', array(
			'joins' => array(
				array(
					'alias' => 'CustomerAddress',
					'table' => 'customer_addresses',
					#'type' => 'RIGHT JOIN',
					'foreignKey' => false,
					'conditions' => array('CustomerAddress.customer_id = Customer.id'),
				),
			),
			'conditions' => $conditions,
			'fields' => array('Customer.id', 'Customer.f_name', 'Customer.l_name', 'Customer.mobile', 'Customer.email', 'CustomerAddress.electric', 'CustomerAddress.parking', 'CustomerAddress.customer_id'),
			'group' => 'Customer.id'
		));
		
		
		#echo '<pre>';
		#$orders = $this->Customer->CustomerAddress->find('all', array('recursive' => 1, 'conditions' => $conditions));
		#print_r($orders); 
		#die;
		
        if (count($orders) <= 0) {
            $response['status'] = 1;
            $response['message'] = 'No records found';
            $response['message_ar'] = 'No records found';
            return $this->fail_safe_return($response);
        }
		
		
		#$orders = $this->Customer->find('all', array('fields' => array(), 'conditions' => array()));
		
		#print_r($orders);
		#die;
		#$customerschk = array();
		$i = 2;
		foreach($orders as $k => $v):
			
			/*$customerschk[] = $v['Customer']['id'];
			print_r($customerschk);
			
			if(in_array($v['Customer']['id'], $customerschk)){
				#echo 'here now'.$v['Customer']['id'];
				continue;
			}
				
				
			pr($v);*/
			
			#$this->Order->recursive = -1;
			$ordersAddrs = $this->Order->find('all', array('group' => array('Order.customer_id'), 'fields' => array('count(Order.id) AS orders', 'SUM(Order.total) AS ordpays'), 'conditions' => array('Order.customer_id' => $v['Customer']['id'])));
			#pr($ordersAddrs);
			
			#$this->Area->recursive = -1;
			#$ordersArea = $this->Area->find('all', array('fields' => array(), 'conditions' => array('Area.id' => $v['Order']['area_id'])));
			#pr($ordersArea);
			
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$i, $v['Customer']['id'])
				->setCellValue('B'.$i, $v['Customer']['f_name'].' '.$v['Customer']['l_name'])
				->setCellValue('C'.$i, $v['Customer']['mobile'])
				->setCellValue('D'.$i, $v['Customer']['email'])
				->setCellValue('E'.$i, @$v['CustomerAddress'][0]['areaname'])
				->setCellValue('F'.$i, @$ordersAddrs[0][0]['orders'])
				->setCellValue('G'.$i, @$ordersAddrs[0][0]['ordpays'])
				->setCellValue('H'.$i, @$v['CustomerAddress'][0]['electric'])
				->setCellValue('I'.$i, @$v['CustomerAddress'][0]['parking']);
			
			$i++;
			
		endforeach;
		
		#die('hey');
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		#$objWriter->save( $folderToSaveXls . '/RS_Sales_Report_'.date('his').'.xlsx' );
		$objWriter->save( SITE_ROOT . '/RS_Customer_Report.xlsx' );
		#die('here!');
		#$this->autoRender = false;
		#die;
		
		
		// Email sending logic goes here
		$message = 'Hi,
					<br /><br />
					Thank you for reports request.
					<br /><br />
					Rolling Scissor TEAM.
					';
					
		$email = !empty($usrInfo['User']['email']) ? $usrInfo['User']['email'] : 'adityabhai@gmail.com';
		$attachment1 = '';
		$attachment2 = '';
		
		// email configuration
		$Email = new CakeEmail('smtp');
		$mailSend = $Email->from(array('no-reply@rollingscissors.com' => 'Rolling Scissor App'))
			->sender('generalmanager@rollingscissors.com', 'Rolling Scissor App')
			->to(array($email))
			->bcc(array('aditya.simplifiedinformatics@gmail.com', 'yms80@live.com', 'generalmanager@rollingscissors.com', 'partner1@rollingscissors.com'))
			->subject('RS Reports')
			->emailFormat('both')
			->attachments(array(SITE_ROOT . '/RS_Customer_Report.xlsx'))
			->send($message);
			
			
		if($mailSend){
			
			$response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
			$response['message'] = $GLOBALS["Webservice"]["messages"]['report_email_sent'];
			$response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['report_email_sent'];
			$response['data'] = '';
			
        } else {
			
			$response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
			$response['message'] = $GLOBALS["Webservice"]["messages"]['data_defination_error'];
			$response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['data_defination_error'];
			$response['data'] = '';
			
        }
        return $this->fail_safe_return($response);
		
    }
	
	
	
    public function get_random_token() {
        return md5(microtime());
    }

    protected function generate_user_auth_token($customerid = 0) {

        $options = array();
        $options['fields'] = array('Customer.api_auth_token');
        $options['conditions'] = array('Customer.id' => $customerid);

        $user_data = $this->Customer->find('first', $options);
        if (!empty($user_data)) {
            if (empty($user_data['Customer']['api_auth_token'])) {
                $encrypted_auth_token = $this->get_random_token();
                $update = array('id' => $customerid, 'api_auth_token' => $encrypted_auth_token);
                $this->Customer->save($update);
            } else {
                $encrypted_auth_token = $user_data['Customer']['api_auth_token'];
            }
            return $encrypted_auth_token;
        }
        return '';
    }

    function check_mobile_verification($api_token = '', $deviceid = '', $device_type = '', $mobile = '') {

        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $response = array();
        $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];

        $customerData = $this->Customer->find('first', array('conditions' => array('Customer.mobile' => $mobile)));

        if (isset($customerData['Customer']['id']) && $customerData['Customer']['id'] > 0) {

            $response_data = $customerData;
            $response_data['success'] = "1";
            $response_data['is_blocked'] = $customerData['Customer']['status'];
        } else {
            $response_data['success'] = "0";
        }

        if (!empty($response_data)) {
            if ($response_data['success'] == "1") {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                    'data' => $response_data
                );
            } else {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['no_customer_found'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_customer_found'],
                    'data' => $response_data
                );
            }
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }



        return $this->fail_safe_return($response);
    }

    function signup($api_token, $f_name, $l_name, $email, $password, $type, $dob, $mobile, $deviceid, $device_type, $address) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $isBlockOrNot = $this->Customer->find('count', array('conditions' => array('Customer.mobile' => $mobile, 'Customer.email' => $email, 'Customer.status' => '2')));

        if ($isBlockOrNot > 0) {
            $response['status'] = "1";
            $response['message'] = $GLOBALS["Webservice"]["messages"]['customer_blocked'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['customer_blocked'];
            return $this->fail_safe_return($response);
        }

        $checkMobile = $this->Customer->find('count', array('conditions' => array('Customer.mobile' => $mobile)));

        if ($checkMobile == 0) {
            if (!empty($email)) {
                $checkemail = $this->Customer->find('count', array('conditions' => array('Customer.email' => $email)));
            } else {
                $checkemail = 0;
            }

            if ($checkemail == 0) {
                $activation_hash = $this->get_unique_name();
                $activation_url = SITE_URL . 'customers/verify/' . $activation_hash;
                $response = array();
                $data = array();

                $user_registration = array();
                $user_registration['f_name'] = $f_name;
                $user_registration['l_name'] = $l_name;
                if (!empty($dob)) {
                    $user_registration['dob'] = date('Y-m-d', strtotime($dob));
                } else {
                    $user_registration['dob'] = '0000-00-00';
                }
                $user_registration['mobile'] = $mobile;
                $user_registration['user_type'] = $type;
                if (!empty($email)) {
                    $user_registration['email'] = $email;
                }
                if (!empty($password)) {
                    $user_registration['password'] = $password;
                } else {
                    $user_registration['password'] = "";
                }
                $user_registration['info'] = $password;
                $user_registration['status'] = 1;
                $user_registration['deviceid'] = $deviceid;
                $user_registration['device_os'] = $device_type;
                $user_registration['activation_hash'] = $activation_hash;


                $passed_array = array();
                $user_registration = $this->security_save($user_registration, $passed_array);

                if ($this->Customer->save($user_registration)) {

                    $address = json_decode($address);

                    if (is_array($address) && count($address) > 0) {
                        foreach ($address as $key => $value) {

                            if ($value->area_id > 0) {
                                $this->CustomerAddress->create();

                                // showing Area name for en / ar by Aditya
                                $areaData = $this->Area->find('first', array('conditions' => array('Area.id' => $value->area_id)));
                                #echo '<pre>';
                                #print_r($user_registration);
                                #print_r($areaData);
                                #die;

                                $addAddress['areaname'] = $areaData['Area']['areaname'];
                                $addAddress['areaname_ar'] = $areaData['Area']['areaname_ar'];

                                $addAddress['parking'] = (isset($value->parking))?$value->parking:"No";
                                $addAddress['electric'] =(isset($value->electric))?$value->electric:"No";
                                $addAddress['block'] = $value->block;
                                $addAddress['street'] = $value->street;
                                $addAddress['floor'] = $value->floor;
                                $addAddress['area_id'] = $value->area_id;
                                $addAddress['building'] = $value->building;
                                $addAddress['latitude'] = $value->latitude;
                                $addAddress['longitude'] = $value->longitude;
                                $addAddress['unit_number'] = $value->unit_number;
                                $addAddress['customer_id'] = $this->Customer->id;
                                $this->CustomerAddress->save($addAddress);
                            }
                        }
                    }

                    if ($type == 1) {

                        $options['conditions'] = array(
                            "Customer.id" => $this->Customer->id,
                        );
                        $check_user = $this->Customer->find('first', $options);
                        $response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
                        $response['message'] = $GLOBALS["Webservice"]["messages"]['registration_success'];
                        $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['registration_success'];
                        $response['data'] = $check_user;
                    } else {

                        $options['conditions'] = array(
                            "Customer.id" => $this->Customer->id,
                        );
                        $check_user = $this->Customer->find('first', $options);
                        if ($type == 2) {
                            $cartData = $this->AppointmentDetail->find('first', array('conditions' => array('AppointmentDetail.customer_id' => $deviceid)));
                            if (isset($cartData['AppointmentDetail']) && !empty($cartData['AppointmentDetail'])) {
                                $this->AppointmentDetail->updateAll(array('AppointmentDetail.customer_id' => $this->Customer->id), array('AppointmentDetail.customer_id' => $deviceid));
                            }
                        }

                        $response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
                        $response['message'] = $GLOBALS["Webservice"]["messages"]['nonmember_success'];
                        $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['nonmember_success'];
                        $response['data'] = $check_user;
                    }


                    // Thanks EMAIL for successfull Registration
                    if (!empty($email) && $type == 1) {
                        /*$message = 'Hi ' . ucfirst($f_name . ' ' . $l_name) . ',
                                <br /><br />
                                Thank you for registering with Rolling Scissor APP.
                               
                                <br /><br />
                                Please click on the link to activate you account. <a href="' . $activation_url . '">Activate Account</a>

                                <br /><br />
                                We hope you enjoy using our application.
                                <br /><br />
                                Good luck.
                                <br />
                                Rolling Scissor TEAM.
                                
                                <br />';*/
						$message = 'Hi ' . ucfirst($f_name . ' ' . $l_name) . ',
                                <br /><br />
                                Thank you for registering with Rolling Scissor APP.                             
                               
                                <br /><br />
                                We hope you enjoy using our application.
                                <br /><br />
                                Good luck.
                                <br />
                                Rolling Scissor TEAM.
                                
                                <br />';

                        $this->sendEmail($message, $email, 'Welcome To Rolling Scissor APP');
                    }
                }
            } else {
				
                $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                $response['message'] = $GLOBALS["Webservice"]["messages"]['email_address_alreay_exists'];
                $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['email_address_alreay_exists'];
            }
        } else {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
            $response['message'] = $GLOBALS["Webservice"]["messages"]['mobile_alreay_exists'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['mobile_alreay_exists'];
        }
        return $this->fail_safe_return($response);
    }

    function login($api_token = '', $email = '', $password = '', $deviceid = '', $device_type = '') {
        // $this->request->data['pass'] = 'Customer';

        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $response = array();
        $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];

        if ((empty($email) && empty($password)) || empty($email) || empty($password)) {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
            $response['message'] = $GLOBALS["Webservice"]["messages"]['auth_Empty_error'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['auth_Empty_error'];
            return $this->fail_safe_return($response);
        }

        //$this->Customer->validate = $this->Customer->custom_auth_login;
        if ($this->Customer->validates()) {

            App::uses('Sha256PasswordHasher', 'Controller/Component');
            $passwordHasher = new Sha256PasswordHasher();
            $password = $passwordHasher->hash($password);
            unset($passwordHasher);


            $this->Customer->unbindModel(
                    array('hasMany' => array('Appid', 'Favorite', 'Property', 'Search'))
            );

            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $checkvariable = 'email';
                $options['conditions'] = array(
                    "Customer.email" => $email,
                    'Customer.password' => $password,
                );
            } else {
                $checkvariable = 'mobile';
                $options['conditions'] = array(
                    "Customer.mobile" => $email,
                    'Customer.password' => $password
                );
            }
            #print_r($options);
            $check_user = $this->Customer->find('first', $options);

            #echo '<pre>';
            #print_r($check_user);
            #exit;

            if (!empty($check_user['Customer']['id'])) {

                $cartData = $this->AppointmentDetail->find('first', array('conditions' => array('AppointmentDetail.customer_id' => $deviceid)));

                if (isset($cartData['AppointmentDetail']) && !empty($cartData['AppointmentDetail'])) {
                    $this->AppointmentDetail->updateAll(array('AppointmentDetail.customer_id' => $check_user['Customer']['id']), array('AppointmentDetail.customer_id' => $deviceid));
                }

                if ($check_user['Customer']['status'] == 2) {
                    $response['status'] = "1";
                    $response['message'] = $GLOBALS["Webservice"]["messages"]['customer_blocked'];
                    $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['customer_blocked'];
                } else if ($check_user['Customer']['status'] == 0 && $checkvariable == 'email') {
                    $response['status'] = "1";
                    $response['message'] = $GLOBALS["Webservice"]["messages"]['account_not_verified'];
                    $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['account_not_verified'];
                } else {
                    $customerid = $check_user['Customer']['id'];
                    $response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
                    $response['message'] = $GLOBALS["Webservice"]["messages"]['auth_success'];
                    $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['auth_success'];
                    $response['data'] = $check_user;
                }
            } else {
                if ($checkvariable == 'email') {
                    $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                    $response['message'] = $GLOBALS["Webservice"]["messages"]['email_not_registered'];
                    $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['email_not_registered'];
                } else {
                    $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                    $response['message'] = $GLOBALS["Webservice"]["messages"]['mobile_not_registered'];
                    $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['mobile_not_registered'];
                }
            }
        } else {
            $response['message'] = $this->setServerError($this->Customer->validationErrors);
            $response['message_ar'] = ERROR_INPUT_DATA_AR;
            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
            if ($response['message'] == '') {
                $response['message'] = $GLOBALS["Webservice"]["messages"]['auth_error'];
            }
            //$response['error_info'] = $this->Customer->validationErrors;
        }
        // print_r($response);exit;
        $response = $this->setEmptyStringSimple($response);

        return $this->fail_safe_return($response);
    }

    public function get_customer_detail($api_token, $device_type, $user_id) {
        $deviceid = '1';
        $api_auth_token = 'skip_auth_token';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $user_id, $api_auth_token);

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        if (!$this->Customer->exists($user_id)) {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
            $response['message'] = $GLOBALS["Webservice"]["messages"]['invalid_user'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['invalid_user'];
            return $this->fail_safe_return($response);
        }




        $check_user = $this->Customer->findById($user_id);

        //print_r($check_user);exit;

        if (isset($check_user['Customer']['id']) && $check_user['Customer']['id'] > 0 && $check_user['Customer']['status'] == 2) {
            $response['status'] = "1";
            $response['is_blocked'] = "2";
            $response['message'] = $GLOBALS["Webservice"]["messages"]['customer_blocked'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['customer_blocked'];
            return $this->fail_safe_return($response);
        }
        if (is_array($check_user['CustomerAddress']) && count($check_user['CustomerAddress']) > 0) {
            foreach ($check_user['CustomerAddress'] as $key => $value) {
                $areaData = $this->Area->find('first', array('conditions' => array('Area.id' => $value['area_id'])));
                $check_user['CustomerAddress'][$key]['areaname_ar'] = $areaData['Area']['areaname_ar'];
                $check_user['CustomerAddress'][$key]['areaname'] = $areaData['Area']['areaname'];

                $check_user['CustomerAddress'] = $check_user['CustomerAddress'];
            }
        }
        $response['is_blocked'] = "1";
        $response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
        $response['message'] = $GLOBALS["Webservice"]["messages"]['data_defination_success'];
        $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['data_defination_success'];
        $response['data'] = $check_user;
        $response = $this->setEmptyStringSimple($response);

        return $this->fail_safe_return($response);
    }
	
	public function get_customer_detail_v2($api_token, $device_type, $user_id,$order_id) {
        $deviceid = '1';
        $api_auth_token = 'skip_auth_token';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $user_id, $api_auth_token);

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        if (!$this->Customer->exists($user_id)) {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
            $response['message'] = $GLOBALS["Webservice"]["messages"]['invalid_user'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['invalid_user'];
            return $this->fail_safe_return($response);
        }




        $check_user = $this->Customer->findById($user_id);

        //print_r($check_user);exit;

        if (isset($check_user['Customer']['id']) && $check_user['Customer']['id'] > 0 && $check_user['Customer']['status'] == 2) {
            $response['status'] = "1";
            $response['is_blocked'] = "2";
            $response['message'] = $GLOBALS["Webservice"]["messages"]['customer_blocked'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['customer_blocked'];
            return $this->fail_safe_return($response);
        }
		
		
        if (is_array($check_user['CustomerAddress']) && count($check_user['CustomerAddress']) > 0) {
            foreach ($check_user['CustomerAddress'] as $key => $value) {
                $areaData = $this->Area->find('first', array('conditions' => array('Area.id' => $value['area_id'])));
                $check_user['CustomerAddress'][$key]['areaname_ar'] = $areaData['Area']['areaname_ar'];
                $check_user['CustomerAddress'][$key]['areaname'] = $areaData['Area']['areaname'];

                $check_user['CustomerAddress'] = $check_user['CustomerAddress'];
            }
        }
		
		$order_data = $this->Order->findById($order_id);
		//echo '<pre>'; print_r($order_data);
		$address_id = $order_data['Order']['customer_address_id'];
		$address_data = $this->CustomerAddress->findById($address_id);
		$check_user['CustomerOrderAddress'] = array();
		
		if (is_array($address_data['CustomerAddress']) && count($address_data['CustomerAddress']) > 0) {
            foreach ($address_data['CustomerAddress'] as $key => $value) {
				
                $areaData = $this->Area->find('first', array('conditions' => array('Area.id' => $value['area_id'])));
                $check_user['CustomerOrderAddress'][$key]['areaname_ar'] = $areaData['Area']['areaname_ar'];
                $check_user['CustomerOrderAddress'][$key]['areaname'] = $areaData['Area']['areaname'];

                $check_user['CustomerOrderAddress'] = $address_data['CustomerAddress'];
            }
        }
			
		
        $response['is_blocked'] = "1";
        $response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
        $response['message'] = $GLOBALS["Webservice"]["messages"]['data_defination_success'];
        $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['data_defination_success'];
        $response['data'] = $check_user;
        $response = $this->setEmptyStringSimple($response);

        return $this->fail_safe_return($response);
    }
    /**
     * If Customer haven't verified email, then email will be sent through this function
     */
    function resendvemail($api_token, $deviceid, $device_type, $user_id) {

        $api_auth_token = 'skip_auth_token';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $user_id, $api_auth_token);

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }
        $this->Customer->recursive = -1;
        $checkCustomer = $this->Customer->find('first', array('fields' => array('id', 'f_name', 'l_name', 'email', 'activation_hash', 'status'), 'conditions' => array('Customer.id' => $user_id)));

        $activation_url = SITE_URL . 'customers/verify/' . $checkCustomer['Customer']['activation_hash'];
        $message = 'Hi ' . ucfirst($checkCustomer['Customer']['f_name'] . ' ' . $checkCustomer['Customer']['l_name']) . ',
                    <br /><br />
                    Thank you for registering with Rolling Scissor APP.
                   
                    <br /><br />
                    Please click on the link to activate you account. <a href="' . $activation_url . '">Activate Account</a>

                    <br /><br />
                    We hope you enjoy using our application.
                    <br /><br />
                    Good luck.
                    <br />
                    Rolling Scissor TEAM.
                    
                    <br />';

        $this->sendEmail($message, $checkCustomer['Customer']['email'], 'Welcome To Rolling Scissor APP');
        $response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
        $response['message'] = $GLOBALS["Webservice"]["messages"]['email_sent'];
        $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['email_sent'];
        return $this->fail_safe_return($response);
    }

    function get_cms_pages($api_token = '', $deviceid = '', $device_type = '', $page_id = '') {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $response_data = array();
        $options['conditions'] = array(
            "id" => $page_id
        );
        $response_data = $this->Cmspage->find("all", $options);

        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    //14-12-2016
    function add_cms_page($api_token = '', $deviceid = '', $device_type = '', $title, $title_ar, $description, $description_ar, $status) {

        // http://si-kw.com/rollingscissor/admin/cmspages/edit/2
        // http://si-kw.com/rollingscissor/admin/cmspages/edit/1

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $cmspage['title'] = $title;
        $cmspage['title_ar'] = $title_ar;
        $cmspage['description'] = $description;
        $cmspage['description_ar'] = $description_ar;
        $cmspage['status'] = $status;
        $cmspage['created_date'] = date('Y-m-d H:i:s');
        $cmspage['modified_date'] = date('Y-m-d H:i:s');

        $checkexists = $this->Cmspage->find('first', array('conditions' => array('OR' => array('title' => $title, 'title_ar' => $title_ar))));


        if (isset($checkexists['Cmspage']['id']) && $checkexists['Cmspage']['id'] > 0) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['cms_page_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['cms_page_exists'],
            );
            return $this->fail_safe_return($response);
        } else {

            if ($this->Cmspage->save($cmspage)) {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['cms_page_add'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['cms_page_add'],
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['error'],
                    'message' => $GLOBALS['Webservice']['messages']['cms_page_add_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['cms_page_add_error'],
                );
            }
            return $this->fail_safe_return($response);
        }
    }

    function edit_cms_page($api_token = '', $deviceid = '', $device_type = '', $title, $title_ar, $description, $description_ar, $status, $cms_page_id) {

        // http://si-kw.com/rollingscissor/admin/cmspages/edit/2
        // http://si-kw.com/rollingscissor/admin/cmspages/edit/1

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $cmspage['id'] = $cms_page_id;
        $cmspage['title'] = $title;
        $cmspage['title_ar'] = $title_ar;
        $cmspage['description'] = $description;
        $cmspage['description_ar'] = $description_ar;
        $cmspage['status'] = $status;
        $cmspage['modified_date'] = date('Y-m-d H:i:s');


        $checkexists = $this->Cmspage->find('first', array('conditions' => array('OR' => array('title' => $title, 'title_ar' => $title_ar), 'AND' => array('id !=' => $cms_page_id))));

        if (isset($checkexists['Cmspage']['id']) && $checkexists['Cmspage']['id'] > 0) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['cms_page_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['cms_page_exists'],
            );
            return $this->fail_safe_return($response);
        } else {

            if ($this->Cmspage->save($cmspage)) {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['cms_page_edit'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['cms_page_edit'],
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['error'],
                    'message' => $GLOBALS['Webservice']['messages']['cms_page_edit_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['cms_page_edit_error'],
                );
            }
            return $this->fail_safe_return($response);
        }
    }

    function get_all_cms_pages($api_token = '', $deviceid = '', $device_type = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');


        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $date_offset = '';
        if (empty($date_offset)) {
            $date_offset = date('Y-m-d H:i:s');
        }

        $pagesize = $pagesize == '' ? 10 : $pagesize;
        $page = $page == '' ? 1 : $page;
        $total_pages = 0;

        //pagination 
        if ($page > 0) {
            --$page;
        }
        $page = (int) $page;

        $pagesize = (int) $pagesize;
        if ($pagesize <= 0) {
            $pagesize = 1;
        }

        $response_data = array();

        $options = array();

        $total_records = $this->Cmspage->find("count", $options);


        if ($total_records > 0) {
            $total_pages = ceil($total_records / $pagesize);

            if ($page > $total_pages) {
                $response_data['customer_data'] = [];
            } else {

                $offset = $page * $pagesize;
                $limit = $pagesize;


                $cmspages = $this->Cmspage->find("all", array('order' => array('Cmspage.id' => 'asc'), 'limit' => $limit, 'offset' => $offset));
                // $log=$this->Cmspage->getDataSource()->getLog(false, false);  echo "<pre>"; print_r($log); exit;     

                if (is_array($cmspages) && count($cmspages) > 0) {

                    $cmspages = Set::extract('/Cmspage/.', $cmspages);

                    $response_data['total_pages'] = $total_records;
                    $response_data['cmspages'] = $cmspages;
                }
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function delete_cms_page($api_token, $deviceid, $device_type, $cms_page_id) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        if ($this->Cmspage->delete($cms_page_id)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['cms_page_delete'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['cms_page_delete'],
            );
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['cms_page_delete_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['cms_page_delete_error'],
            );
        }
        return $this->fail_safe_return($response);
    }

    function add_business_rule($api_token = '', $deviceid = '', $device_type = '', $name, $name_ar, $value) {
        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $business['name'] = $name;
        $business['name_ar'] = $name_ar;
        $business['value'] = $value;
        $business['created_date'] = date('Y-m-d H:i:s');
        $business['modified_date'] = date('Y-m-d H:i:s');

        $checkexists = $this->Businessrule->find('first', array('conditions' => array('OR' => array('name' => $name, 'name_ar' => $name_ar))));


        if (isset($checkexists['Businessrule']['id']) && $checkexists['Businessrule']['id'] > 0) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['business_rule_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['business_rule_exists'],
            );
            return $this->fail_safe_return($response);
        } else {

            if ($this->Businessrule->save($business)) {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['business_rule_add'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['business_rule_add'],
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['error'],
                    'message' => $GLOBALS['Webservice']['messages']['business_rule_add_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['business_rule_add_error'],
                );
            }
            return $this->fail_safe_return($response);
        }
    }

    function edit_business_rule($api_token = '', $deviceid = '', $device_type = '', $name, $name_ar, $value, $business_id) {
        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $business['id'] = $business_id;
        $business['name'] = $name;
        $business['name_ar'] = $name_ar;
        $business['value'] = $value;
        $business['modified_date'] = date('Y-m-d H:i:s');

        $checkexists = $this->Businessrule->find('first', array('conditions' => array('OR' => array('name' => $name, 'name_ar' => $name_ar), 'AND' => array('id !=' => $business_id))));
        // $log=$this->Businessrule->getDataSource()->getLog(false, false); echo "<pre>"; print_r($log); exit;

        if (isset($checkexists['Businessrule']['id']) && $checkexists['Businessrule']['id'] > 0) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['business_rule_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['business_rule_exists'],
            );
            return $this->fail_safe_return($response);
        } else {

            if ($this->Businessrule->save($business)) {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['business_rule_edit'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['business_rule_edit'],
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['error'],
                    'message' => $GLOBALS['Webservice']['messages']['business_rule_edit_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['business_rule_edit_error'],
                );
            }
            return $this->fail_safe_return($response);
        }
    }

    function get_all_business_rule($api_token = '', $deviceid = '', $device_type = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');


        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $date_offset = '';
        if (empty($date_offset)) {
            $date_offset = date('Y-m-d H:i:s');
        }

        $pagesize = $pagesize == '' ? 10 : $pagesize;
        $page = $page == '' ? 1 : $page;
        $total_pages = 0;

        //pagination 
        if ($page > 0) {
            --$page;
        }
        $page = (int) $page;

        $pagesize = (int) $pagesize;
        if ($pagesize <= 0) {
            $pagesize = 1;
        }

        $response_data = array();

        $options = array();

        $total_records = $this->Businessrule->find("count", $options);


        if ($total_records > 0) {
            $total_pages = ceil($total_records / $pagesize);

            if ($page > $total_pages) {
                $response_data['customer_data'] = [];
            } else {

                $offset = $page * $pagesize;
                $limit = $pagesize;

                $businessrules = $this->Businessrule->find("all", array('order' => array('Businessrule.id' => 'asc'), 'limit' => $limit, 'offset' => $offset));
                // $log=$this->Cmspage->getDataSource()->getLog(false, false);  echo "<pre>"; print_r($log); exit;     

                if (is_array($businessrules) && count($businessrules) > 0) {

                    $businessrules = Set::extract('/Businessrule/.', $businessrules);

                    $response_data['total_pages'] = $total_records;
                    $response_data['Businessrules'] = $businessrules;
                }
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function delete_business_rule($api_token, $deviceid, $device_type, $business_id) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        if ($this->Businessrule->delete($business_id)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['business_rule_delete'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['business_rule_delete'],
            );
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['business_rule_delete_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['business_rule_delete_error'],
            );
        }
        return $this->fail_safe_return($response);
    }

    //

    /**
     * Change Password for Customer
     */
    function change_password($api_token = '', $user_id = '', $old_password = '', $new_password = '', $deviceid = '', $device_type = '') {
        $api_auth_token = 'skip_auth_token';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $user_id, $api_auth_token);

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        App::uses('Sha256PasswordHasher', 'Controller/Component');
        $passwordHasher = new Sha256PasswordHasher();
        $old_password = $passwordHasher->hash($old_password);

        $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
        $response['message'] = $GLOBALS["Webservice"]["messages"]['change_password_error'];
        $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['change_password_error'];

        $check_user_pass = $this->Customer->find('first', array('conditions' => array('Customer.id' => $user_id, 'Customer.password' => $old_password)));



        if (isset($check_user_pass['Customer']['id']) && $check_user_pass['Customer']['status'] == 2) {
            $response['status'] = "1";
            $response['is_blocked'] = "2";
            $response['message'] = $GLOBALS["Webservice"]["messages"]['customer_blocked'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['customer_blocked'];
            return $this->fail_safe_return($response);
        }

        if (isset($check_user_pass['Customer']['id']) && $check_user_pass['Customer']['id'] > 0) {
            $update_user = array();
            $update_user['id'] = $user_id;
            $update_user['password'] = $new_password;
            $update_user['info'] = $new_password;

            $passed_array = array();
            $update_user = $this->security_save($update_user, $passed_array);

            if ($this->Customer->save($update_user)) {
                $response['is_blocked'] = "1";
                $response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
                $response['message'] = $GLOBALS["Webservice"]["messages"]['change_password_success'];
                $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['change_password_success'];
            } else {
                $response['is_blocked'] = "1";
                $response['message'] = $this->setServerError($this->Customer->validationErrors);
                $response['message_ar'] = ERROR_INPUT_DATA_AR;
                if ($response['message'] == '') {
                    $response['message'] = $GLOBALS["Webservice"]["messages"]['change_password_error'];
                }
            }
        } else {
            $response['message'] = $GLOBALS["Webservice"]["messages"]['change_password_invalid'];
        }

        return $this->fail_safe_return($response);
    }

	
    /**
     * Change Password for Customer
     */
    function admin_change_password($api_token = '', $user_id = '', $old_password = '', $new_password = '', $deviceid = '', $device_type = '') {
		
        $api_auth_token = 'skip_auth_token';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $user_id, $api_auth_token);

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        App::uses('Sha256PasswordHasher', 'Controller/Component');
        $passwordHasher = new Sha256PasswordHasher();
        $old_password = $passwordHasher->hash($old_password);

        $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
        $response['message'] = $GLOBALS["Webservice"]["messages"]['change_password_error'];
        $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['change_password_error'];

        $check_user_pass = $this->User->find('first', array('conditions' => array('User.id' => $user_id)));
		
        if (isset($check_user_pass['User']['id']) && $check_user_pass['User']['status'] == 2) {
            $response['status'] = "1";
            $response['is_blocked'] = "2";
            $response['message'] = $GLOBALS["Webservice"]["messages"]['customer_blocked'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['customer_blocked'];
            return $this->fail_safe_return($response);
        }

        if (isset($check_user_pass['User']['id']) && $check_user_pass['User']['id'] > 0) {
            $update_user = array();
            $update_user['id'] = $user_id;
            $update_user['password'] = $new_password;
            $update_user['info'] = $new_password;

            $passed_array = array();
            $update_user = $this->security_save($update_user, $passed_array);

            if ($this->User->save($update_user)) {
                $response['is_blocked'] = "1";
                $response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
                $response['message'] = $GLOBALS["Webservice"]["messages"]['change_password_success'];
                $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['change_password_success'];
            } else {
                $response['is_blocked'] = "1";
                $response['message'] = $this->setServerError($this->Customer->validationErrors);
                $response['message_ar'] = ERROR_INPUT_DATA_AR;
                if ($response['message'] == '') {
                    $response['message'] = $GLOBALS["Webservice"]["messages"]['change_password_error'];
                }
            }
        } else {
            $response['message'] = $GLOBALS["Webservice"]["messages"]['change_password_invalid'];
        }

        return $this->fail_safe_return($response);
    }


    function forgot_password($api_token, $email, $device_type) {

        $customerid = 'skip_user_id';
        $api_auth_token = 'skip_auth_token';
        $error_response = $this->common_checks($api_token, '1', $device_type, $customerid, $api_auth_token);

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $user_data = array();


        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $checkvariable = 'Customer.email';
        } else {
            $checkvariable = 'Customer.mobile';
        }


        // $user_data['user_id'] = $customerid;
        $user_data['email'] = $email;
        
        // by Aditya on 01.12.2017
    		
			// check email address or udid already registered
			$this->Customer->recursive = 0;
			$checkCustomer = $this->Customer->find('first', array('fields' => array('id', 'info', 'email', 'status', 'f_name'), 'conditions' => array('Customer.email' => $email, 'Customer.status != ' => 2) ));
			#pr($checkCustomer, 1);
			if(!empty($checkCustomer['Customer']['id'])){
	
				// send email
				$activation_link = '
				Hi '.ucfirst($checkCustomer['Customer']['f_name']).',
				<br /><br />
				Please find below your password to access Rolling Scissors account.
				<br /><br />
				Email: '.$checkCustomer['Customer']['email'].' <br>
				Password: '.$checkCustomer['Customer']['info'].'
				<br /><br />
				We hope you enjoy using our application.
				<br /><br />
				Good luck.
				<br />
				Rolling Scissors Team
				'	
				;
				$this->sendEmail($activation_link, $checkCustomer['Customer']['email'], 'Rolling Scissor - Forgot Password Request');
				
				

				
        // by Aditya on 01.12.2017
        
        
        
/*
        $passed_array = array();
        $user_data = $this->security_save($user_data, $passed_array);

        $check_email = $this->Customer->find('first', array('fields' => array('id', 'email', 'username', 'f_name', 'l_name', 'status'), 'conditions' => array($checkvariable => $email)));




        if (isset($check_email['Customer']['id']) && !empty($check_email['Customer']['id'])) {

            if ($check_email['Customer']['status'] == 2) {
                $response['status'] = "1";
                $response['is_blocked'] = "2";
                $response['message'] = $GLOBALS["Webservice"]["messages"]['customer_blocked'];
                $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['customer_blocked'];
                return $this->fail_safe_return($response);
            }



            $forgot_hash = $this->get_unique_name();
            $this->request->data['Customer']['id'] = $check_email['Customer']['id'];
            $this->request->data['Customer']['forgot_hash'] = $forgot_hash;
            $this->Customer->save($this->request->data);
            $activation_url = SITE_URL . 'Customers/forgot/' . $forgot_hash;



            $message = 'Hi ' . ucfirst($check_email['Customer']['f_name'] . ' ' . $check_email['Customer']['l_name']) . ',
                    <br /><br />
                    Please click on following link to reset your password.
                   
                    <br /><br />
                    <a href="' . $activation_url . '">Forgot Password Link</a>
                    <br /><br />
                    Get connected.
                    <br /><br />
                    Best regards,
                    <br />
                    Rolling Scissor team.                    
                    <br />';


            $this->sendEmail($message, $check_email['Customer']['email'], 'Rolling Scissor - Forgot Password Request');
*/

            $response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
            $response['message'] = $GLOBALS['Webservice']['messages']['forget_password_success'];
            $response['message_ar'] = $GLOBALS['Webservice']['messages_ar']['forget_password_success'];
        } else {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
            $response['message'] = $GLOBALS['Webservice']['messages']['forget_password_error2'];
            $response['message_ar'] = $GLOBALS['Webservice']['messages_ar']['forget_password_error2'];
        }

        return $this->fail_safe_return($response);
    }

    /**
     * Edit the profile details for Customer
     */
    public function update_profile($api_token, $user_id, $deviceid, $device_type, $f_name, $l_name, $dob, $mobile, $email, $address) {
        $api_auth_token = 'skip_auth_token';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $user_id, $api_auth_token);

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $customerData = $this->Customer->find('first', array('conditions' => array('Customer.id' => $user_id)));
        if ($customerData['Customer']['status'] == 2) {
            $response['status'] = "1";
            $response['is_blocked'] = "2";
            $response['message'] = $GLOBALS["Webservice"]["messages"]['customer_blocked'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['customer_blocked'];
            return $this->fail_safe_return($response);
        }
        if ($customerData['Customer']['id'] > 0) {
            // email same than goes here
            if ($customerData['Customer']['email'] == $email) {
                // mobile number check with existing mobile number
                $moblecount = $this->Customer->find('count', array('conditions' => array('Customer.mobile' => $mobile, 'Customer.id !=' => $user_id)));
                if ($moblecount > 0) {
                    $response['is_blocked'] = "1";
                    $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                    $response['message'] = $GLOBALS["Webservice"]["messages"]['mobile_alreay_exists'];
                    $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['mobile_alreay_exists'];
                } else {

                    // update mobile number with new and other information 
                    $profile_data = array();
                    $profile_data['id'] = $user_id;
                    $profile_data['f_name'] = $f_name;
                    $profile_data['l_name'] = $l_name;
                    if ($dob == '00-00-0000') {
                        $profile_data['dob'] = '0000-00-00';
                    } else {
                        $profile_data['dob'] = date('Y-m-d', strtotime($dob));
                    }
                    $profile_data['mobile'] = $mobile;

                    $profile_data = $this->security_save($profile_data);

                    if ($this->Customer->save($profile_data)) {


                        if ($this->CustomerAddress->deleteAll(array('CustomerAddress.customer_id' => $user_id))) {
                            $address = json_decode($address);

                            if (is_array($address) && count($address) > 0) {
                                foreach ($address as $key => $value) {

                                    if ($value->area_id > 0) {
                                        $this->CustomerAddress->create();

                                        // showing Area name for en / ar by Aditya
                                        $areaData = $this->Area->find('first', array('conditions' => array('Area.id' => $value->area_id)));
                                        $addAddress['areaname'] = $areaData['Area']['areaname'];
                                        $addAddress['areaname_ar'] = $areaData['Area']['areaname_ar'];

                                        #$addAddress['address_name'] = $value->address_name;
                                        $addAddress['parking'] = $value->parking;
                                        $addAddress['electric'] = $value->electric;
                                        $addAddress['block'] = $value->block;
                                        $addAddress['street'] = $value->street;
                                        $addAddress['floor'] = $value->floor;
                                        $addAddress['area_id'] = $value->area_id;
                                        $addAddress['building'] = $value->building;
                                        $addAddress['customer_id'] = $this->Customer->id;
                                        $addAddress['unit_number'] = $value->unit_number;
                                        $addAddress['latitude'] = $value->latitude;
                                        $addAddress['longitude'] = $value->longitude;
                                        $this->CustomerAddress->save($addAddress);
                                    }
                                }
                            }
                        }
                        $userData = $this->Customer->find('first', array('conditions' => array('Customer.id' => $user_id)));
                        if ($userData['Customer']['dob'] == '0000-00-00') {
                            $userData['Customer']['dob'] = "00-00-0000";
                        } else {
                            $userData['Customer']['dob'] = date('d-m-Y', strtotime($userData['Customer']['dob']));
                        }
                        $response['data'] = $userData;
                        $response['is_blocked'] = "1";
                        $response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
                        $response['message'] = $GLOBALS['Webservice']['messages']['edit_profile_success'];
                        $response['message_ar'] = $GLOBALS['Webservice']['messages_ar']['edit_profile_success'];
                    } else {
                        $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                        $response['data'] = array();
                        $response['is_blocked'] = "1";
                        $response['message'] = $this->setServerError($this->Customer->validationErrors);
                        $response['message_ar'] = $GLOBALS['Webservice']['messages_ar']['edit_profile_error'];
                        if ($response['message'] == '') {
                            $response['message'] = $GLOBALS["Webservice"]["messages"]['edit_profile_error'];
                        }
                    }
                }
            } else {
                // if email address edit by user and check with existing user that new email address is unique or not
                if (!empty($email)) {
                    $emailcount = $this->Customer->find('count', array('conditions' => array('Customer.email' => $email, 'Customer.id !=' => $user_id)));
                } else {
                    $emailcount = 0;
                }
                if ($emailcount == 0) {
                    // mobile number check with existing mobile number
                    $moblecount = $this->Customer->find('count', array('conditions' => array('Customer.mobile' => $mobile, 'Customer.id !=' => $user_id)));
                    if ($moblecount > 0) {
                        $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                        $response['message'] = $GLOBALS["Webservice"]["messages"]['mobile_alreay_exists'];
                        $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['mobile_alreay_exists'];
                    } else {
                        // update mobile number with new and other information 

                        $activation_hash = $this->get_unique_name();
                        $activation_url = SITE_URL . 'customers/verify/' . $activation_hash;
                        $profile_data = array();
                        $profile_data['id'] = $user_id;
                        $profile_data['f_name'] = $f_name;
                        $profile_data['l_name'] = $l_name;
                        $profile_data['status'] = 0;
                        $profile_data['verified'] = 0;
                        if ($dob == '00-00-0000') {
                            $profile_data['dob'] = '0000-00-00';
                        } else {
                            $profile_data['dob'] = date('Y-m-d', strtotime($dob));
                        }
                        $profile_data['mobile'] = $mobile;
                        if (!empty($email)) {
                            $profile_data['email'] = $email;
                        } else {
                            $profile_data['email'] = '';
                        }

                        $profile_data['activation_hash'] = $activation_hash;


                        $profile_data = $this->security_save($profile_data);

                        if ($this->Customer->save($profile_data)) {


                            if ($this->CustomerAddress->deleteAll(array('CustomerAddress.customer_id' => $user_id))) {
                                $address = json_decode($address);

                                if (is_array($address) && count($address) > 0) {
                                    foreach ($address as $key => $value) {

                                        $this->CustomerAddress->create();

                                        // showing Area name for en / ar by Aditya
                                        $areaData = $this->Area->find('first', array('conditions' => array('Area.id' => $value->area_id)));
                                        $addAddress['areaname'] = $areaData['Area']['areaname'];
                                        $addAddress['areaname_ar'] = $areaData['Area']['areaname_ar'];

                                        $addAddress['parking'] = $value->parking;
                                        $addAddress['electric'] = $value->electric;
                                        $addAddress['block'] = $value->block;
                                        $addAddress['street'] = $value->street;
                                        $addAddress['floor'] = $value->floor;
                                        $addAddress['area_id'] = $value->area_id;
                                        $addAddress['building'] = $value->building;
                                        $addAddress['customer_id'] = $this->Customer->id;
                                        $addAddress['unit_number'] = $value->unit_number;
                                        $addAddress['latitude'] = $value->latitude;
                                        $addAddress['longitude'] = $value->longitude;

                                        $this->CustomerAddress->save($addAddress);
                                    }
                                }
                            }

                            if (!empty($email)) {
                                $message = 'Hi ' . ucfirst($f_name) . ' ' . ucfirst($l_name) . ',
                                           
                                            <br /><br />
                                            Please click on the link to activate you account. <a href="' . $activation_url . '">Activate Account</a>

                                            <br /><br />
                                            We hope you enjoy using our application.
                                            <br /><br />
                                            Good luck.
                                            <br />
                                            Rolling Scissor TEAM.
                                            
                                            <br />';

                                $this->sendEmail($message, $email, 'Verification Email for Rolling Scissor APP');
                            }
                            $userData = $this->Customer->find('first', array('conditions' => array('Customer.id' => $user_id)));
                            if ($userData['Customer']['dob'] == '0000-00-00') {
                                $userData['Customer']['dob'] = "00-00-0000";
                            } else {
                                $userData['Customer']['dob'] = date('d-m-Y', strtotime($userData['Customer']['dob']));
                            }
                            $response['data'] = $userData;
                            $response['is_blocked'] = "1";
                            $response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
                            $response['message'] = $GLOBALS['Webservice']['messages']['edit_profile_success_verification'];
                            $response['message_ar'] = $GLOBALS['Webservice']['messages_ar']['edit_profile_success_verification'];
                        } else {
                            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                            $response['message'] = $this->setServerError($this->Customer->validationErrors);
                            $response['data'] = array();
                            $response['is_blocked'] = "1";
                            $response['message_ar'] = $GLOBALS['Webservice']['messages_ar']['edit_profile_error'];
                            if ($response['message'] == '') {
                                $response['message'] = $GLOBALS["Webservice"]["messages"]['edit_profile_error'];
                            }
                        }
                    }
                } else {
                    $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                    $response['message'] = $GLOBALS["Webservice"]["messages"]['email_address_alreay_exists'];
                    $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['email_address_alreay_exists'];
                }
            }
        } else {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
            $response['message'] = $GLOBALS['Webservice']['messages']['no_customer_found'];
            $response['message_ar'] = $GLOBALS['Webservice']['messages_ar']['no_customer_found'];
        }


        return $this->fail_safe_return($response);
    }
	
	public function update_profile_v2($api_token, $user_id, $deviceid, $device_type, $f_name, $l_name, $dob, $mobile, $email, $address) {
        $api_auth_token = 'skip_auth_token';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $user_id, $api_auth_token);

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $customerData = $this->Customer->find('first', array('conditions' => array('Customer.id' => $user_id)));
        if ($customerData['Customer']['status'] == 2) {
            $response['status'] = "1";
            $response['is_blocked'] = "2";
            $response['message'] = $GLOBALS["Webservice"]["messages"]['customer_blocked'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['customer_blocked'];
            return $this->fail_safe_return($response);
        }
        if ($customerData['Customer']['id'] > 0) {
            // email same than goes here
            if ($customerData['Customer']['email'] == $email) {
                // mobile number check with existing mobile number
                $moblecount = $this->Customer->find('count', array('conditions' => array('Customer.mobile' => $mobile, 'Customer.id !=' => $user_id)));
                if ($moblecount > 0) {
                    $response['is_blocked'] = "1";
                    $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                    $response['message'] = $GLOBALS["Webservice"]["messages"]['mobile_alreay_exists'];
                    $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['mobile_alreay_exists'];
                } else {

                    // update mobile number with new and other information 
                    $profile_data = array();
                    $profile_data['id'] = $user_id;
                    $profile_data['f_name'] = $f_name;
                    $profile_data['l_name'] = $l_name;
                    if ($dob == '00-00-0000') {
                        $profile_data['dob'] = '0000-00-00';
                    } else {
                        $profile_data['dob'] = date('Y-m-d', strtotime($dob));
                    }
                    $profile_data['mobile'] = $mobile;

                    $profile_data = $this->security_save($profile_data);

                    if ($this->Customer->save($profile_data)) {


                        //if ($this->CustomerAddress->deleteAll(array('CustomerAddress.customer_id' => $user_id))) {
                            $address = json_decode($address);

                            if (is_array($address) && count($address) > 0) {
								
                                foreach ($address as $key => $value) {
									
                                    if ($value->area_id > 0) {
										if($value->id == '/' or $value->id == ''){
											$addAddress = array();
											$this->CustomerAddress->create();
											// showing Area name for en / ar by Aditya
											$areaData = $this->Area->find('first', array('conditions' => array('Area.id' => $value->area_id)));
											$addAddress['areaname'] = $areaData['Area']['areaname'];
											$addAddress['areaname_ar'] = $areaData['Area']['areaname_ar'];

											#$addAddress['address_name'] = $value->address_name;
											$addAddress['parking'] = $value->parking;
											$addAddress['electric'] = $value->electric;
											$addAddress['block'] = $value->block;
											$addAddress['street'] = $value->street;
											$addAddress['floor'] = $value->floor;
											$addAddress['area_id'] = $value->area_id;
											$addAddress['building'] = $value->building;
											$addAddress['customer_id'] = $this->Customer->id;
											$addAddress['unit_number'] = $value->unit_number;
											$addAddress['latitude'] = $value->latitude;
											$addAddress['longitude'] = $value->longitude;
											$this->CustomerAddress->save($addAddress);
										}
										else{
											$updateAddress = array();
											//$this->CustomerAddress->create();
											$updateAddress['id'] = $value->id;
											// showing Area name for en / ar by Aditya
											$areaData = $this->Area->find('first', array('conditions' => array('Area.id' => $value->area_id)));
											$updateAddress['areaname'] = $areaData['Area']['areaname'];
											$updateAddress['areaname_ar'] = $areaData['Area']['areaname_ar'];

											#$addAddress['address_name'] = $value->address_name;
											$updateAddress['parking'] = $value->parking;
											$updateAddress['electric'] = $value->electric;
											$updateAddress['block'] = $value->block;
											$updateAddress['street'] = $value->street;
											$updateAddress['floor'] = $value->floor;
											$updateAddress['area_id'] = $value->area_id;
											$updateAddress['building'] = $value->building;
											$updateAddress['customer_id'] = $this->Customer->id;
											$updateAddress['unit_number'] = $value->unit_number;
											$updateAddress['latitude'] = $value->latitude;
											$updateAddress['longitude'] = $value->longitude;
											$this->CustomerAddress->save($updateAddress);
										}
                                    }
                                }
                            }
                        //}
                        $userData = $this->Customer->find('first', array('conditions' => array('Customer.id' => $user_id)));
                        if ($userData['Customer']['dob'] == '0000-00-00') {
                            $userData['Customer']['dob'] = "00-00-0000";
                        } else {
                            $userData['Customer']['dob'] = date('d-m-Y', strtotime($userData['Customer']['dob']));
                        }
                        $response['data'] = $userData;
                        $response['is_blocked'] = "1";
                        $response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
                        $response['message'] = $GLOBALS['Webservice']['messages']['edit_profile_success'];
                        $response['message_ar'] = $GLOBALS['Webservice']['messages_ar']['edit_profile_success'];
                    } else {
                        $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                        $response['data'] = array();
                        $response['is_blocked'] = "1";
                        $response['message'] = $this->setServerError($this->Customer->validationErrors);
                        $response['message_ar'] = $GLOBALS['Webservice']['messages_ar']['edit_profile_error'];
                        if ($response['message'] == '') {
                            $response['message'] = $GLOBALS["Webservice"]["messages"]['edit_profile_error'];
                        }
                    }
                }
            } else {
                // if email address edit by user and check with existing user that new email address is unique or not
                if (!empty($email)) {
                    $emailcount = $this->Customer->find('count', array('conditions' => array('Customer.email' => $email, 'Customer.id !=' => $user_id)));
                } else {
                    $emailcount = 0;
                }
                if ($emailcount == 0) {
                    // mobile number check with existing mobile number
                    $moblecount = $this->Customer->find('count', array('conditions' => array('Customer.mobile' => $mobile, 'Customer.id !=' => $user_id)));
                    if ($moblecount > 0) {
                        $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                        $response['message'] = $GLOBALS["Webservice"]["messages"]['mobile_alreay_exists'];
                        $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['mobile_alreay_exists'];
                    } else {
                        // update mobile number with new and other information 

                        $activation_hash = $this->get_unique_name();
                        $activation_url = SITE_URL . 'customers/verify/' . $activation_hash;
                        $profile_data = array();
                        $profile_data['id'] = $user_id;
                        $profile_data['f_name'] = $f_name;
                        $profile_data['l_name'] = $l_name;
                        $profile_data['status'] = 0;
                        $profile_data['verified'] = 0;
                        if ($dob == '00-00-0000') {
                            $profile_data['dob'] = '0000-00-00';
                        } else {
                            $profile_data['dob'] = date('Y-m-d', strtotime($dob));
                        }
                        $profile_data['mobile'] = $mobile;
                        if (!empty($email)) {
                            $profile_data['email'] = $email;
                        } else {
                            $profile_data['email'] = '';
                        }

                        $profile_data['activation_hash'] = $activation_hash;


                        $profile_data = $this->security_save($profile_data);

                        if ($this->Customer->save($profile_data)) {


                            if ($this->CustomerAddress->deleteAll(array('CustomerAddress.customer_id' => $user_id))) {
                                $address = json_decode($address);

                                if (is_array($address) && count($address) > 0) {
                                    foreach ($address as $key => $value) {

                                        $this->CustomerAddress->create();

                                        // showing Area name for en / ar by Aditya
                                        $areaData = $this->Area->find('first', array('conditions' => array('Area.id' => $value->area_id)));
                                        $addAddress['areaname'] = $areaData['Area']['areaname'];
                                        $addAddress['areaname_ar'] = $areaData['Area']['areaname_ar'];

                                        $addAddress['parking'] = $value->parking;
                                        $addAddress['electric'] = $value->electric;
                                        $addAddress['block'] = $value->block;
                                        $addAddress['street'] = $value->street;
                                        $addAddress['floor'] = $value->floor;
                                        $addAddress['area_id'] = $value->area_id;
                                        $addAddress['building'] = $value->building;
                                        $addAddress['customer_id'] = $this->Customer->id;
                                        $addAddress['unit_number'] = $value->unit_number;
                                        $addAddress['latitude'] = $value->latitude;
                                        $addAddress['longitude'] = $value->longitude;

                                        $this->CustomerAddress->save($addAddress);
                                    }
                                }
                            }

                            if (!empty($email)) {
                                $message = 'Hi ' . ucfirst($f_name) . ' ' . ucfirst($l_name) . ',
                                           
                                            <br /><br />
                                            Please click on the link to activate you account. <a href="' . $activation_url . '">Activate Account</a>

                                            <br /><br />
                                            We hope you enjoy using our application.
                                            <br /><br />
                                            Good luck.
                                            <br />
                                            Rolling Scissor TEAM.
                                            
                                            <br />';

                                $this->sendEmail($message, $email, 'Verification Email for Rolling Scissor APP');
                            }
                            $userData = $this->Customer->find('first', array('conditions' => array('Customer.id' => $user_id)));
                            if ($userData['Customer']['dob'] == '0000-00-00') {
                                $userData['Customer']['dob'] = "00-00-0000";
                            } else {
                                $userData['Customer']['dob'] = date('d-m-Y', strtotime($userData['Customer']['dob']));
                            }
                            $response['data'] = $userData;
                            $response['is_blocked'] = "1";
                            $response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
                            $response['message'] = $GLOBALS['Webservice']['messages']['edit_profile_success_verification'];
                            $response['message_ar'] = $GLOBALS['Webservice']['messages_ar']['edit_profile_success_verification'];
                        } else {
                            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                            $response['message'] = $this->setServerError($this->Customer->validationErrors);
                            $response['data'] = array();
                            $response['is_blocked'] = "1";
                            $response['message_ar'] = $GLOBALS['Webservice']['messages_ar']['edit_profile_error'];
                            if ($response['message'] == '') {
                                $response['message'] = $GLOBALS["Webservice"]["messages"]['edit_profile_error'];
                            }
                        }
                    }
                } else {
                    $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                    $response['message'] = $GLOBALS["Webservice"]["messages"]['email_address_alreay_exists'];
                    $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['email_address_alreay_exists'];
                }
            }
        } else {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
            $response['message'] = $GLOBALS['Webservice']['messages']['no_customer_found'];
            $response['message_ar'] = $GLOBALS['Webservice']['messages_ar']['no_customer_found'];
        }


        return $this->fail_safe_return($response);
    }

    function get_governorates($api_token = '', $deviceid = '', $device_type = '') {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $response_data = array();
        $options['conditions'] = array(
            "Governorate.status" => "1"
        );
        $this->Governorate->recursive = -1;
        $response_data = $this->Governorate->find("all", $options);
        $response_data = Set::extract('/Governorate/.', $response_data);

        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function get_governorates_areas($api_token = '', $deviceid = '', $device_type = '') {

        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $response_data = array();
        $options['conditions'] = array(
            "Governorate.status" => "1"
        );
        #$this->Governorate->recursive = -1;
        $response_data = $this->Governorate->find("all", $options);
        #$response_data = Set::extract('/Governorate/.', $response_data);

        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function get_areas($api_token = '', $deviceid = '', $device_type = '', $type = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }


        if ($type == 0) {
            $date_offset = '';
            if (empty($date_offset)) {
                $date_offset = date('Y-m-d H:i:s');
            }

            if ($page > 0) {
                --$page;
            }
            $page = (int) $page;

            $pagesize = (int) $pagesize;
            if ($pagesize <= 0) {
                $pagesize = 1;
            }
            $response_data = array();

            $options['conditions'] = array("Area.status" => 1);

            $total_records = $this->Area->find("count", $options);


            if ($total_records > 0) {
                $total_pages = ceil($total_records / $pagesize);

                if ($page > $total_pages) {
                    $response_data['service_data'] = [];
                } else {

                    $offset = $page * $pagesize;
                    $limit = $pagesize;

                    $getArea = $this->Area->find("all", array('conditions' => array("Area.status" => 1),
                        'order' => array('Area.created' => 'desc'), 'limit' => $limit, 'offset' => $offset));

                    if (is_array($getArea) && count($getArea) > 0) {

                        $getArea = Set::extract('/Area/.', $getArea);
                        $response_data['area_data'] = $getArea;

                        $response_data['total_pages'] = "$total_pages";
                        $response_data['date_offset'] = $date_offset;
                        $page = $page + 1;
                        $response_data['current_page'] = "$page";
                    }
                }
            }
        } else {

            $response_data = array();
            $options['conditions'] = array(
                "Area.status" => 1,
            );
            $options['order'] = array(
                "Area.areaname" => 'asc'
            );
            $response_data = $this->Area->find("all", $options);
            $response_data = Set::extract('/Area/.', $response_data);
        }


        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function get_admin_areas($api_token = '', $deviceid = '', $device_type = '', $coloum_name = '', $search_text = '', $type = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }


        if ($type == 0) {
            $date_offset = '';
            if (empty($date_offset)) {
                $date_offset = date('Y-m-d H:i:s');
            }

            if ($page > 0) {
                --$page;
            }
            $page = (int) $page;

            $pagesize = (int) $pagesize;
            if ($pagesize <= 0) {
                $pagesize = 1;
            }
            $response_data = array();


            if (!empty($coloum_name)) {
                $options['conditions'] = array("Area.$coloum_name LIKE" => "%$search_text%" ); //,"Area.status" => 1,
            } else {
                $options['conditions'] = array(); //"Area.status" => 1
            }



            $total_records = $this->Area->find("count", $options);


            if ($total_records > 0) {
                $total_pages = ceil($total_records / $pagesize);

                if ($page > $total_pages) {
                    $response_data['service_data'] = [];
                } else {

                    $offset = $page * $pagesize;
                    $limit = $pagesize;


                    if (!empty($coloum_name)) {
						// "Area.status" => 1, 
                        $getArea = $this->Area->find("all", array('conditions' => array("Area.$coloum_name LIKE" => "%$search_text%"),
                            'order' => array('Area.status' => 'desc', 'Area.areaname' => 'asc', 'Area.created' => 'desc'), 'limit' => $limit, 'offset' => $offset)); //, 'limit' => $limit, 'offset' => $offset
                    } else {
                        $getArea = $this->Area->find("all", array('conditions' => array(), //"Area.status" => 1
                            'order' => array('Area.status' => 'desc', 'Area.areaname' => 'asc', 'Area.created' => 'desc'), 'limit' => $limit, 'offset' => $offset)); // 
                    }



                    if (is_array($getArea) && count($getArea) > 0) {

                        $getArea = Set::extract('/Area/.', $getArea);
                        $response_data['area_data'] = $getArea;

                        $response_data['total_pages'] = "$total_pages";
                        $response_data['date_offset'] = $date_offset;
                        $page = $page + 1;
                        $response_data['current_page'] = "$page";
                    }
                }
            }
        } else {

            $response_data = array();
            $options['conditions'] = array(
                //"Area.status" => 1,
            );

            $response_data = $this->Area->find("all", $options);
            $response_data = Set::extract('/Area/.', $response_data);
        }


        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function get_business_rules($api_token = '', $deviceid = '', $device_type = '') {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $res = $this->Businessrule->find('first', array('conditions' => array('Businessrule.id' => 1)));
        if ($res['Businessrule']['id'] > 0) {
            unset($res['Businessrule']['id']);
            unset($res['Businessrule']['created']);
            $response_data = $res['Businessrule'];
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function add_areas($api_token, $governorates_id, $name, $name_ar, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $area['governorates_id'] = $governorates_id;
        $area['areaname'] = $name;
        $area['areaname_ar'] = $name_ar;
		//$area['status'] = !empty($status) ? $status : 1;
		
        $checkexists = $this->Area->find('first', array('conditions' => array('OR' => array('areaname' => $name, 'areaname_ar' => $name_ar))));


        if (isset($checkexists['Area']['id']) && $checkexists['Area']['id'] > 0) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['area_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['area_exists'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        } else {


            if ($this->Area->save($area)) {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['area_add'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['area_add'],
                    'data' => array()
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['area_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['area_error'],
                    'data' => array()
                );
            }
            return $this->fail_safe_return($response);
        }
    }

    function edit_areas($api_token, $governorates_id, $name, $name_ar, $area_id, $deviceid, $device_type) {
		
        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $area['governorates_id'] = $governorates_id;
        $area['areaname'] = $name;
        $area['areaname_ar'] = $name_ar;
        $area['id'] = $area_id;
		//$area['status'] = !empty($status) ? $status : 1;

        $checkexists = $this->Area->find('first', array('conditions' => array('OR' => array('areaname' => $name, 'areaname_ar' => $name_ar), 'Area.id !=' => $area_id)));

        if (isset($checkexists['Area']['id']) && $checkexists['Area']['id'] > 0) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['area_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['area_exists'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        } else {

            if ($this->Area->save($area)) {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['area_edit'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['area_edit'],
                    'data' => array()
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['area_edit_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['area_edit_error'],
                    'data' => array()
                );
            }
            return $this->fail_safe_return($response);
        }
    }

    function delete_areas($api_token, $area_id, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }



        if ($this->Area->delete($area_id)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['area_delete'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['area_delete'],
                'data' => array()
            );
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['area_delete_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['area_delete_error'],
                'data' => array()
            );
        }
        return $this->fail_safe_return($response);
    }

    function find_areas($api_token, $name, $name_ar, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $area['areaname'] = strtolower($name);
        $area['areaname_ar'] = strtolower($name_ar);

        $checkexists = $this->Area->find('first', array('conditions' => array('OR' => array('areaname' => $name, 'areaname_ar' => $name_ar))));
        #pr($checkexists);
        #die('found!');

        if (isset($checkexists['Area']['id']) && $checkexists['Area']['id'] > 0) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => array($checkexists)
            );
            return $this->fail_safe_return($response);
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        }
    }

    function get_services($api_token = '', $deviceid = '', $device_type = '', $type = '', $service_type, $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }


        if ($type == 0) {
            $date_offset = '';
            if (empty($date_offset)) {
                $date_offset = date('Y-m-d H:i:s');
            }

            if ($page > 0) {
                --$page;
            }
            $page = (int) $page;

            $pagesize = (int) $pagesize;
            if ($pagesize <= 0) {
                $pagesize = 1;
            }
            $response_data = array();


            $options['conditions'] = array("Service.status" => "1");





            $total_records = $this->Service->find("count", $options);


            if ($total_records > 0) {
                $total_pages = ceil($total_records / $pagesize);

                if ($page > $total_pages) {
                    $response_data['service_data'] = [];
                } else {

                    $offset = $page * $pagesize;
                    $limit = $pagesize;


                    $getService = $this->Service->find("all", array('conditions' => array("Service.status" => "1"),
                        'order' => array('Service.created' => 'desc'), 'limit' => $limit, 'offset' => $offset));





                    if (is_array($getService) && count($getService) > 0) {

                        foreach ($getService as $key => $value) {

                            $getService[$key]['Service']['description'] = $value['Service']['description'];
                            $getService[$key]['Service']['description_ar'] = $value['Service']['description_ar'];

                            $getService[$key]['Service']['image'] = SERVICES_IMG_PATH . $value['Service']['image'];
                            if ($value['Service']['parent_id'] > 0) {
                                $getParentCat = $this->Service->find('first', array('fields' => array('Service.name', 'Service.name_ar'), 'conditions' => array('Service.id' => $value['Service']['parent_id'])));
                                $getService[$key]['Service']['parent_name'] = $getParentCat['Service']['name'];
                                $getService[$key]['Service']['parent_name_ar'] = $getParentCat['Service']['name_ar'];
                            } else {

                                $getService[$key]['Service']['parent_name'] = "";
                                $getService[$key]['Service']['parent_name_ar'] = "";
                            }
                        }

                        $getService = Set::extract('/Service/.', $getService);
                        $response_data['service_data'] = $getService;

                        $response_data['total_pages'] = "$total_pages";
                        $response_data['date_offset'] = $date_offset;
                        $page = $page + 1;
                        $response_data['current_page'] = "$page";
                    }
                }
            }
        } else {
            $response_data = array();
            if ($service_type == 2) {
                $options['conditions'] = array(
                    "Service.status" => 1,
                    "Service.parent_id !=" => 0,
                );
            } else {
                $options['conditions'] = array(
                    "Service.status" => 1,
                    "Service.parent_id !=" => 0,
                    "Service.service_type" => $service_type
                );
            }

            $getService = $this->Service->find("all", $options);

            if (is_array($getService) && count($getService) > 0) {

                foreach ($getService as $key => $value) {

                    $getService[$key]['Service']['description'] = $value['Service']['description'];
                    $getService[$key]['Service']['description_ar'] = $value['Service']['description_ar'];

                    $getService[$key]['Service']['image'] = SERVICES_IMG_PATH . $value['Service']['image'];
                    $getParentCat = $this->Service->find('first', array('fields' => array('Service.name', 'Service.name_ar'), 'conditions' => array('Service.id' => $value['Service']['parent_id'])));
                    $getService[$key]['Service']['parent_name'] = $getParentCat['Service']['name'];
                    $getService[$key]['Service']['parent_name_ar'] = $getParentCat['Service']['name_ar'];
                }

                $response_data = Set::extract('/Service/.', $getService);
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }
        return $this->fail_safe_return($response);
    }

    function add_services($api_token, $name, $parent_id, $price, $service_type, $no_slot, $name_ar, $description, $description_ar, $image, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }
        if ($image != '') {
            $proper_image = $this->base64_filesave($image, SERVICES_ABS_PATH, '.png');
            $service['image'] = $proper_image;
        }

        $service['name'] = $name;
        $service['name_ar'] = $name_ar;
        $service['description'] = $description;
        $service['description_ar'] = $description_ar;
        $service['price'] = $price;
        $service['parent_id'] = $parent_id;
        $service['no_slot'] = $no_slot;
        $service['service_type'] = $service_type;

        $checkexists = $this->Service->find('first', array('conditions' => array('OR' => array('name' => $name, 'name_ar' => $name_ar ), 'service_type' => $service_type)));

        if (isset($checkexists['Service']['id']) && $checkexists['Service']['id'] > 0) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['service_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['service_exists'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        } else {
            if ($this->Service->save($service)) {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['service_add'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['service_add'],
                    'data' => array()
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['service_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['service_error'],
                    'data' => array()
                );
            }
            return $this->fail_safe_return($response);
        }
    }

    function edit_services($api_token, $name, $parent_id, $price, $service_type, $image, $no_slot, $name_ar, $description, $description_ar, $service_id, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        // Add extra checks for Same service exists - service type and parent id by Aditya on 13.04.2017
        $checkexists = $this->Service->find('first', array('conditions' => array('OR' => array('name' => $name, 'name_ar' => $name_ar), 'Service.id !=' => $service_id, 'Service.service_type' => $service_type, 'Service.parent_id' => $parent_id)));

        if (isset($checkexists['Service']['id']) && $checkexists['Service']['id'] > 0) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['service_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['service_exists'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        } else {

            $serviceData = $this->Service->find('first', array('conditions' => array('Service.id' => $service_id)));

            if ($image != '') {
                $old_profile = $serviceData['Service']['image'];

                if ($old_profile != '') {
                    $unlinkpath = SERVICES_ABS_PATH . $old_profile;
                    @unlink($unlinkpath);
                }
            }
            $image = $this->base64_filesave($image, SERVICES_ABS_PATH, '.png');
            $service['image'] = $image;

            $service['name'] = $name;
            $service['name_ar'] = $name_ar;
            $service['description'] = $description;
            $service['description_ar'] = $description_ar;
            $service['price'] = $price;
            $service['parent_id'] = $parent_id;
            $service['no_slot'] = $no_slot;
            $service['id'] = $service_id;
            $service['service_type'] = $service_type;

            if ($this->Service->save($service)) {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['service_edit'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['service_edit'],
                    'data' => array()
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['service_edit_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['service_edit_error'],
                    'data' => array()
                );
            }
            return $this->fail_safe_return($response);
        }
    }

    function delete_services($api_token, $service_id, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }


        $serviceData = $this->Service->find('first', array('conditions' => array('Service.id' => $service_id)));
        if ($this->Service->delete($service_id)) {
            $this->Service->deleteAll(array('Service.parent_id' => $serviceData['Service']['id']));
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['service_delete'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['service_delete'],
                'data' => array()
            );
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['service_delete_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['service_delete_error'],
                'data' => array()
            );
        }
        return $this->fail_safe_return($response);
    }

    function get_combo_services($api_token = '', $deviceid = '', $device_type = '', $coloum_name = '', $search_text = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');


        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }



        $date_offset = '';
        if (empty($date_offset)) {
            $date_offset = date('Y-m-d H:i:s');
        }

        if ($page > 0) {
            --$page;
        }
        $page = (int) $page;

        $pagesize = (int) $pagesize;
        if ($pagesize <= 0) {
            $pagesize = 1;
        }
        $response_data = array();

        $options = array();
        if (!empty($coloum_name)) {
            $options['conditions'] = array("Service.$coloum_name LIKE" => "%$search_text%", "Package.status" => "1",);
        }

        $total_records = $this->Combo->find("count", $options);


        if ($total_records > 0) {
            $total_pages = ceil($total_records / $pagesize);

            if ($page > $total_pages) {
                $response_data['combo_data'] = [];
            } else {

                $offset = $page * $pagesize;
                $limit = $pagesize;


                $getCombo = $this->Combo->find("all", array('conditions' => array("Combo.status" => "1"),
                    'order' => array('Combo.created' => 'desc'), 'limit' => $limit, 'offset' => $offset));

                if (is_array($getCombo) && count($getCombo) > 0) {

                    foreach ($getCombo as $key => $value) {
                        $service = array();
                        $service_ar = array();
                        foreach ($value['Service'] as $k => $val) {
                            array_push($service, $val['name']);
                            array_push($service_ar, $val['name_ar']);
                        }
                        $getCombo[$key]['Combo']['service'] = implode(', ', $service);
                        $getCombo[$key]['Combo']['service_ar'] = implode(', ', $service_ar);
                    }
                    $getCombo = Set::extract('/Combo/.', $getCombo);



                    $response_data['combo_data'] = $getCombo;

                    $response_data['total_pages'] = "$total_pages";
                    $response_data['date_offset'] = $date_offset;
                    $page = $page + 1;
                    $response_data['current_page'] = "$page";
                }
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function add_combo_services($api_token, $combo_service_id, $no_slots, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }




        $service['no_slots'] = $no_slots;
        $service['service_string'] = $combo_service_id;




        $checkexists = $this->Combo->find('first', array('conditions' => array('Combo.service_string' => $combo_service_id)));

        if (isset($checkexists['Combo']['id']) && $checkexists['Combo']['id'] > 0) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['combo_service_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['combo_service_exists'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        } else {
            if ($this->Combo->save($service)) {
                if ($combo_service_id != '') {
                    $combo_service_id = explode(',', $combo_service_id);
                    if (is_array($combo_service_id) && count($combo_service_id) > 0) {
                        foreach ($combo_service_id as $key => $value) {
                            $this->ComboService->create();
                            $this->request->data['ComboService']['service_id'] = $value;
                            $this->request->data['ComboService']['combo_id'] = $this->Combo->id;
                            $this->ComboService->save($this->request->data['ComboService']);
                        }
                    }
                }

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['combo_service_add'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['combo_service_add'],
                    'data' => array()
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['combo_service_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['combo_service_error'],
                    'data' => array()
                );
            }
            return $this->fail_safe_return($response);
        }
    }

    function edit_combo_services($api_token, $combo_id, $no_slots, $combo_service_id, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }


        $checkexists = $this->Combo->find('first', array('conditions' => array('Combo.service_string' => $combo_service_id, 'Combo.id != ' => $combo_id)));

        if (isset($checkexists['Combo']['id']) && $checkexists['Combo']['id'] > 0) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['combo_service_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['combo_service_exists'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        } else {

            $serviceData = $this->Combo->find('first', array('conditions' => array('Combo.id' => $combo_id)));
            $this->ComboService->deleteAll(array('ComboService.combo_id' => $combo_id));

            $service['no_slots'] = $no_slots;
            $service['service_string'] = $combo_service_id;
            $service['id'] = $combo_id;

            if ($this->Combo->save($service)) {
                if ($combo_service_id != '') {
                    $combo_service_id = explode(',', $combo_service_id);
                    if (is_array($combo_service_id) && count($combo_service_id) > 0) {
                        foreach ($combo_service_id as $key => $value) {
                            $this->ComboService->create();
                            $this->request->data['ComboService']['service_id'] = $value;
                            $this->request->data['ComboService']['combo_id'] = $this->Combo->id;
                            $this->ComboService->save($this->request->data['ComboService']);
                        }
                    }
                }
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['combo_service_edit'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['combo_service_edit'],
                    'data' => array()
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['combo_service_edit_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['combo_service_edit_error'],
                    'data' => array()
                );
            }
            return $this->fail_safe_return($response);
        }
    }

    function delete_combo_services($api_token, $combo_id, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }



        if ($this->Combo->delete($combo_id)) {
            $this->ComboService->deleteAll(array('ComboService.combo_id' => $combo_id));
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['combo_service_delete'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['combo_service_delete'],
                'data' => array()
            );
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['combo_service_delete_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['combo_service_delete_error'],
                'data' => array()
            );
        }
        return $this->fail_safe_return($response);
    }

    function get_package_dates($api_token = '', $deviceid = '', $device_type = '', $last_date = '') {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $i = 0;
        $available_dates = array();
        if (!empty($last_date)) {
            $today_date = $last_date;
        } else {
            $today_date = date('Y-m-d');
        }
        do {
            $dates = $today_date;
            $available_date = $this->AppointmentDetail->find('count', array('fields' => array('AppointmentDetail.appointment_date'), 'conditions' => array('AppointmentDetail.appointment_date' => $dates)));

            if ($available_date == 0) {
                array_push($available_dates, array('date' => $dates, 'selected' => false));
                $i++;
            }
            $today_date = date('Y-m-d', strtotime($dates . "+1 days"));
        } while ($i <= 4);

        if (is_array($available_dates) && count($available_dates) > 0) {
            $response_data['available_date'] = $available_dates;
        } else {
            $response_data['available_date'] = array();
        }

        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }
        return $this->fail_safe_return($response);
    }

    function get_admin_packages($api_token = '', $deviceid = '', $coloum_name = '', $search_text = '', $device_type = '', $type = '', $package_type = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');


        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }


        if ($type == 0) {
            $date_offset = '';
            if (empty($date_offset)) {
                $date_offset = date('Y-m-d H:i:s');
            }

            if ($page > 0) {
                --$page;
            }
            $page = (int) $page;

            $pagesize = (int) $pagesize;
            if ($pagesize <= 0) {
                $pagesize = 1;
            }
            $response_data = array();

            if (!empty($coloum_name)) {
                $options['conditions'] = array("Package.$coloum_name LIKE" => "%$search_text%", "Package.status" => "1",);
            } else {
                $options['conditions'] = array("Package.status" => "1");
            }




            $total_records = $this->Package->find("count", $options);


            if ($total_records > 0) {
                $total_pages = ceil($total_records / $pagesize);

                if ($page > $total_pages) {
                    $response_data['package_data'] = [];
                } else {

                    $offset = $page * $pagesize;
                    $limit = $pagesize;

                    if (!empty($coloum_name)) {
                        $getPackage = $this->Package->find("all", array('conditions' => array("Package.$coloum_name LIKE" => "%$search_text%", "Package.status" => "1"),
                            'order' => array('Package.created' => 'desc'), 'limit' => $limit, 'offset' => $offset));
                    } else {
                        $getPackage = $this->Package->find("all", array('conditions' => array("Package.status" => "1"),
                            'order' => array('Package.created' => 'desc'), 'limit' => $limit, 'offset' => $offset));
                    }



                    if (is_array($getPackage) && count($getPackage) > 0) {
                        foreach ($getPackage as $key => $value) {
                            $getPackage[$key]['Package']['image'] = PACKAGE_IMG_PATH . $value['Package']['image'];
                            $service_array = array();
                            $service_array_ar = array();
                            foreach ($value['Service'] as $k => $value) {
                                array_push($service_array, $value['name']);
                                array_push($service_array_ar, $value['name_ar']);
                            }
                            $getPackage[$key]['Package']['service_name'] = implode(',', $service_array);
                            $getPackage[$key]['Package']['service_name_ar'] = implode(',', $service_array_ar);
                        }
                        $getPackage = Set::extract('/Package/.', $getPackage);
                        $response_data['package_data'] = $getPackage;

                        $response_data['total_pages'] = "$total_pages";
                        $response_data['date_offset'] = $date_offset;
                        $page = $page + 1;
                        $response_data['current_page'] = "$page";
                    }
                }
            }
        } else {
            $response_data = array();
            $options['conditions'] = array(
                "Package.status" => "1",
                "Package.packages_type" => $package_type,
            );

            $response_data = $this->Package->find("all", $options);
            if (is_array($response_data) && count($response_data) > 0) {

                foreach ($response_data as $key => $value) {
                    $response_data[$key]['Package']['image'] = PACKAGE_IMG_PATH . $value['Package']['image'];
                    $service_array = array();
                    $service_array_ar = array();
                    foreach ($value['Service'] as $k => $value) {
                        array_push($service_array, $value['name']);
                        array_push($service_array_ar, $value['name_ar']);
                    }
                    $response_data[$key]['Package']['service_name'] = implode(',', $service_array);
                    $response_data[$key]['Package']['service_name_ar'] = implode(',', $service_array_ar);
                }
            }
            $response_data = Set::extract('/Package/.', $response_data);
        }



        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function get_packages($api_token = '', $deviceid = '', $device_type = '', $type = '', $package_type = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');


        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }


        if ($type == 0) {
            $date_offset = '';
            if (empty($date_offset)) {
                $date_offset = date('Y-m-d H:i:s');
            }

            if ($page > 0) {
                --$page;
            }
            $page = (int) $page;

            $pagesize = (int) $pagesize;
            if ($pagesize <= 0) {
                $pagesize = 1;
            }
            $response_data = array();


            $options['conditions'] = array("Package.status" => "1");





            $total_records = $this->Package->find("count", $options);


            if ($total_records > 0) {
                $total_pages = ceil($total_records / $pagesize);

                if ($page > $total_pages) {
                    $response_data['package_data'] = [];
                } else {

                    $offset = $page * $pagesize;
                    $limit = $pagesize;


                    $getPackage = $this->Package->find("all", array('conditions' => array("Package.status" => "1"),
                        'order' => array('Package.created' => 'desc'), 'limit' => $limit, 'offset' => $offset));




                    if (is_array($getPackage) && count($getPackage) > 0) {
                        foreach ($getPackage as $key => $value) {
                            $getPackage[$key]['Package']['image'] = PACKAGE_IMG_PATH . $value['Package']['image'];
                            $service_array = array();
                            $service_array_ar = array();
                            foreach ($value['Service'] as $k => $value) {
                                array_push($service_array, $value['name']);
                                array_push($service_array_ar, $value['name_ar']);
                            }
                            $getPackage[$key]['Package']['service_name'] = implode(',', $service_array);
                            $getPackage[$key]['Package']['service_name_ar'] = implode(',', $service_array_ar);
                        }
                        $getPackage = Set::extract('/Package/.', $getPackage);
                        $response_data['package_data'] = $getPackage;

                        $response_data['total_pages'] = "$total_pages";
                        $response_data['date_offset'] = $date_offset;
                        $page = $page + 1;
                        $response_data['current_page'] = "$page";
                    }
                }
            }
        } else {

            $response_data = array();
            $options['conditions'] = array(
                "Package.status" => "1",
                "Package.packages_type" => $package_type,
            );

            $response_data = $this->Package->find("all", $options);
            if (is_array($response_data) && count($response_data) > 0) {

                foreach ($response_data as $key => $value) {
                    $response_data[$key]['Package']['image'] = PACKAGE_IMG_PATH . $value['Package']['image'];
                    $service_array = array();
                    $service_array_ar = array();
                    foreach ($value['Service'] as $k => $value) {
                        array_push($service_array, $value['name']);
                        array_push($service_array_ar, $value['name_ar']);
                    }
                    $response_data[$key]['Package']['service_name'] = implode(',', $service_array);
                    $response_data[$key]['Package']['service_name_ar'] = implode(',', $service_array_ar);
                }
            }
            $response_data = Set::extract('/Package/.', $response_data);
        }



        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function add_packages($api_token, $name, $name_ar, $no_slot, $package_type, $image, $price, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        if ($image != '') {
            $proper_image = $this->base64_filesave($image, PACKAGE_ABS_PATH, '.png');
            $packages['image'] = $proper_image;
        }

        $packages['no_slot'] = $no_slot;
        $packages['name'] = $name;
        $packages['name_ar'] = $name_ar;
        $packages['price'] = $price;
        $packages['packages_type'] = $package_type;

        $checkexists = $this->Package->find('first', array('conditions' => array('OR' => array('name' => $name, 'name_ar' => $name_ar))));

        if (isset($checkexists['Package']['id']) && $checkexists['Package']['id'] > 0) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['package_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['package_exists'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        } else {
            if ($this->Package->save($packages)) {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['pacakge_add'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['pacakge_add'],
                    'data' => array()
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['pacakge_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['pacakge_error'],
                    'data' => array()
                );
            }
            return $this->fail_safe_return($response);
        }
    }

    function edit_packages($api_token, $name, $name_ar, $no_slot, $image, $package_type, $price, $package_id, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }



        $checkexists = $this->Package->find('first', array('conditions' => array('OR' => array('name' => $name, 'name_ar' => $name_ar), 'Package.id !=' => $package_id)));

        if (isset($checkexists['Package']['id']) && $checkexists['Package']['id'] > 0) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['package_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['package_exists'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        } else {

            $packages['no_slot'] = $no_slot;
            $packages['name'] = $name;
            $packages['name_ar'] = $name_ar;
            $packages['price'] = $price;
            $packages['packages_type'] = $package_type;
            $packages['id'] = $package_id;
            $packageData = $this->Package->find('first', array('conditions' => array('Package.id' => $package_id)));
            if ($image != '') {
                $old_profile = $packageData['Package']['image'];

                if ($old_profile != '') {
                    $unlinkpath = PACKAGE_ABS_PATH . $old_profile;
                    @unlink($unlinkpath);
                }
            }
            $image = $this->base64_filesave($image, PACKAGE_ABS_PATH, '.png');
            $packages['image'] = $image;

            if ($this->Package->save($packages)) {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['pacakge_edit'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['pacakge_edit'],
                    'data' => array()
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['pacakge_edit_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['pacakge_edit_error'],
                    'data' => array()
                );
            }
            return $this->fail_safe_return($response);
        }
    }

    function delete_packages($api_token, $package_id, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }



        if ($this->Package->delete($package_id)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['service_delete'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['service_delete'],
                'data' => array()
            );
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['service_delete_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['service_delete_error'],
                'data' => array()
            );
        }
        return $this->fail_safe_return($response);
    }

    function get_admin_users($api_token = '', $deviceid = '', $device_type = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');


        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }



        $date_offset = '';
        if (empty($date_offset)) {
            $date_offset = date('Y-m-d H:i:s');
        }

        if ($page > 0) {
            --$page;
        }
        $page = (int) $page;

        $pagesize = (int) $pagesize;
        if ($pagesize <= 0) {
            $pagesize = 1;
        }
        $response_data = array();


        $options['conditions'] = array();


        $total_records = $this->User->find("count", $options);


        if ($total_records > 0) {
            $total_pages = ceil($total_records / $pagesize);

            if ($page > $total_pages) {
                $response_data['user_data'] = [];
            } else {

                $offset = $page * $pagesize;
                $limit = $pagesize;


                $getUser = $this->User->find("all", array('conditions' => array(),
                    'order' => array('User.created' => 'desc'), 'limit' => $limit, 'offset' => $offset));

                if (is_array($getUser) && count($getUser) > 0) {

                    $getUser = Set::extract('/User/.', $getUser);
                    $response_data['admin_data'] = $getUser;

                    $response_data['total_pages'] = "$total_pages";
                    $response_data['date_offset'] = $date_offset;
                    $page = $page + 1;
                    $response_data['current_page'] = "$page";
                }
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function add_admin_users($api_token, $f_name, $l_name, $email, $mobile, $username, $password, $address, $admin_type, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $user['f_name'] = $f_name;
        $user['l_name'] = $l_name;
        $user['email'] = $email;
        $user['mobile'] = $mobile;
        $user['username'] = $username;
        $user['password'] = $password;
        $user['address'] = $address;
        $user['admin_type'] = $admin_type;
        $user['device_type'] = $device_type;
        $user['device_id'] = $deviceid;



        $checkexists = $this->User->find('first', array('conditions' => array('OR' => array('email' => $email, 'username' => $username))));



        if (isset($checkexists['User']['id']) && $checkexists['User']['id'] > 0) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['user_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['user_exists'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        } else {

            if ($this->User->save($user)) {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['user_add'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['user_add'],
                    'data' => array()
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['user_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['user_error'],
                    'data' => array()
                );
            }
            return $this->fail_safe_return($response);
        }
    }

    function edit_admin_users($api_token, $f_name, $l_name, $mobile, $address, $deviceid, $user_id, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $user['f_name'] = $f_name;
        $user['l_name'] = $l_name;
        $user['mobile'] = $mobile;
        $user['address'] = $address;
        $user['device_type'] = $device_type;
        $user['device_id'] = $deviceid;
        $user['id'] = $user_id;



        if ($this->User->save($user)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['user_edit'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['user_edit'],
                'data' => array()
            );
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['user_edit_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['user_edit_error'],
                'data' => array()
            );
        }
        return $this->fail_safe_return($response);
    }

    function delete_admin_users($api_token, $user_id, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }



        if ($this->User->delete($user_id)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['user_delete'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['user_delete'],
                'data' => array()
            );
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['service_delete_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['service_delete_error'],
                'data' => array()
            );
        }
        return $this->fail_safe_return($response);
    }

    function admin_login($api_token = '', $username = '', $password = '', $deviceid = '', $device_type = '') {
        // $this->request->data['pass'] = 'Customer';

        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $response = array();
        $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];



        if ((empty($username) && empty($password)) || empty($username) || empty($password)) {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
            $response['message'] = $GLOBALS["Webservice"]["messages"]['auth_Empty_error'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['auth_Empty_error'];
            return $this->fail_safe_return($response);
        }

        if ($this->Customer->validates()) {
            App::uses('Sha256PasswordHasher', 'Controller/Component');
            $passwordHasher = new Sha256PasswordHasher();
            $password = $passwordHasher->hash($password);

            $options['conditions'] = array(
                "User.email" => $username,
                'User.password' => $password
            );


            $check_user = $this->User->find('first', $options);

            if (!empty($check_user['User']['id'])) {

                if ($check_user['User']['status'] == 2) {
                    $response['status'] = "1";
                    $response['message'] = $GLOBALS["Webservice"]["messages"]['customer_blocked'];
                    $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['customer_blocked'];
                } else {
                    $customerid = $check_user['User']['id'];
                    $response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
                    $response['message'] = $GLOBALS["Webservice"]["messages"]['auth_success'];
                    $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['auth_success'];
                    $response['data'] = $check_user;
                }
            } else {
                $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                $response['message'] = $GLOBALS["Webservice"]["messages"]['auth_error'];
                $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['auth_error'];
            }
        } else {
            $response['message'] = $this->setServerError($this->Customer->validationErrors);
            $response['message_ar'] = ERROR_INPUT_DATA_AR;
            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
            if ($response['message'] == '') {
                $response['message'] = $GLOBALS["Webservice"]["messages"]['auth_error'];
            }
            //$response['error_info'] = $this->Customer->validationErrors;
        }
        // print_r($response);exit;
        $response = $this->setEmptyStringSimple($response);

        return $this->fail_safe_return($response);
    }

    function get_customer($api_token = '', $deviceid = '', $device_type = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');


        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }



        $date_offset = '';
        if (empty($date_offset)) {
            $date_offset = date('Y-m-d H:i:s');
        }

        if ($page > 0) {
            --$page;
        }
        $page = (int) $page;

        $pagesize = (int) $pagesize;
        if ($pagesize <= 0) {
            $pagesize = 1;
        }
        $response_data = array();

        $options = array();

        // $options['conditions'] = array(
        //     "Package.status" => 1,
        // );


        $total_records = $this->Customer->find("count", $options);


        if ($total_records > 0) {
            $total_pages = ceil($total_records / $pagesize);

            if ($page > $total_pages) {
                $response_data['customer_data'] = [];
            } else {

                $offset = $page * $pagesize;
                $limit = $pagesize;


                $getCustomer = $this->Customer->find("all", array('order' => array('Customer.created' => 'desc'), 'limit' => $limit, 'offset' => $offset));


                if (is_array($getCustomer) && count($getCustomer) > 0) {
                    foreach ($getCustomer as $k => $v) {
                        if (is_array($v['CustomerAddress']) && count($v['CustomerAddress']) > 0) {
                            foreach ($v['CustomerAddress'] as $key => $value) {

                                $areaname = $this->Area->find('first', array('fields' => array('Area.areaname', 'Area.areaname_ar', 'Area.id'), 'conditions' => array('Area.id' => $value['area_id'])));

                                $v['CustomerAddress'][$key]['areaname_ar'] = $areaname['Area']['areaname_ar'];
                                $v['CustomerAddress'][$key]['areaname'] = $areaname['Area']['areaname'];
                            }
                            $getCustomer[$k]['CustomerAddress'] = $v['CustomerAddress'];
                        }
                    }
                }


                $response_data['customer_data'] = $getCustomer;

                $response_data['total_pages'] = "$total_pages";
                $response_data['date_offset'] = $date_offset;
                $page = $page + 1;
                $response_data['current_page'] = "$page";
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function customer_block_unblock($api_token = '', $deviceid = '', $device_type = '', $customer_id = '') {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $response_data = array();
        $customerData = $this->Customer->find('first', array('fields' => array('Customer.status', 'Customer.id'), 'conditions' => array('Customer.id' => $customer_id)));
        if (isset($customerData['Customer']['id']) && $customerData['Customer']['id'] > 0) {
            if ($customerData['Customer']['status'] == 1) {
                $status = 2;
                $message = $GLOBALS['Webservice']['messages']['admin_customer_blocked'];
                $message_ar = $GLOBALS['Webservice']['messages_ar']['admin_customer_blocked'];
            } else {
                $status = 1;
                $message = $GLOBALS['Webservice']['messages']['admin_customer_unblocked'];
                $message_ar = $GLOBALS['Webservice']['messages_ar']['admin_customer_unblocked'];
            }
            if ($this->Customer->updateAll(array('Customer.status' => $status), array('Customer.id' => $customer_id))) {
                $response_data['success'] = true;
            }
            if (!empty($response_data)) {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $message,
                    'message_ar' => $message_ar,
                    'data' => $response_data
                );
            } else {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['error'],
                    'message' => $GLOBALS['Webservice']['messages']['admin_customer_blocking_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['admin_customer_blocking_error'],
                    'data' => $response_data
                );
            }
            return $this->fail_safe_return($response);
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['no_customer_found'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_customer_found'],
                'data' => $response_data
            );
            return $this->fail_safe_return($response);
        }
    }

    function change_active_inactive_status($api_token = '', $deviceid = '', $device_type = '', $table = '', $id = '') {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $response_data = array();
        $tableData = $this->$table->find('first', array('fields' => array("$table.status", "$table.id"), 'conditions' => array("$table.id" => $id)));

        if (isset($tableData["$table"]['id']) && $tableData["$table"]['id'] > 0) {
            if ($tableData["$table"]['status'] == 1) {
                $status = 0;
            } else {
                $status = 1;
            }
            if ($this->$table->updateAll(array("$table.status" => $status), array("$table.id" => $id))) {
                $response_data['success'] = true;
            }
            if (!empty($response_data)) {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['admin_status_change_success'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['admin_status_change_success'],
                    'data' => $response_data
                );
            } else {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['error'],
                    'message' => $GLOBALS['Webservice']['messages']['admin_status_change_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['admin_status_change_error'],
                    'data' => $response_data
                );
            }
            return $this->fail_safe_return($response);
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['table_id_not_found'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['table_id_not_found'],
                'data' => $response_data
            );
            return $this->fail_safe_return($response);
        }
    }

    function master_search($api_token = '', $deviceid = '', $device_type = '', $table = '', $coloum_name = '', $search_text = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');


        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }



        $date_offset = '';
        if (empty($date_offset)) {
            $date_offset = date('Y-m-d H:i:s');
        }

        if ($page > 0) {
            --$page;
        }
        $page = (int) $page;

        $pagesize = (int) $pagesize;
        if ($pagesize <= 0) {
            $pagesize = 1;
        }
        $response_data = array();

        $options = array();

        $options['conditions'] = array(
            "$table.$coloum_name LIKE" => "%$search_text%",
        );


        $total_records = $this->$table->find("count", $options);


        if ($total_records > 0) {
            $total_pages = ceil($total_records / $pagesize);

            if ($page > $total_pages) {
                $response_data['search_data'] = [];
            } else {

                $offset = $page * $pagesize;
                $limit = $pagesize;


                $getSearch = $this->$table->find("all", array('conditions' => array("$table.$coloum_name LIKE" => "%$search_text%"), 'limit' => $limit, 'offset' => $offset));



                if (is_array($getSearch) && count($getSearch) > 0) {
                    $response_data['customer_data'] = $getSearch;
                    $response_data['total_pages'] = "$total_pages";
                    $response_data['date_offset'] = $date_offset;
                    $page = $page + 1;
                    $response_data['current_page'] = "$page";
                }
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['admin_search'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['admin_search'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    /*
     * Added by Alpesh Trivedi (Derivedweb) on 10 March 2017 Guided by Aditya Bhatt
     * Start of v2 add_schedular_v2
     */

    function add_schedular_v2($api_token, $schedular_type, $admin_id, $start_date, $end_date, $start_time, $end_time, $travel_time, $cleaning_time, $maximum_appointment, $maximum_slot_in_row, $slot_mins, $slot_lunch, $weekends, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        // $begin = new DateTime( '2010-05-01' );
        // $end = new DateTime( '2010-05-10' );
        // $interval = DateInterval::createFromDateString('1 day');
        // $period = new DatePeriod($begin, $interval, $end);
        $schedular['schedular_type'] = $schedular_type;
        $schedular['start_date'] = $start_date;
        $schedular['end_date'] = $end_date;
        $schedular['start_time'] = $start_time;
        $schedular['end_time'] = $end_time;
        $schedular['user_id'] = $admin_id;
        $schedular['travel_time'] = $travel_time;
        $schedular['cleaning_time'] = $cleaning_time;
        $schedular['maximum_appointment'] = $maximum_appointment;
        $schedular['maximum_slot_in_row'] = $maximum_slot_in_row;
        $schedular['slot_mins'] = $slot_mins;
        $schedular['slot_lunch'] = $slot_lunch;
        $schedular['weekends'] = $weekends;
        $schedular['device_type'] = $device_type;
        $schedular['device_id'] = $deviceid;

        if (strtotime($start_date) > strtotime($end_date)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['end_date_greater'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['end_date_greater'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        }
        if ((int) $schedular_type === 1 || (int) $schedular_type === 2) {
            $checkexists = $this->Schedular->find('first', array('conditions' => array('Schedular.schedular_type' => $schedular_type, 'Schedular.end_date >' => $start_date), 'order' => 'Schedular.id desc'));
//                        echo $this->Schedular->getLastQuery();die;
        } elseif ((int) $schedular_type === 3) {

            $checkexists = $this->Schedular->find('first', array('conditions' => array('Schedular.schedular_type' => $schedular_type, 'AND' => array('Schedular.start_date <=' => $start_date, 'Schedular.end_date >=' => $start_date)), 'order' => 'Schedular.id desc'));
//            echo $this->Schedular->getLastQuery();die;
        }

        if (isset($checkexists['Schedular']['id']) && $checkexists['Schedular']['id'] > 0) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['schedular_alreay_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_alreay_exists'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        } else {
            if ($this->Schedular->save($schedular)) {



                // $total_slot_time = $travel_time + $cleaning_time + $slot_mins;
                // for ($i=0; $i < $maximum_appointment ; $i++) { 
                //     $this->Appointment->create();
                //     $this->request->data['Appointment']['start_time'] = $start_time;
                //     $end_time = strtotime("+$total_slot_time minutes", strtotime($start_time));
                //     $this->request->data['Appointment']['end_time'] = date('h:i:s', $end_time);
                //     $this->request->data['Appointment']['schedular_id'] = $this->Schedular->id;
                //     $this->request->data['Appointment']['slot_no'] = $i + 1;
                //     $this->Appointment->save($this->request->data['Appointment']);
                //     $start_time = date('h:i:s', $end_time);
                // }


                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['schedular_add_success'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_add_success'],
                    'data' => array()
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['schedular_add_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_add_error'],
                    'data' => array()
                );
            }
            return $this->fail_safe_return($response);
        }
    }

    /*
     * Added by Alpesh Trivedi (Derivedweb) on 10 March 2017 Guided by Aditya Bhatt
     * Start of v2 edit_schedular_v2
     */

    function edit_schedular_v2($api_token, $schedular_type, $admin_id, $start_date, $end_date, $schedular_id, $start_time, $end_time, $travel_time, $cleaning_time, $maximum_appointment, $maximum_slot_in_row, $slot_mins, $slot_lunch, $weekends, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $schedular['schedular_type'] = $schedular_type;
        $schedular['start_date'] = $start_date;
        $schedular['end_date'] = $end_date;
        $schedular['start_time'] = $start_time;
        $schedular['end_time'] = $end_time;
        $schedular['user_id'] = $admin_id;
        $schedular['travel_time'] = $travel_time;
        $schedular['cleaning_time'] = $cleaning_time;
        $schedular['maximum_appointment'] = $maximum_appointment;
        $schedular['maximum_slot_in_row'] = $maximum_slot_in_row;
        $schedular['slot_mins'] = $slot_mins;
        $schedular['slot_lunch'] = $slot_lunch;
        $schedular['weekends'] = $weekends;
        $schedular['device_type'] = $device_type;
        $schedular['id'] = $schedular_id;
        $schedular['device_id'] = $deviceid;

        if (strtotime($start_date) > strtotime($end_date)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['end_date_greater'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['end_date_greater'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        }

        $checkSchedualexists = $this->Schedular->find('all', array('conditions' => array('Schedular.schedular_type' => $schedular_type, 'Schedular.end_date >=' => date('Y-m-d'), 'Schedular.id !=' => $schedular_id)));

        if (is_array($checkSchedualexists) && count($checkSchedualexists) > 0) {
            $is_fall_start_end_date = false;
            foreach ($checkSchedualexists as $key => $value) {
                $start_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $start_date);
                $end_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $end_date);
                if ($start_date_fall == true) {
                    $is_fall_start_end_date = true;
                }
                if ($end_date_fall == true) {
                    $is_fall_start_end_date = true;
                }
            }
        }

        if ($is_fall_start_end_date) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['schedular_alreay_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_alreay_exists'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        }



        if ($is_fall_start_end_date == false) {

            if ($this->Schedular->save($schedular)) {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['schedular_edit_success'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_edit_success'],
                    'data' => array()
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['schedular_edit_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_edit_error'],
                    'data' => array()
                );
            }
            return $this->fail_safe_return($response);
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['schedular_not_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_not_exists'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        }
    }

    /*
     * Added by Alpesh Trivedi (Derivedweb) on 10 March 2017 Guided by Aditya Bhatt
     * Start of v2 get_schedular_v2
     */

    function get_schedular_v2($api_token = '', $schedular_type = '', $deviceid = '', $device_type = '', $start_date = '', $end_date = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');
        if ($pagesize === '') {
            $pagesize = 10;
        }
        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }



        $date_offset = '';
        if (empty($date_offset)) {
            $date_offset = date('Y-m-d H:i:s');
        }

        if ($page > 0) {
            --$page;
        }
        $page = (int) $page;
        $pagesize = (int) $pagesize;
        if ($pagesize <= 0) {
            $pagesize = 1;
        }
        $response_data = array();

        $options = array();

        if (!empty($start_date) && !empty($end_date)) {
            $options['conditions'] = array(
                'Schedular.start_date <=' => "$end_date",
                'Schedular.start_date >=' => "$start_date"
            );
        }

        $total_records = $this->Schedular->find("count", $options);


        if ($total_records > 0) {
            $total_pages = ceil($total_records / $pagesize);

            if ($page > $total_pages) {
                $response_data['customer_data'] = [];
            } else {

                $offset = $page * $pagesize;
                $limit = $pagesize;

                if (!empty($start_date) && !empty($end_date)) {
                    $getSchedular = $this->Schedular->find("all", array('fields' => array('*', 'DATEDIFF(`start_date`, CURDATE()) AS diff'), 'conditions' => array('Schedular.start_date <=' => "$end_date", 'Schedular.start_date >=' => "$start_date"), 'order' => array('Schedular.schedular_type' => 'asc', 'abs(unix_timestamp(start_date) - unix_timestamp(now()))'), 'limit' => $limit, 'offset' => $offset));
                } else {
                    $getSchedular = $this->Schedular->find("all", array('fields' => array('*', 'DATEDIFF(`start_date`, CURDATE()) AS diff'), 'order' => array('Schedular.schedular_type' => 'asc', 'abs(unix_timestamp(start_date) - unix_timestamp(now()))'), 'limit' => $limit, 'offset' => $offset));
                }

                if (is_array($getSchedular) && count($getSchedular) > 0) {
                    $getSchedular = Set::extract('/Schedular/.', $getSchedular);
                    $response_data['schedular_data'] = $getSchedular;

                    $response_data['total_pages'] = "$total_pages";
                    $response_data['date_offset'] = $date_offset;
                    $page = $page + 1;
                    $response_data['current_page'] = "$page";
                }
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    /*
     * End of v2 get_schedular_v2 
     */


    /*
     * Added by Alpesh Trivedi (Derivedweb) on 10 March 2017 Guided by Aditya Bhatt
     * Start of v2 delete_schedular_v2
     */

    function delete_schedular_v2($api_token, $schedular_id, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }
        if ($this->Schedular->delete($schedular_id)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['schedular_delete_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_delete_success'],
                'data' => array()
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['schedular_delete_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_delete_error'],
                'data' => array()
            );
        }
        return $this->fail_safe_return($response);
    }

    function add_schedular($api_token, $admin_id, $start_date, $end_date, $start_time, $end_time, $travel_time, $cleaning_time, $maximum_appointment, $maximum_slot_in_row, $slot_mins, $slot_lunch, $weekends, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        // $begin = new DateTime( '2010-05-01' );
        // $end = new DateTime( '2010-05-10' );
        // $interval = DateInterval::createFromDateString('1 day');
        // $period = new DatePeriod($begin, $interval, $end);
        $schedular['start_date'] = $start_date;
        $schedular['end_date'] = $end_date;
        $schedular['start_time'] = $start_time;
        $schedular['end_time'] = $end_time;
        $schedular['user_id'] = $admin_id;
        $schedular['travel_time'] = $travel_time;
        $schedular['cleaning_time'] = $cleaning_time;
        $schedular['maximum_appointment'] = $maximum_appointment;
        $schedular['maximum_slot_in_row'] = $maximum_slot_in_row;
        $schedular['slot_mins'] = $slot_mins;
        $schedular['slot_lunch'] = $slot_lunch;
        $schedular['weekends'] = $weekends;
        $schedular['device_type'] = $device_type;
        $schedular['device_id'] = $deviceid;

        if (strtotime($start_date) > strtotime($end_date)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['end_date_greater'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['end_date_greater'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        }

        $checkexists = $this->Schedular->find('first', array('conditions' => array('Schedular.end_date >' => $start_date), 'order' => 'Schedular.id desc'));




        if (isset($checkexists['Schedular']['id']) && $checkexists['Schedular']['id'] > 0) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['schedular_alreay_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_alreay_exists'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        } else {
            if ($this->Schedular->save($schedular)) {



                // $total_slot_time = $travel_time + $cleaning_time + $slot_mins;
                // for ($i=0; $i < $maximum_appointment ; $i++) { 
                //     $this->Appointment->create();
                //     $this->request->data['Appointment']['start_time'] = $start_time;
                //     $end_time = strtotime("+$total_slot_time minutes", strtotime($start_time));
                //     $this->request->data['Appointment']['end_time'] = date('h:i:s', $end_time);
                //     $this->request->data['Appointment']['schedular_id'] = $this->Schedular->id;
                //     $this->request->data['Appointment']['slot_no'] = $i + 1;
                //     $this->Appointment->save($this->request->data['Appointment']);
                //     $start_time = date('h:i:s', $end_time);
                // }


                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['schedular_add_success'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_add_success'],
                    'data' => array()
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['schedular_add_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_add_error'],
                    'data' => array()
                );
            }
            return $this->fail_safe_return($response);
        }
    }

    private function check_in_range($start_date, $end_date, $date_from_user) {
        // Convert to timestamp
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($date_from_user);

        // Check that user date is between start & end
        return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }

    function edit_schedular($api_token, $admin_id, $start_date, $end_date, $schedular_id, $start_time, $end_time, $travel_time, $cleaning_time, $maximum_appointment, $maximum_slot_in_row, $slot_mins, $slot_lunch, $weekends, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $schedular['start_date'] = $start_date;
        $schedular['end_date'] = $end_date;
        $schedular['start_time'] = $start_time;
        $schedular['end_time'] = $end_time;
        $schedular['user_id'] = $admin_id;
        $schedular['travel_time'] = $travel_time;
        $schedular['cleaning_time'] = $cleaning_time;
        $schedular['maximum_appointment'] = $maximum_appointment;
        $schedular['maximum_slot_in_row'] = $maximum_slot_in_row;
        $schedular['slot_mins'] = $slot_mins;
        $schedular['slot_lunch'] = $slot_lunch;
        $schedular['weekends'] = $weekends;
        $schedular['device_type'] = $device_type;
        $schedular['id'] = $schedular_id;
        $schedular['device_id'] = $deviceid;

        if (strtotime($start_date) > strtotime($end_date)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['end_date_greater'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['end_date_greater'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        }

        $checkSchedualexists = $this->Schedular->find('all', array('conditions' => array('Schedular.end_date >=' => date('Y-m-d'), 'Schedular.id !=' => $schedular_id)));
        if (is_array($checkSchedualexists) && count($checkSchedualexists) > 0) {
            $is_fall_start_end_date = false;
            foreach ($checkSchedualexists as $key => $value) {
                $start_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $start_date);
                $end_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $end_date);
                if ($start_date_fall == true) {
                    $is_fall_start_end_date = true;
                }
                if ($end_date_fall == true) {
                    $is_fall_start_end_date = true;
                }
            }
        }

        if ($is_fall_start_end_date) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['schedular_alreay_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_alreay_exists'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        }



        if ($is_fall_start_end_date == false) {

            if ($this->Schedular->save($schedular)) {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['schedular_edit_success'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_edit_success'],
                    'data' => array()
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['schedular_edit_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_edit_error'],
                    'data' => array()
                );
            }
            return $this->fail_safe_return($response);
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['schedular_not_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_not_exists'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        }
    }

    function get_schedular($api_token = '', $deviceid = '', $device_type = '', $start_date = '', $end_date = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');


        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }



        $date_offset = '';
        if (empty($date_offset)) {
            $date_offset = date('Y-m-d H:i:s');
        }

        if ($page > 0) {
            --$page;
        }
        $page = (int) $page;

        $pagesize = (int) $pagesize;
        if ($pagesize <= 0) {
            $pagesize = 1;
        }
        $response_data = array();

        $options = array();



        if (!empty($start_date) && !empty($end_date)) {
            $options['conditions'] = array(
                'Schedular.start_date <=' => "$end_date",
                'Schedular.start_date >=' => "$start_date"
            );
        }



        $total_records = $this->Schedular->find("count", $options);


        if ($total_records > 0) {
            $total_pages = ceil($total_records / $pagesize);

            if ($page > $total_pages) {
                $response_data['customer_data'] = [];
            } else {

                $offset = $page * $pagesize;
                $limit = $pagesize;

                if (!empty($start_date) && !empty($end_date)) {
                    $getSchedular = $this->Schedular->find("all", array('conditions' => array('Schedular.start_date <=' => "$end_date", 'Schedular.start_date >=' => "$start_date"), 'order' => array('Schedular.created' => 'desc'), 'limit' => $limit, 'offset' => $offset));
                } else {
                    $getSchedular = $this->Schedular->find("all", array('order' => array('Schedular.created' => 'desc'), 'limit' => $limit, 'offset' => $offset));
                }

                if (is_array($getSchedular) && count($getSchedular) > 0) {

                    $getSchedular = Set::extract('/Schedular/.', $getSchedular);
                    $response_data['schedular_data'] = $getSchedular;

                    $response_data['total_pages'] = "$total_pages";
                    $response_data['date_offset'] = $date_offset;
                    $page = $page + 1;
                    $response_data['current_page'] = "$page";
                }
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function delete_schedular($api_token, $schedular_id, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }
        if ($this->Schedular->delete($schedular_id)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['schedular_delete_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_delete_success'],
                'data' => array()
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['schedular_delete_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['schedular_delete_error'],
                'data' => array()
            );
        }
        return $this->fail_safe_return($response);
    }

    public function add_admin_appointment($api_token = '', $admin_id = '', $guest_data = '', $back_to_back = '', $appointment_date = '', $package_id = '', $customer_id = '', $payment_method = '', $deviceid = '', $device_type = '', $timezone_time = '') {

        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $response_data = array();
        $response_ = array();

        /*
          $response = $this->Schedular->find('all', array('conditions' => array('Schedular.end_date >=' => date('Y-m-d'))));

          if (is_array($response) && count($response) > 0) {

          $schedular_id = 0;
          foreach ($response as $key => $value) {
          $start_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $appointment_date);
          $end_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $appointment_date);
          if ($start_date_fall == true) {
          $schedular_id = $value['Schedular']['id'];
          continue;
          }
          if ($end_date_fall == true) {
          $schedular_id = $value['Schedular']['id'];
          continue;
          }
          }
          }

          $options['conditions'] = array(
          "Schedular.id" => $schedular_id,
          );
          $schedularData = $this->Schedular->find("first", $options);
         */

        $schedularData = $this->get_schedular_from_date($appointment_date);
        #pr($schedularData);
        #die;

        if (empty($schedularData)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['no_schedular_set'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_schedular_set'],
                'data' => NULL
            );
            return $this->fail_safe_return($response);
        }

        $schedular_id = $schedularData['Schedular']['id'];


        $available_slots_array = array();

        if (is_array($schedularData) && count($schedularData) > 0) {

            $total_slot_time = $schedularData['Schedular']['travel_time'] + $schedularData['Schedular']['cleaning_time'] + $schedularData['Schedular']['slot_mins'];
            $travelTime = $schedularData['Schedular']['travel_time'];
            #echo $travelTime.'<br>';
            $cleaningTime = $schedularData['Schedular']['cleaning_time'];
            #echo $cleaningTime.'<br>';

            $start_time = $schedularData['Schedular']['start_time'];
            $schedular_id = $schedularData['Schedular']['id'];
            $slot_lunch = $schedularData['Schedular']['slot_lunch'];

            for ($i = 0; $i < $schedularData['Schedular']['maximum_appointment']; $i++) {

                $responses[$i]['start_time'] = $start_time;
                $end_time = strtotime("+$total_slot_time minutes", strtotime($start_time));
                $responses[$i]['end_time'] = date('H:i:s', $end_time);
                $responses[$i]['schedular_id'] = $schedular_id;
                $responses[$i]['slot_no'] = (string) ($i + 1);

                if ($slot_lunch == ($i + 1)) {
                    $responses[$i]['lunch_slot'] = "1";
                } else {
                    $responses[$i]['lunch_slot'] = "0";
                }

                $appointCount = $this->AppointmentDetail->find('count', array('conditions' => array('AppointmentDetail.appointment_date' => $appointment_date, 'AppointmentDetail.slot_no' => $responses[$i]['slot_no'])));

                if ($appointCount > 0) {
                    $responses[$i]['available_slot'] = "0";
                } else {
                    $responses[$i]['available_slot'] = "1";
                }
                array_push($available_slots_array, $responses[$i]['slot_no']);

                $start_time = date('H:i:s', $end_time);
                array_push($response_, $responses[$i]);
            }
        }

        $guest_data = json_decode($guest_data, true);
        $address_id = $guest_data[0]['address_id']; // added by Aditya 08.05.2017

        if (is_array($response_) && count($response_) > 0) {
            $start_time = $guest_data[0]['start_time'];


            $this->Appointment->create();
            $appoint['customer_id'] = $customer_id;
            $appoint['start_time'] = '00:00:00';
            $appoint['end_time'] = '00:00:00';
            $appoint['payment_method'] = $payment_method;
            $appoint['admin_id'] = $admin_id;
            $appoint['device_type'] = $device_type;
            $appoint['appointment_date'] = $appointment_date;
            $appoint['schedular_type'] = $schedularData['Schedular']['schedular_type'];
            $appoint['schedular_id'] = $schedularData['Schedular']['id'];

            if ($this->Appointment->save($appoint)) {

                $guest_ids = array();
                $isCombo = false;
                $i = 0;
                $gstid = NULL;
                $travelling = false;
                $kei = 0;

                #pr($guest_data); die('1here');

                foreach ($guest_data as $ke => $g) {

                    #pr($ke);
                    #pr($g);

                    $service_ = explode(',', $g['service_id']);
                    $comboData = $this->Combo->find('all', array('fields' => array('Combo.id', 'Combo.no_slots')));
                    if (is_array($comboData) && count($comboData) > 0) {

                        foreach ($comboData as $key => $value) {
                            $combos = array();
                            foreach ($value['ComboService'] as $key => $va) {
                                array_push($combos, $va['service_id']);
                            }
                            $checkcombo = array_diff($combos, $service_);
                            if (count($checkcombo) == 0) {
                                $isCombo = true;
                                $comboNoSlots = $value['Combo']['no_slots'];
                            }
                        }
                    }
                    if ($isCombo) {
                        $gs = $comboNoSlots;
                    } else {
                        $gs = $g['no_slots'];
                    }

                    $total_time_service = $gs * $total_slot_time;

                    $endTime = date('H:i', strtotime("+$total_time_service minutes", strtotime($g['start_time'])));

                    $this->AppointmentDetail->create();
                    $appointDetail['guest_id'] = $g['guest_id'];

                    array_push($guest_ids, $g['guest_id']);



                    // Travel Start/End time calculations
                    if ($i == 0 && $travelling == false) {

                        $appointDetail['travel_start_time'] = $travelStartTime = date("H:i:s", strtotime($g['start_time']));
                        $now = new DateTime($g['start_time']); //current date/time
                        $now->add(new DateInterval("PT{$travelTime}M"));
                        $appointDetail['travel_end_time'] = $travelEndTime = $now->format('H:i:s');
                    } else {

                        $appointDetail['travel_start_time'] = $appointDetail['travel_end_time'] = $travelStartTime = $travelEndTime = '00:00:00';
                    }

                    // Appointments Start/End time calculations
                    if ($i == 0 && $travelling == false) {

                        $appointDetail['start_time'] = $travelEndTime;
                        $now = new DateTime($endTime); //current date/time
                        $now->sub(new DateInterval("PT{$cleaningTime}M"));
                        $appointDetail['end_time'] = $now->format('H:i:s');
                    } else {

                        $appointDetail['start_time'] = $g['start_time'];
                        $now = new DateTime($endTime); //current date/time
                        $now->sub(new DateInterval("PT{$cleaningTime}M"));
                        $appointDetail['end_time'] = $now->format('H:i:s');
                    }

                    // Cleaning Start/End time calculations
                    $appointDetail['clean_start_time'] = $cleanStartTime = ($appointDetail['end_time']);
                    $now = new DateTime($appointDetail['end_time']); //current date/time
                    $now->add(new DateInterval("PT{$cleaningTime}M"));
                    $appointDetail['clean_end_time'] = $cleanEndTime = $now->format('H:i:s');



                    #$appointDetail['start_time'] = $g['start_time'];
                    #$appointDetail['end_time'] = $endTime;
                    /*
                      $appointDetail['travel_start_time'] = $g['travel_start_time'];
                      $appointDetail['travel_end_time'] = $g['travel_end_time'];

                      $appointDetail['start_time'] = $g['start_time'];
                      $appointDetail['end_time'] = $endTime;

                      $appointDetail['clean_start_time'] = $g['clean_start_time'];
                      $appointDetail['clean_end_time'] = $g['clean_end_time'];
                     */


                    $appointDetail['slot_no'] = $g['slots_no'];
                    $appointDetail['schedular_id'] = $schedular_id;
                    $appointDetail['customer_id'] = $customer_id;
                    $appointDetail['appointment_id'] = $this->Appointment->id;
                    $appointDetail['appointment_date'] = $appointment_date;
                    $appointDetail['appointment_confirmed'] = '1';

                    $this->AppointmentDetail->save($appointDetail);

                    if (!empty($g['service_id']) && $g['service_id'] > 0) {
                        $g['service_id'] = explode(',', $g['service_id']);
                        if (is_array($g['service_id']) && count($g['service_id']) > 0) {
                            foreach ($g['service_id'] as $k => $s) {
                                $serviceData = array();
                                $this->AppointmentService->create();
                                $serviceData['service_id'] = $s;
                                $serviceData['guest_id'] = $g['guest_id'];
                                $serviceData['appointment_id'] = $this->Appointment->id;
                                $serviceData['appointmentdetail_id'] = $this->AppointmentDetail->id;
                                $this->AppointmentService->save($serviceData);
                            }
                        }
                    }
                    $i++;
                }

                $startData = $this->AppointmentDetail->find('first', array('fields' => array('travel_start_time'), 'conditions' => array('appointment_id' => $this->Appointment->id), 'order' => array('AppointmentDetail.id' => 'asc')));
                $endData = $this->AppointmentDetail->find('first', array('fields' => array('clean_end_time'), 'conditions' => array('appointment_id' => $this->Appointment->id), 'order' => array('AppointmentDetail.id' => 'desc')));

                $this->Appointment->updateAll(array('Appointment.start_time' => "'" . $startData['AppointmentDetail']['travel_start_time'] . "'",
                    'Appointment.end_time' => "'" . $endData['AppointmentDetail']['clean_end_time'] . "'"), array('Appointment.id' => $this->Appointment->id));

                $orderResponse = $this->checkout_order($api_token = '123', $deviceid = 'admin', $device_type = '2', $user_id = $customer_id, $admin_id = $admin_id, $address_id, $appointment_id = $this->Appointment->id, $guest_id = implode(',', $guest_ids), $payment_method = 2, $total_amount = '0.000', $remaining_amount = '0.000', $transaction_id = '23523523523', $timezone_time);

                if ($orderResponse['status'] == 0) {
                    $response_data['success'] = true;
                }
            }
        }

        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['appointment_booked_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_booked_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['appointment_booked_already'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_booked_already'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    public function add_appointment($api_token = '', $admin_id = '', $guest_data = '', $back_to_back = '', $appointment_date = '', $package_id = '', $customer_id = '', $deviceid = '', $device_type = '') {

        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }
        if ($customer_id == '') {
            $customer_id = 1;
        }
        $response_data = array();
        $response_ = array();

        /*
          $response = $this->Schedular->find('all', array('conditions' => array('Schedular.end_date >=' => date('Y-m-d'))));
          #pr($response, 1);

          if (is_array($response) && count($response) > 0) {

          $schedular_id = 0;
          foreach ($response as $key => $value) {
          $start_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $appointment_date);
          $end_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $appointment_date);
          if ($start_date_fall == true) {
          $schedular_id = $value['Schedular']['id'];
          continue;
          }
          if ($end_date_fall == true) {
          $schedular_id = $value['Schedular']['id'];
          continue;
          }
          }
          }
          #echo $schedular_id;
          #die('hey!');


          $options['conditions'] = array(
          "Schedular.id" => $schedular_id,
          );
          $schedularData = $this->Schedular->find("first", $options);
          #echo 'here!';
          #pr($schedularData, 1);
          #die;
         */

        #pr($guest_data, 1);
        #die('here!');
		
		if($device_type == 2)
			$appointment_date = date('Y-m-d', strtotime($appointment_date));
			
        $schedularData = $this->get_schedular_from_date($appointment_date);
        #pr($schedularData);
        #die;
        if (empty($schedularData)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['no_schedular_set'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_schedular_set'],
                'data' => NULL
            );
            return $this->fail_safe_return($response);
        }

        $schedular_id = $schedularData['Schedular']['id'];
		
		// Decode Guest Data for "no_slots" parameter
		$guest_data = json_decode($guest_data, true);
		#print_r($guest_data); #die;
		$no_slotsreq = 0;
		foreach ($guest_data as $keygst => $valgst) {
			
			#pr($valgst);
			$no_slotsreq += $valgst['no_slots'];
		}
		#echo $no_slotsreq;
		#die;
		
		
		
		// Stop app between two dates by Aditya on 11.04.2018
		$paymentDate = new DateTime($appointment_date); // Today
		#echo $paymentDate->format('d/m/Y'); // echos today! 
		$contractDateBegin = new DateTime('2018-05-05');
		$contractDateEnd  = new DateTime('2018-05-14');
		
		if ( $paymentDate->getTimestamp() > $contractDateBegin->getTimestamp() && $paymentDate->getTimestamp() < $contractDateEnd->getTimestamp()){
			
			$response = array(
				'status' => $GLOBALS["Webservice"]["codes"]['error'],
				'message' => $GLOBALS['Webservice']['messages']['fully_booked_msg'],
				'message_ar' => $GLOBALS['Webservice']['messages_ar']['fully_booked_msg'],
				'data' => array()
			);
			return $this->fail_safe_return($response);
			
		}
		// Stop app between two dates by Aditya on 11.04.2018
		
		
		

        $available_slots_array = array();
        if (is_array($schedularData) && count($schedularData) > 0) {
			
			
			#echo $schedularData['Schedular']['travel_time'] .'='. $schedularData['Schedular']['cleaning_time'] .'='. $schedularData['Schedular']['slot_mins'] .'='. $guest_data[0]['no_slots']; // "* $num_of_slots" added by Aditya on 06.09.2017
			#echo '<br>';
			#$total_slot_time = $schedularData['Schedular']['travel_time'] + $schedularData['Schedular']['cleaning_time'] + ($schedularData['Schedular']['slot_mins'] * $guest_data[0]['no_slots']); // " * $num_of_slots" added by Aditya on 11.09.2017
			
			#$total_slot_time = $schedularData['Schedular']['travel_time'] + $schedularData['Schedular']['cleaning_time'] + ($schedularData['Schedular']['slot_mins'] * $no_slotsreq); // " * $num_of_slots" added by Aditya on 11.09.2017
			
			$total_slot_time = $schedularData['Schedular']['travel_time'] + ($schedularData['Schedular']['cleaning_time'] * count($guest_data)) + ($schedularData['Schedular']['slot_mins'] * $no_slotsreq); // " * $num_of_slots" added by Aditya on 11.09.2017
			
			#die;

            #$total_slot_time = $schedularData['Schedular']['slot_mins'] + $schedularData['Schedular']['cleaning_time'];
            $travelTime = $schedularData['Schedular']['travel_time'];
            #echo $travelTime.'<br>';
            $cleaningTime = $schedularData['Schedular']['cleaning_time'];
            #echo $cleaningTime.'<br>';
            #echo $travel_start_time = $schedularData['Schedular']['start_time'];
            #echo $travel_end_time = date("H:i:s", strtotime("+$travelTime minutes", strtotime($travel_start_time)));

            $start_time = $schedularData['Schedular']['start_time'];
            $schedular_id = $schedularData['Schedular']['id'];
            $slot_lunch = $schedularData['Schedular']['slot_lunch'];
            for ($i = 0; $i < $schedularData['Schedular']['maximum_appointment']; $i++) {

                #$responses[$i]['travel_start_time'] = $travel_start_time;
                #$responses[$i]['travel_end_time'] = $travel_end_time;

                $responses[$i]['start_time'] = $start_time;
				#echo '<br>';
                $end_time = strtotime("+$total_slot_time minutes", strtotime($start_time));
				#echo '<br>';
                $responses[$i]['end_time'] = date('H:i:s', $end_time);
                $responses[$i]['schedular_id'] = $schedular_id;
                $responses[$i]['slot_no'] = (string) ($i + 1);
                if ($slot_lunch == ($i + 1)) {
                    $responses[$i]['lunch_slot'] = "1";
                } else {
                    $responses[$i]['lunch_slot'] = "0";
                }
                $appointCount = $this->AppointmentDetail->find('count', array('conditions' => array('AppointmentDetail.appointment_date' => $appointment_date, 'AppointmentDetail.slot_no' => $responses[$i]['slot_no'])));
                if ($appointCount > 0) {
                    $responses[$i]['available_slot'] = "0";
                } else {
                    $responses[$i]['available_slot'] = "1";
                }
                array_push($available_slots_array, $responses[$i]['slot_no']);

                $start_time = date('H:i:s', $end_time);
                array_push($response_, $responses[$i]);
            }
        }


        
          #pr($response_); die;
/*

          #$date_start = $startData['AppointmentDetail']['travel_start_time'];
          #$date_end = $endData['AppointmentDetail']['clean_end_time'];
          $date_start = '06:00:00';
          $date_end = '10:00:00';
          $conditions = array(
          'conditions' => array(
          'Appointment.appointment_date' => $appointment_date,
          'Appointment.start_time >=' => ($date_start),
          'Appointment.end_time <=' => ($date_end),
          #'Appointment.id !=' => $this->Appointment->id,
          ));
          $findAppts = $this->Appointment->find('all', $conditions);
          pr($findAppts);



          die('here');
         */





        


        #var_dump($api_token, $admin_id, $guest_data, $back_to_back, $appointment_date, $package_id, $customer_id, $deviceid, $device_type);
        #pr($guest_data);
        #die('here now!');
        // Delete old Appointment details by customer_id by Aditya on 16.04.2017
        if (isset($customer_id) && !empty($customer_id)) {

            $appointmentDelete = $this->AppointmentDetail->find('all', array('conditions' => array('AppointmentDetail.appointment_confirmed' => '0', 'AppointmentDetail.customer_id' => $customer_id)));

            if (is_array($appointmentDelete) && count($appointmentDelete) > 0) {

                $appointmentDetailId = array();
                foreach ($appointmentDelete as $key => $value) {
                    array_push($appointmentDetailId, $value['AppointmentDetail']['id']);
                }

                if (is_array($appointmentDetailId) && count($appointmentDetailId) > 0) {
                    $this->AppointmentDetail->deleteAll(array('AppointmentDetail.id' => $appointmentDetailId));
                }
            }
        }

        // Delete old Appointment details by guest_id by Aditya on 16.04.2017
        /* foreach ($guest_data as $keya => $vala) {

          $this->AppointmentDetail->deleteAll(array('AppointmentDetail.guest_id' => $vala['guest_id']));
          } */
		  
		#echo '<pre>';
		#print_r($response_); #die;


			// remove slots as per Ramadan timings by Aditya on 16.05.2018

			$response_data = $response_;

			$sltst = false;
			foreach($response_data as $ak => $av):
			
				$now = date("H", strtotime($av['start_time']));
				$nowend = date("H", strtotime($av['end_time']));
				
				// remove slots between 17.00 till 20.00
				if ($now >= 13 && $now <= 16) {
					unset($response_data[$ak]);
					$sltno = $av['slot_no'];
					$sltst = true;
				}
				
			endforeach;
			
			#print_r($response_data);
			
			if($sltst == true){
				$response_data = array_reverse($response_data, true);
					#print_r($response_data);
				#pr($response_data); die;
				
				foreach ($response_data as $ak => $av):
				
					// do something here
					$now = date("H", strtotime($av['start_time']));
					$nowend = date("H", strtotime($av['end_time']));
										
					// remove slots after 02.00 at night
					if ($now > 21) {
						unset($response_data[$ak]);
					
					// will leave the foreach loop and also the if statement
					} else {
						
						break;
					}
				endforeach;
			}
			
			#print_r($response_data); die('hi');
			
			$response_data = array_reverse($response_data, true);
			#pr($response_data); die;

			#print_r($response_data); die('hi');

			$response_ = $response_data;
			#echo '<pre>'; print_r($response_); die;
			// remove slots as per Ramadan timings by Aditya on 16.05.2018
			




        if ($back_to_back == 1) {

            if (is_array($response_) && count($response_) > 0) {

                $newStartTime = new DateTime($guest_data[0]['start_time']); //current date/time
                $newStartTime->sub(new DateInterval("PT{$schedularData['Schedular']['travel_time']}M"));
                $start_time = $newStartTime->format('H:i:s');

                // commented by Aditya on 13.04.2017
                #$start_time = $guest_data[0]['start_time'];
                #echo $start_time;
                $available_slots = 0;
                $available_detail = array();
                #print_r($response_);
                #exit('1hi');

                foreach ($response_ as $key => $value) {

                    #if ($value['available_slot'] == 1 && $value['lunch_slot'] == 0 && strtotime($start_time) <= strtotime($value['start_time'])) {
                	// change in Condition by Aditya on 04.06.2018 for Ramadan timings to work
                	if ($value['available_slot'] == 1 && strtotime($start_time) <= strtotime($value['start_time'])) {
						
						#echo $start_time;
						#echo 'if';
						#pr($value);

                        array_push($available_detail, array('start_time' => $value['start_time'], 'end_time' => $value['end_time'], 'slot_no' => $value['slot_no']));
                        $available_slots = $available_slots + 1;
						break;
						
                    } else if ($value['available_slot'] == 0 && strtotime($start_time) <= strtotime($value['start_time'])) {
						#echo 'else if';
                        //$available_slots = 0;
                        //break;
                    } #else
						#echo 'else';
                }

                if (is_array($guest_data) && count($guest_data) > 0) {

                    $require_total_slot = 0;

                    #echo '<pre>';
                    #pr($guest_data);
                    #file_put_contents('/home/public_html/rollingscissor/admin/app/webroot/guestdata.txt', implode("\n", $guest_data) . "\n", FILE_APPEND);


                    foreach ($guest_data as $key => $val) {
						
                        #print_r($key);
                        #print_r($val);
                        
						$service_ = explode(',', $val['service_id']);
                        $comboData = $this->Combo->find('all', array('fields' => array('Combo.id', 'Combo.no_slots')));

                        if (is_array($comboData) && count($comboData) > 0) {
                            $isCombo = false;
                            foreach ($comboData as $key => $value) {
                                $combos = array();
                                foreach ($value['ComboService'] as $key => $va) {
                                    array_push($combos, $va['service_id']);
                                }
                                $checkcombo = array_diff($combos, $service_);
                                if (count($checkcombo) == 0) {
                                    $isCombo = true;
                                    $comboNoSlots = $value['Combo']['no_slots'];
                                }
                            }
                        }

                        if (isset($isCombo)) {
                            $gs = $comboNoSlots;
                        } else {
                            /*if ($val['no_slots'] == 0) {
                                $gs = 1;
                            } else {
                                $gs = $val['no_slots'];
                            }*/
							$gs = 1;
                        }

                        $arrays = array();

                        if ($schedularData['Schedular']['maximum_slot_in_row'] < $gs) {
                            $maximum_slots = floor($gs / $schedularData['Schedular']['maximum_slot_in_row']);
                            $gs = $gs + $maximum_slots;
                        }

                        $require_total_slot = $require_total_slot + $gs;
                    }
                }
            }
			
			#echo '<pre>';
            #print_r($available_slots);
            #print_r($require_total_slot);
            #print_r($available_detail); 
            #die('here!123');
			
            // change this later as done
            if (true == true) {
            #if (($available_slots >= $require_total_slot)) { //  or ($available_slots >= $require_total_slot - 1) 
			
                #echo 'av details=';
                #print_r($available_detail); #exit('21');

                $this->Appointment->create();
                $appoint['id'] = NULL;
                $appoint['customer_id'] = $customer_id;
                #$appoint['start_time'] = '00:00:00';
                #$appoint['end_time'] = '00:00:00';
				
				$appoint['start_time'] = $available_detail[0]['start_time'];
                $appoint['end_time'] = $available_detail[0]['end_time'];
				
                $appoint['admin_id'] = $admin_id;
                $appoint['device_type'] = $device_type;
                $appoint['appointment_date'] = $appointment_date;
                $appoint['schedular_type'] = $schedularData['Schedular']['schedular_type'];
                $appoint['schedular_id'] = $schedularData['Schedular']['id'];
				
				#echo '<br> -- appt save -- <br>';
				#print_r($appoint);
				#die('m here!');
				#if (true == true) {
                if ($this->Appointment->save($appoint)) {
                    // print_r($available_detail);  #exit;
                    $guest_ids = array();
                    $gstid = NULL;
                    $travelling = false;
                    #$kei = 0;
                    #pr($guest_data);
					
					#echo 'guest='.count($guest_data);

                    foreach ($guest_data as $ke => $g) {
						
						/*
						#pr($g);
						
                        $arrays = array();

                        if (isset($available_detail[$ke]['slot_no'])) {

                            $gt = $available_detail[$ke]['slot_no'];
                            for ($i = 0; $i < $g['no_slots']; $i++) {
                                array_push($arrays, $gt);
                                $gt++;
                            }
                            $service_ = explode(',', $g['service_id']);

                            if (isset($comboData) && is_array($comboData) && count($comboData) > 0) {
                                $isCombo = false;

                                foreach ($comboData as $key => $value) {
                                    $combos = array();
                                    foreach ($value['ComboService'] as $key => $va) {
                                        array_push($combos, $va['service_id']);
                                    }
                                    $checkcombo = array_diff($combos, $service_);
                                    if (count($checkcombo) == 0) {
                                        $isCombo = true;
                                        $comboNoSlots = $value['Combo']['no_slots'];
                                    }
                                }
                            }

                            if (isset($isCombo)) {
                                $gs = $comboNoSlots;
                            } else {
								
								#$gs = 1;
								
								
								//// Commented by Aditya on 11.09.2017
								//if ($g['no_slots'] == 0) {
									//$gs = 1;
								//} else {
									//$gs = $g['no_slots'];
								//}
								
								
								$gs = count($guest_data);
								
								
                            }


                            if ($schedularData['Schedular']['maximum_slot_in_row'] < $gs) {
                                $maximum_slots = floor($gs / $schedularData['Schedular']['maximum_slot_in_row']);
                                $gs = $gs + $maximum_slots;
                                #echo 'mx='.$maximum_slots.' '.'gs>'.$gs;
                            }
                            $appointDetail = array();
                            $appointmentDetailId = array();
                            $appointment_time = array();
							*/
							
							$appointmentDetailId = array();
                           	#echo 'ke='.count($ke);
                            #pr($ke);
                            #print_r($available_detail);
                            #die('3h');
                            #echo 'gs='.$gs;

                            #for ($i = 0; $i < $gs; $i++) {

                                #pr($available_detail);
								
								// Calculate Appt Service Slots & Minutes required by per Service - No Slot, and accordingly for each Guest
								// by Aditya on 12.09.2017 -- Apple Event live at moment, iPhone 8, 8 Plus and iPhone X launching now :)
								$ttlSrvcSlots = 0;
								foreach ($g['serviceIdArray'] as $k => $s) {
									
									$srvcData = $this->Service->find('first', array('fields' => array('no_slot'), 'conditions' => array('id' => $s)));
									$ttlSrvcSlots += $srvcData['Service']['no_slot'];
									#pr($ttlSrvcSlots);
									
								}
								
								$ttlSrvcMntCnt = ($ttlSrvcSlots * $schedularData['Schedular']['slot_mins']);
								
								#echo $ttlSrvcSlots;
								#pr($g);
								#die;

                                $this->AppointmentDetail->create();
                                $appointDetail['id'] = NULL;
                                $appointDetail['guest_id'] = $g['guest_id'];

                                array_push($guest_ids, $g['guest_id']);


                                // Travel Start/End time calculations
                                #if ($i == 0 && $travelling == false) {
								if ($travelling == false) {

                                    $appointDetail['travel_start_time'] = $travelStartTime = date("H:i:s", strtotime($available_detail[0]['start_time']));
                                    $now = new DateTime($available_detail[0]['start_time']); //current date/time
                                    $now->add(new DateInterval("PT{$travelTime}M"));
                                    $appointDetail['travel_end_time'] = $travelEndTime = $now->format('H:i:s');
                                } else {

                                    $appointDetail['travel_start_time'] = $appointDetail['travel_end_time'] = $travelStartTime = $travelEndTime = '00:00:00';
                                }
								
								

                                // Appointments Start/End time calculations
								
                                #if ($i == 0 && $travelling == false) {
								if ($travelling == false) {
									
                                    $appointDetail['start_time'] = $travelEndTime;
									
									$now = new DateTime($travelEndTime); //current date/time
									$now->add(new DateInterval("PT{$ttlSrvcMntCnt}M"));
                                    $appointDetail['end_time'] = $now->format('H:i:s');
									
                                    #$now = new DateTime($available_detail[0]['end_time']); //current date/time
                                    #$now->sub(new DateInterval("PT{$cleaningTime}M"));
                                    #$appointDetail['end_time'] = $now->format('H:i:s');
									
                                } else {

									//if ($g['no_slots'] > 1) {
										//$kei++;
									//}
									
									#echo 'kei='.$kei;
									#pr($available_detail);

                                    #$appointDetail['start_time'] = $available_detail[$kei]['start_time'];
									#$appointDetail['start_time'] = $appointDetail['travel_end_time'];
									
									$appointDetail['start_time'] = $appointDetail['clean_end_time'];
									
									$now = new DateTime($appointDetail['start_time']); //current date/time
									$now->add(new DateInterval("PT{$ttlSrvcMntCnt}M"));
                                    $appointDetail['end_time'] = $now->format('H:i:s');
									
                                    #$now = new DateTime($available_detail[0]['end_time']); //current date/time
                                    #$now->sub(new DateInterval("PT{$cleaningTime}M"));
                                    #$appointDetail['end_time'] = $now->format('H:i:s');
                                }
								
                                // Cleaning Start/End time calculations
                                if ($gstid != $g['guest_id']) {
                                    $appointDetail['clean_start_time'] = $cleanStartTime = ($appointDetail['end_time']);
                                    $now = new DateTime($appointDetail['end_time']); //current date/time
                                    $now->add(new DateInterval("PT{$cleaningTime}M"));
                                    $appointDetail['clean_end_time'] = $cleanEndTime = $now->format('H:i:s');
                                }
								
                                #pr($available_detail);
                                #echo '<br>';
								
                                $appointDetail['slot_no'] = $available_detail[0]['slot_no'];
                                $appointDetail['schedular_id'] = $schedular_id;
                                $appointDetail['customer_id'] = $customer_id;
                                $appointDetail['appointment_id'] = $this->Appointment->id;
                                $appointDetail['appointment_date'] = $appointment_date;
                                if ($admin_id > 0) {
                                    $appointDetail['appointment_confirmed'] = '1';
                                } else {
                                    $appointDetail['appointment_confirmed'] = '0';
                                }
								
								#echo '<br> -- details save -- <br>';
                                #print_r($appointDetail);
								
                                $this->AppointmentDetail->save($appointDetail);
                                array_push($appointmentDetailId, $this->AppointmentDetail->id);

                                $travelling = true;
                                $gstid = $g['guest_id'];

                                //unset($available_detail[$i]);
                            #}

                            #die('3h');

                            $available_detail = array_values($available_detail);
							#pr($g['serviceIdArray']); die;
							#$g['service_id'] = explode(',', $g['serviceIdArray']);
							#pr($g['service_id']);
							#die;
							foreach ($g['serviceIdArray'] as $k => $s) {
								$serviceData = array();
								$this->AppointmentService->create();
								$serviceData['service_id'] = $s;
								$serviceData['guest_id'] = $g['guest_id'];
								$serviceData['appointment_id'] = $this->Appointment->id;
								$serviceData['appointmentdetail_id'] = $this->AppointmentDetail->id;
								$this->AppointmentService->save($serviceData);
							}
							

                            /*if (!empty($g['serviceIdArray']) && $g['serviceIdArray'] > 0) {

                                $g['service_id'] = explode(',', $g['serviceIdArray']);

                                if (is_array($g['service_id']) && count($g['service_id']) > 0) {

                                    foreach ($g['service_id'] as $k => $s) {
                                        $serviceData = array();
                                        $this->AppointmentService->create();
                                        $serviceData['service_id'] = $s;
                                        $serviceData['guest_id'] = $g['guest_id'];
                                        $serviceData['appointment_id'] = $this->Appointment->id;
                                        $serviceData['appointmentdetail_id'] = $this->AppointmentDetail->id;
                                        $this->AppointmentService->save($serviceData);
                                    }
                                }
                            }*/
							
							
							#pr($g);
                            #die('here now!');
							
							
                        }

                        #$kei++;
                    }
/*
                    $startData = $this->AppointmentDetail->find('first', array('fields' => array('travel_start_time'), 'conditions' => array('appointment_id' => $this->Appointment->id), 'order' => array('AppointmentDetail.id' => 'asc')));
                    $endData = $this->AppointmentDetail->find('first', array('fields' => array('clean_end_time'), 'conditions' => array('appointment_id' => $this->Appointment->id), 'order' => array('AppointmentDetail.id' => 'desc')));
					
					#echo '<pre>';
                    #print_r($startData);
                    #print_r($endData);
                    #die('here i m ');
                    // Save the Appointment start/end time and update Appt table
					#if($startData['AppointmentDetail']['travel_start_time'] && $endData['AppointmentDetail']['clean_end_time'])
						$this->Appointment->updateAll(array('Appointment.start_time' => "'" . $startData['AppointmentDetail']['travel_start_time'] . "'", 'Appointment.end_time' => "'" . $endData['AppointmentDetail']['clean_end_time'] . "'"), array('Appointment.id' => $this->Appointment->id));
*/

                    /*
                      $date_start = $startData['AppointmentDetail']['travel_start_time'];
                      $date_end = $endData['AppointmentDetail']['clean_end_time'];
                      $conditions = array(
                      'conditions' => array(
                      'Appointment.start_time >=' => array($date_start),
                      'Appointment.end_time <=' => array($date_end),
                      'Appointment.id !=' => $this->Appointment->id,
                      ));
                      $this->set('appointments', $this->Appointment->find('first', $conditions));
                     */


					#die('here i m ');

                    $response_data['success'] = true;
					
					
					
					
                #}
            } else if ($available_slots < $require_total_slot) {

                $response_data['success'] = false;
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['error'],
                    'message' => $GLOBALS['Webservice']['messages']['required_slot_not_avaliable'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['required_slot_not_avaliable'],
                    'data' => $response_data
                );
                return $this->fail_safe_return($response);
            } else {

                $response_data['success'] = false;
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['error'],
                    'message' => $GLOBALS['Webservice']['messages']['appointment_booked_already'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_booked_already'],
                    'data' => $response_data
                );
                return $this->fail_safe_return($response);
            }


            // Random appointment booking, not back to back
        } else {

            $guest_datas = array();
            foreach ($guest_data as $key => $row) {
                $guest_datas[$key] = $row['slots_no'];
            }
            array_multisort($guest_datas, SORT_ASC, $guest_data);

            $available_detail = array();
            if (is_array($guest_data) && count($guest_data) > 0) {
                $available = true;

                foreach ($guest_data as $key => $val) {
                    if (is_array($response_) && count($response_) > 0) {
                        $start_time = $val['start_time'];
                        $no_slots = $val['no_slots'];
                        $appointment_date = $val['appointment_date'];
                        $available_slots = 0;

                        foreach ($response_ as $key => $value) {

                            // print_r($value);

                            if ($value['available_slot'] == 1 && $value['lunch_slot'] == 0 && strtotime($start_time) == strtotime($value['start_time'])) {
                                for ($i = 0; $i < $no_slots; $i++) {
                                    $available_slots = $available_slots + 1;
                                }
                            } else if ($value['available_slot'] == 0 && strtotime($start_time) == strtotime($value['start_time'])) {
                                $available_slots = 0;
                                break;
                            }
                        }
                        if ($no_slots != $available_slots) {
                            $available = false;
                        }
                    }
                }

                if ($available == true) {

                    $this->Appointment->create();
                    $appoint['customer_id'] = $customer_id;
                    $appoint['device_type'] = $device_type;
                    if ($this->Appointment->save($appoint)) {


                        $test = array();
                        foreach ($response_ as $key => $val) {
                            $test[$key + 1] = $val;
                        }
                        $guest_ids = array();
                        foreach ($guest_data as $ke => $g) {
                            $appointmentBooked = $this->AppointmentDetail->find('all', array('fields' => array('slot_no'), 'conditions' => array('AppointmentDetail.appointment_date' => $g['appointment_date'])));
                            $appointmentBooked = Set::extract('{n}.AppointmentDetail.slot_no', $appointmentBooked);
                            if (is_array($appointmentBooked) && count($appointmentBooked) > 0) {
                                $final_available_slot = array_diff($available_slots_array, $appointmentBooked);
                            } else {
                                $final_available_slot = $available_slots_array;
                            }


                            if (in_array($g['slots_no'], $final_available_slot)) {
                                $g['slots_no'] = $g['slots_no'];
                            } else {
                                $final_available_slot = array_values($final_available_slot);
                                $g['slots_no'] = $final_available_slot[0];
                            }
                            $arrays = array();
                            $gt = $g['slots_no'];
                            for ($i = 0; $i < $g['no_slots']; $i++) {
                                array_push($arrays, $gt);
                                $gt++;
                            }
                            //lunch slot checking
                            if (in_array($slot_lunch, $arrays)) {
                                $gs = $g['no_slots'] + 1;
                            } else {
                                $gs = $g['no_slots'];
                            }

                            if ($schedularData['Schedular']['maximum_slot_in_row'] < $gs) {
                                $maximum_slots = floor($gs / $schedularData['Schedular']['maximum_slot_in_row']);
                                $gs = $gs + $maximum_slots;
                            }

                            for ($i = 0; $i < $gs; $i++) {


                                $this->AppointmentDetail->create();
                                $appointDetail['guest_id'] = $g['guest_id'];
                                array_push($guest_ids, $g['guest_id']);
                                if ($i > 0) {
                                    $g['slots_no'] = $g['slots_no'] + 1;
                                }
                                $appointDetail['slot_no'] = $g['slots_no'];

                                $appointDetail['travel_start_time'] = $test[$g['slots_no']]['travel_start_time'];
                                $appointDetail['travel_end_time'] = $test[$g['slots_no']]['travel_end_time'];

                                $appointDetail['start_time'] = $test[$g['slots_no']]['start_time'];
                                $appointDetail['end_time'] = $test[$g['slots_no']]['end_time'];

                                $appointDetail['clean_start_time'] = $test[$g['slots_no']]['clean_start_time'];
                                $appointDetail['clean_end_time'] = $test[$g['slots_no']]['clean_end_time'];

                                $appointDetail['customer_id'] = $customer_id;
                                $appointDetail['schedular_id'] = $schedular_id;
                                $appointDetail['appointment_id'] = $this->Appointment->id;
                                if ($admin_id > 0) {
                                    $appointDetail['appointment_confirmed'] = '1';
                                } else {
                                    $appointDetail['appointment_confirmed'] = '0';
                                }
                                $appointDetail['appointment_date'] = $g['appointment_date'];
                                $this->AppointmentDetail->save($appointDetail);
                            }


                            if (!empty($g['service_id']) && $g['service_id'] > 0) {
                                $g['service_id'] = explode(',', $g['service_id']);
                                if (is_array($g['service_id']) && count($g['service_id']) > 0) {
                                    foreach ($g['service_id'] as $k => $s) {
                                        $serviceData = array();
                                        $this->AppointmentService->create();
                                        $serviceData['service_id'] = $s;
                                        $serviceData['guest_id'] = $g['guest_id'];
                                        $serviceData['appointment_id'] = $this->Appointment->id;
                                        $serviceData['appointmentdetail_id'] = $this->AppointmentDetail->id;
                                        $this->AppointmentService->save($serviceData);
                                    }
                                }
                            }
                        }
                        $response_data['success'] = true;
                    }
                } else {
                    $response_data['success'] = false;
                    $response = array(
                        'status' => $GLOBALS["Webservice"]["codes"]['error'],
                        'message' => $GLOBALS['Webservice']['messages']['appointment_booked_already'],
                        'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_booked_already'],
                        'data' => $response_data
                    );
                    return $this->fail_safe_return($response);
                }
            }
        }

        if (!empty($response_data)) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['appointment_booked_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_booked_success'],
                'data' => $response_data
            );
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['appointment_booked_already'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_booked_already'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    public function add_packages_appointment($api_token = '', $admin_id = '', $appointment_date = '', $appointment_time = '', $package_id = '', $customer_id = '', $payment_method = '', $deviceid = '', $device_type = '') {

        $timezone_time = '';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $response_data = array();
        $response_ = array();

        /*
          $response = $this->Schedular->find('all', array('conditions' => array('Schedular.end_date >=' => date('Y-m-d'))));
          #pr($response); #die;

          if (is_array($response) && count($response) > 0) {

          $schedular_id = 0;
          foreach ($response as $key => $value) {
          $start_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $appointment_date);
          $end_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $appointment_date);
          if ($start_date_fall == true) {
          $schedular_id = $value['Schedular']['id'];
          continue;
          }
          if ($end_date_fall == true) {
          $schedular_id = $value['Schedular']['id'];
          continue;
          }
          }
          }

          $options['conditions'] = array(
          "Schedular.id" => $schedular_id,
          );
          $schedularData = $this->Schedular->find("first", $options);

         */
        #echo $schedular_id;
        #die;


        $schedularData = $this->get_schedular_from_date($appointment_date);
        #pr($schedularData);
        #die;

        if (empty($schedularData)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['no_schedular_set'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_schedular_set'],
                'data' => NULL
            );
            return $this->fail_safe_return($response);
        }

        $schedular_id = $schedularData['Schedular']['id'];


        $options['conditions'] = array(
            "Package.id" => $package_id,
        );
        $packageData = $this->Package->find("first", $options);
        #pr($packageData);
        #pr($schedularData);
        #die('1');

        $available_slots_array = array();

        if (is_array($schedularData) && count($schedularData) > 0) {

            $total_slot_time = $schedularData['Schedular']['travel_time'] + $schedularData['Schedular']['cleaning_time'] + $schedularData['Schedular']['slot_mins'];
            #$start_time = $schedularData['Schedular']['start_time'];
            $start_time = date('H:i:s', strtotime($appointment_time));
            $schedular_id = $schedularData['Schedular']['id'];
            $slot_lunch = $schedularData['Schedular']['slot_lunch'];

            #for ($i = 0; $i < $schedularData['Schedular']['maximum_appointment']; $i++) {
            for ($i = 0; $i < $packageData['Package']['no_slot']; $i++) {

                $responses[$i]['start_time'] = $start_time;
                $end_time = strtotime("+$total_slot_time minutes", strtotime($start_time));
                $responses[$i]['end_time'] = date('H:i:s', $end_time);
                $responses[$i]['schedular_id'] = $schedular_id;
                #$responses[$i]['slot_no'] = (string) ($i + 1);
                $responses[$i]['slot_no'] = (string) ($i + 1);

                if ($slot_lunch == ($i + 1)) {
                    $responses[$i]['lunch_slot'] = "1";
                } else {
                    $responses[$i]['lunch_slot'] = "0";
                }

                $appointCount = $this->AppointmentDetail->find('count', array('conditions' => array('AppointmentDetail.appointment_date' => $appointment_date, 'AppointmentDetail.slot_no' => $responses[$i]['slot_no'])));
                if ($appointCount > 0) {
                    $responses[$i]['available_slot'] = "0";
                } else {
                    $responses[$i]['available_slot'] = "1";
                }
                array_push($available_slots_array, $responses[$i]['slot_no']);

                $start_time = date('H:i:s', $end_time);
                array_push($response_, $responses[$i]);
            }
        }

        /* pr($responses);

          $lastSlotStartime = end($response_);
          pr($response_);
          pr($lastSlotStartime);
          pr($appointment_time);
          echo date('Y-m-d');
          echo $appointment_date;
          #die; */

        $isTodayDate = false;
        if (strtotime($appointment_date) == strtotime(date('Y-m-d'))) {

            $isTodayDate = true;

            if (strtotime($lastSlotStartime['start_time']) <= strtotime($appointment_time)) {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['error'],
                    'message' => $GLOBALS['Webservice']['messages']['slot_time_passed'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['slot_time_passed'],
                    'data' => new stdClass()
                );
                return $this->fail_safe_return($response);
            }
        } else {
            /*
              echo ' '.strtotime($lastSlotStartime['start_time']) .'-'. strtotime($appointment_time);

              die;
              if (strtotime($lastSlotStartime['start_time']) <= strtotime($appointment_time)) {

              $response = array(
              'status' => $GLOBALS["Webservice"]["codes"]['error'],
              'message' => $GLOBALS['Webservice']['messages']['slot_time_passed'],
              'message_ar' => $GLOBALS['Webservice']['messages_ar']['slot_time_passed'],
              'data' => new stdClass()
              );
              return $this->fail_safe_return($response);
              } */
        }

        #die('here!');
        // Check Appointment End date/Time
        $apptTimeReq = ($packageData['Package']['no_slot'] * $schedularData['Schedular']['slot_mins']);
        $travelTime = $schedularData['Schedular']['travel_time'];
        $cleaningTime = $schedularData['Schedular']['cleaning_time'];

        $apptTime = ( $apptTimeReq + $travelTime + $cleaningTime );

        $now = new DateTime(date('H:i:s', strtotime($schedularData['Schedular']['end_time']))); //current date/time
        $now->sub(new DateInterval("PT{$apptTime}M"));
        $min_end_time = $now->format('H:i:s');
        #echo $appointment_date.' '.date('H:i:s', strtotime($appointment_time)).' '.$apptTime.' '.$min_end_time.' ';

        if (strtotime($min_end_time) < strtotime(date('H:i:s', strtotime($appointment_time)))) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['min_slot_time_error'] . $min_end_time,
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['min_slot_time_error'] . $min_end_time,
                'data' => array('time' => date('H:i', strtotime($min_end_time)))
            );
            return $this->fail_safe_return($response);
        }
        // Check Appointment End date/Time
        #pr($packageData);
        #pr($schedularData);
        #die('hey1thr');


        $appointmentData = $this->AppointmentDetail->find('count', array('conditions' => array('AppointmentDetail.appointment_confirmed !=' => '-1', 'AppointmentDetail.appointment_date' => $appointment_date)));

        // Full Day Booking
        if ($appointmentData == 0) {
            if (is_array($response_) && count($response_) > 0) {

                $this->Appointment->create();

                $appoint['start_time'] = '00:00:00';
                $appoint['end_time'] = '00:00:00';
                $appoint['payment_method'] = $payment_method;
                $appoint['customer_id'] = $customer_id;
                $appoint['device_type'] = $device_type;
                $appoint['appointment_date'] = $appointment_date;
                $appoint['type'] = 1;
                $appoint['schedular_type'] = $schedularData['Schedular']['schedular_type'];
                $appoint['schedular_id'] = $schedularData['Schedular']['id'];

                if ($this->Appointment->save($appoint)) {

                    #pr($response_);
                    #die('2here');

                    if (strtotime($appointment_time) <= strtotime($response_[0]['start_time']) || $isTodayDate == false) {

                        $this->AppointmentDetail->create();
                        $appointDetail['guest_id'] = '1';


                        // Travel Start/End time calculations
                        $appointDetail['travel_start_time'] = $travelStartTime = date("H:i:s", strtotime($appointment_time));
                        $now = new DateTime($appointment_time); //current date/time
                        $now->add(new DateInterval("PT{$travelTime}M"));
                        $appointDetail['travel_end_time'] = $travelEndTime = $now->format('H:i:s');

                        // Appointments Start/End time calculations
                        $appointDetail['start_time'] = $travelEndTime;
                        $now = new DateTime($travelEndTime); //current date/time
                        $now->add(new DateInterval("PT{$apptTimeReq}M"));
                        $appointDetail['end_time'] = $now->format('H:i:s');

                        // Cleaning Start/End time calculations
                        $appointDetail['clean_start_time'] = $cleanStartTime = ($appointDetail['end_time']);
                        $now = new DateTime($appointDetail['end_time']); //current date/time
                        $now->add(new DateInterval("PT{$cleaningTime}M"));
                        $appointDetail['clean_end_time'] = $cleanEndTime = $now->format('H:i:s');


                        $appointDetail['slot_no'] = $packageData['Package']['no_slot'];
                        $appointDetail['type'] = '1';
                        $appointDetail['schedular_id'] = $schedular_id;
                        $appointDetail['customer_id'] = $customer_id;
                        $appointDetail['appointment_id'] = $this->Appointment->id;
                        $appointDetail['appointment_date'] = $appointment_date;
                        if ($admin_id > 0) {
                            $appointDetail['appointment_confirmed'] = '1';
                        } else {
                            $appointDetail['appointment_confirmed'] = '0';
                        }
                        $this->AppointmentDetail->save($appointDetail);

                        if (!empty($package_id) && $package_id > 0) {

                            $packageData = array();
                            $this->AppointmentPackage->create();
                            $packageData['package_id'] = $package_id;
                            $packageData['guest_id'] = '1';
                            $packageData['appointment_id'] = $this->Appointment->id;
                            $packageData['appointmentdetail_id'] = $this->AppointmentDetail->id;
                            $this->AppointmentPackage->save($packageData);
                        }
                    }

                    /*
                      $i = 0;

                      foreach ($response_ as $ke => $g) {

                      pr($response_);
                      pr($ke);
                      pr($g);
                      die('2here');

                      if (strtotime($appointment_time) <= strtotime($g['start_time']) || $isTodayDate == false) {

                      $this->AppointmentDetail->create();
                      $appointDetail['guest_id'] = '1';


                      $appointDetail['travel_start_time'] = $g['travel_start_time'];
                      $appointDetail['travel_end_time'] = $g['travel_end_time'];

                      $appointDetail['start_time'] = $g['start_time'];
                      $appointDetail['end_time'] = $g['end_time'];

                      $appointDetail['clean_start_time'] = $g['clean_start_time'];
                      $appointDetail['clean_end_time'] = $g['clean_end_time'];

                      #$appointDetail['start_time'] = $g['start_time'];
                      #$appointDetail['end_time'] = $g['end_time'];



                      // Travel Start/End time calculations
                      $appointDetail['travel_start_time'] = $travelStartTime = date("H:i:s", strtotime($appointment_time));
                      $now = new DateTime($appointment_time); //current date/time
                      $now->add(new DateInterval("PT{$travelTime}M"));
                      $appointDetail['travel_end_time'] = $travelEndTime = $now->format('H:i:s');

                      // Appointments Start/End time calculations
                      $appointDetail['start_time'] = $travelEndTime;
                      $now = new DateTime($travelEndTime); //current date/time
                      $now->add(new DateInterval("PT{$apptTimeReq}M"));
                      $appointDetail['end_time'] = $now->format('H:i:s');

                      // Cleaning Start/End time calculations
                      $appointDetail['clean_start_time'] = $cleanStartTime = ($appointDetail['end_time']);
                      $now = new DateTime($appointDetail['end_time']); //current date/time
                      $now->add(new DateInterval("PT{$cleaningTime}M"));
                      $appointDetail['clean_end_time'] = $cleanEndTime = $now->format('H:i:s');


                      $appointDetail['slot_no'] = $g['slot_no'];
                      $appointDetail['type'] = '1';
                      $appointDetail['schedular_id'] = $schedular_id;
                      $appointDetail['customer_id'] = $customer_id;
                      $appointDetail['appointment_id'] = $this->Appointment->id;
                      $appointDetail['appointment_date'] = $appointment_date;
                      if ($admin_id > 0) {
                      $appointDetail['appointment_confirmed'] = '1';
                      } else {
                      $appointDetail['appointment_confirmed'] = '0';
                      }
                      $this->AppointmentDetail->save($appointDetail);

                      if (!empty($package_id) && $package_id > 0) {

                      $packageData = array();
                      $this->AppointmentPackage->create();
                      $packageData['package_id'] = $package_id;
                      $packageData['guest_id'] = '1';
                      $packageData['appointment_id'] = $this->Appointment->id;
                      $packageData['appointmentdetail_id'] = $this->AppointmentDetail->id;
                      $this->AppointmentPackage->save($packageData);
                      }
                      }
                      $i++;
                      }
                     */

                    $startData = $this->AppointmentDetail->find('first', array('fields' => array('travel_start_time'), 'conditions' => array('appointment_id' => $this->Appointment->id), 'order' => array('AppointmentDetail.id' => 'asc')));
                    $endData = $this->AppointmentDetail->find('first', array('fields' => array('clean_end_time'), 'conditions' => array('appointment_id' => $this->Appointment->id), 'order' => array('AppointmentDetail.id' => 'desc')));

                    $this->Appointment->updateAll(array('Appointment.start_time' => "'" . $startData['AppointmentDetail']['travel_start_time'] . "'",
                        'Appointment.end_time' => "'" . $endData['AppointmentDetail']['clean_end_time'] . "'"), array('Appointment.id' => $this->Appointment->id));

                    if ($admin_id > 0) {
                        $orderResponse = $this->checkout_order($api_token = '123', $deviceid = 'admin', $device_type = '2', $user_id = $customer_id, $admin_id = $admin_id, $address_id = '1', $appointment_id = $this->Appointment->id, $guest_id = 1, $payment_method = 2, $total_amount = '0.000', $remaining_amount = '0.000', $transaction_id = '23523523523', $timezone_time);
                        if ($orderResponse['status'] == 0) {
                            $response_data['success'] = true;
                        }
                    } else {
                        $response_data['success'] = true;
                    }
                }
            }
        }

        #pr($response_data);
        #die;

        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['appointment_booked_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_booked_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['appointment_booked_already'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_booked_already'],
                'data' => new stdClass()
            );
        }

        return $this->fail_safe_return($response);
    }

    public function get_admin_appointment_daywise_listing($api_token = '', $appointment_date = '', $deviceid = '', $device_type = '') {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $response_data = array();
        $response = $this->Schedular->find('all');
        if (is_array($response) && count($response) > 0) {

            $schedular_id = 0;
            foreach ($response as $key => $value) {
                $start_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $appointment_date);
                $end_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $appointment_date);
                if ($start_date_fall == true) {
                    $schedular_id = $value['Schedular']['id'];
                    continue;
                }
                if ($end_date_fall == true) {
                    $schedular_id = $value['Schedular']['id'];
                    continue;
                }
            }
        }

        if ($schedular_id == 0) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['no_schedular_set'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_schedular_set'],
                'data' => $response_data
            );
            return $this->fail_safe_return($response);
        }


        $options['conditions'] = array(
            "Schedular.id" => $schedular_id,
        );
        $schedularData = $this->Schedular->find("first", $options);

        if (is_array($schedularData) && count($schedularData) > 0) {
            $total_slot_time = $schedularData['Schedular']['travel_time'] + $schedularData['Schedular']['cleaning_time'] + $schedularData['Schedular']['slot_mins'];
            $start_time = $schedularData['Schedular']['start_time'];
            $schedular_id = $schedularData['Schedular']['id'];
            $slot_lunch = $schedularData['Schedular']['slot_lunch'];
            for ($i = 0; $i < $schedularData['Schedular']['maximum_appointment']; $i++) {
                $responses[$i]['start_time'] = $start_time;
                $end_time = strtotime("+$total_slot_time minutes", strtotime($start_time));
                $responses[$i]['end_time'] = date('H:i', $end_time);
                $responses[$i]['schedular_id'] = $schedular_id;
                $responses[$i]['appointment_date'] = $appointment_date;
                $responses[$i]['slot_no'] = (string) ($i + 1);
                if ($slot_lunch == ($i + 1)) {
                    $responses[$i]['lunch_slot'] = "1";
                } else {
                    $responses[$i]['lunch_slot'] = "0";
                }
                $start_time = date('H:i', $end_time);
                array_push($response_data, $responses[$i]);
            }
        }

        if (is_array($response_data) && count($response_data) > 0) {

            foreach ($response_data as $key => $value) {
                if (!empty($value['slot_no'])) {
                    $options['conditions'] = array(
                        "AppointmentDetail.slot_no" => $value['slot_no'],
                        'AppointmentDetail.appointment_date' => $appointment_date,
                        'AppointmentDetail.appointment_confirmed !=' => '-1'
                    );
                    $options['fields'] = array(
                        "AppointmentDetail.start_time", "AppointmentDetail.appointment_id", "AppointmentDetail.end_time", "AppointmentDetail.customer_id",
                        "AppointmentDetail.appointment_date", "AppointmentDetail.type", 'Appointment.admin_id', "AppointmentDetail.slot_no", "Customer.mobile", "Customer.f_name",
                        "Customer.l_name", "Customer.email", 'AppointmentDetail.id', 'AppointmentDetail.guest_id'
                    );
                    $appointmentData = $this->AppointmentDetail->find("first", $options);


                    $appoint = array();
                    $response_data[$key]['available_slot'] = "1";
                    $response_data[$key]['each_slot_time'] = (string) $total_slot_time;
                    $response_data[$key]['appointment_data'] = new stdClass();
                    if (isset($appointmentData['AppointmentDetail']['id'])) {


                        if (isset($appointmentData['OrderData'][0]['order_id']) && !empty($appointmentData['OrderData'][0]['order_id'])) {
                            $orderStatus = $this->Order->find('first', array('fields' => array('Order.status'), 'conditions' => array('Order.id' => $appointmentData['OrderData'][0]['order_id'])));


                            $appoint['status'] = $orderStatus['Order']['status'];
                            $appoint['order_id'] = $appointmentData['OrderData'][0]['order_id'];
                        }


                        $appoint['start_time'] = $appointmentData['AppointmentDetail']['start_time'];
                        $appoint['admin_id'] = $appointmentData['Appointment']['admin_id'];
                        $appoint['end_time'] = $appointmentData['AppointmentDetail']['end_time'];
                        $appoint['appointment_id'] = $appointmentData['AppointmentDetail']['appointment_id'];


                        $appoint['customer_id'] = $appointmentData['AppointmentDetail']['customer_id'];
                        $appoint['mobile'] = $appointmentData['Customer']['mobile'];
                        $appoint['f_name'] = $appointmentData['Customer']['f_name'];
                        $appoint['l_name'] = $appointmentData['Customer']['l_name'];
                        $appoint['email'] = $appointmentData['Customer']['email'];
                        $appoint['appointment_date'] = $appointmentData['AppointmentDetail']['appointment_date'];
                        $appoint['slot_no'] = $appointmentData['AppointmentDetail']['slot_no'];
                        $appoint['guest_id'] = $appointmentData['AppointmentDetail']['guest_id'];
                        $appoint['appointmentdetail_id'] = $appointmentData['AppointmentDetail']['id'];
                        $appoint['service_name'] = "";
                        $appoint['service_name_ar'] = "";
                        $appoint['package_name'] = "";
                        $appoint['package_name_ar'] = "";
                        if ($appointmentData['AppointmentDetail']['type'] == 0) {
                            $appoint['type'] = 'Service';
                        } else {
                            $appoint['type'] = 'Package';
                        }
                        if (is_array($appointmentData['Service']) && count($appointmentData['Service']) > 0) {
                            $service_name = array();
                            $service_name_ar = array();
                            foreach ($appointmentData['Service'] as $y => $l) {
                                array_push($service_name, $l['name']);
                                array_push($service_name_ar, $l['name_ar']);
                            }
                            $appoint['service_name'] = implode(',', $service_name);
                            $appoint['service_name_ar'] = implode(',', $service_name_ar);
                        } else {
                            if ($appointmentData['AppointmentDetail']['type'] == 0) {
                                if (isset($service_name) && is_array($service_name) && count($service_name) > 0) {
                                    $appoint['service_name'] = implode(',', $service_name);
                                    $appoint['service_name_ar'] = implode(',', $service_name_ar);
                                } else {
                                    $serviceData = $this->OrderService->find('all', array('conditions' => array('OrderService.guest_id' => $appointmentData['AppointmentDetail']['guest_id'], 'OrderService.appointment_id' => $appointmentData['AppointmentDetail']['appointment_id'])));

                                    if (isset($serviceData) && is_array($serviceData) && count($serviceData) > 0) {
                                        $service_name = array();
                                        $service_name_ar = array();

                                        foreach ($serviceData as $y => $l) {
                                            array_push($service_name, $l['Service']['name']);
                                            array_push($service_name_ar, $l['Service']['name_ar']);
                                        }

                                        $appoint['service_name'] = implode(',', $service_name);
                                        $appoint['service_name_ar'] = implode(',', $service_name_ar);
                                    }
                                }
                            }
                        }
                        if (is_array($appointmentData['Package']) && count($appointmentData['Package']) > 0) {
                            $package_name = array();
                            $package_name_ar = array();
                            foreach ($appointmentData['Package'] as $y => $l) {
                                array_push($package_name, $l['name']);
                                array_push($package_name_ar, $l['name_ar']);
                            }
                            $appoint['package_name'] = implode(',', $package_name);
                            $appoint['package_name_ar'] = implode(',', $package_name_ar);
                        }
                        $response_data[$key]['available_slot'] = "0";
                        $response_data[$key]['appointment_data'] = $appoint;
                    }
                }
            }
        }

        if (!empty($response_data)) {
            $response = array();
            foreach ($response_data as $key => $value) {
                $response[] = $value;
            }
            $lastSlotStartime = end($response_data);
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'start_time' => $schedularData['Schedular']['start_time'],
                'end_time' => date('H:i:s', strtotime($lastSlotStartime['start_time'])),
                'data' => $response
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'start_time' => '',
                'end_time' => '',
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function get_admin_appointment_listing_pagewise($api_token = '', $deviceid = '', $device_type = '', $start_date = '', $end_date = '', $pagesize = 10, $page = 0) {

        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');


        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }



        $date_offset = '';
        if (empty($date_offset)) {
            $date_offset = date('Y-m-d H:i:s');
        }

        if ($page > 0) {
            --$page;
        }
        $page = (int) $page;

        $pagesize = (int) $pagesize;
        if ($pagesize <= 0) {
            $pagesize = 1;
        }
        $response_data = array();

        $options = array();



        if (!empty($start_date) && !empty($end_date)) {
            $options['conditions'] = array(
                'Order.appointment_date <=' => "$end_date",
                'Order.appointment_date >=' => "$start_date",
                'Order.status !=' => '-1'
            );
        }



        $total_records = $this->Order->find("count", $options);
		
		/*$dbo = $this->Order->getDatasource();
		$logs = $dbo->getLog();
		$lastLog = end($logs['log']);
		debug( $lastLog['query'] );*/
		
        if ($total_records > 0) {
            $total_pages = ceil($total_records / $pagesize);

            if ($page > $total_pages) {
                $response_data = [];
            } else {

                $offset = $page * $pagesize;
                $limit = $pagesize;

                if (!empty($start_date) && !empty($end_date)) {

                    $getAppointmentData = $this->Order->find("all", array('fields' => array(
                            "Order.start_time", "Order.end_time", "Order.customer_id", "Order.is_van_running", 'Order.is_service_running', "Order.id", "Order.appointment_date", 'Order.type', 'Order.appointment_id', 'Order.receipt_num', 'Order.status', 'Customer.f_name', 'Customer.l_name', 'Customer.mobile', 'Customer.email', 'Order.admin_id'), 'conditions' => array('Order.appointment_date <=' => "$end_date", 'Order.appointment_date >=' => "$start_date"), 'conditions' => array('Order.status !=' => '-1'), 'order' => array('Order.appointment_date' => 'ASC'), 'limit' => $limit, 'offset' => $offset));
					
                } else {

                    $getAppointmentData = $this->Order->find("all", array('fields' => array(
                            "Order.start_time", "Order.end_time", "Order.customer_id", "Order.is_van_running", 'Order.is_service_running', "Order.id", "Order.appointment_date", 'Order.type', 'Order.appointment_id', 'Order.receipt_num', 'Order.status', 'Customer.f_name', 'Customer.l_name', 'Customer.mobile', 'Customer.email', 'Order.admin_id'), 'conditions' => array('Order.status !=' => '-1'), 'order' => array('Order.appointment_date' => 'ASC'), 'limit' => $limit, 'offset' => $offset));
                }

                if (is_array($getAppointmentData) && count($getAppointmentData) > 0) {
                    $appoint = array();
                    foreach ($getAppointmentData as $key => $value) {
                        $appoint[$key]['appointment_id'] = $value['Order']['appointment_id'];
                        $appoint[$key]['order_id'] = $value['Order']['id'];
                        $appoint[$key]['appointment_date'] = $value['Order']['appointment_date'];
                        $appoint[$key]['customer_id'] = $value['Order']['customer_id'];
                        $appoint[$key]['start_time'] = $value['Order']['start_time'];
                        $appoint[$key]['status'] = $value['Order']['status'];
                        $appoint[$key]['end_time'] = $value['Order']['end_time'];
                        $appoint[$key]['is_van_running'] = $value['Order']['is_van_running'];
                        $appoint[$key]['is_service_running'] = $value['Order']['is_service_running'];
                        $appoint[$key]['type'] = $value['Order']['type'];
                        $appoint[$key]['order_pdf_link'] = SITE_URL . 'uploads/invoice/' . $value['Order']['receipt_num'] . '.pdf';
                        $appoint[$key]['customer_name'] = $value['Customer']['f_name'] . ' ' . $value['Customer']['l_name'];
                        $appoint[$key]['mobile'] = $value['Customer']['mobile'];
                        $appoint[$key]['email'] = $value['Customer']['email'];
                        $appoint[$key]['admin_id'] = $value['Order']['admin_id'];
                    }

                    $response_data['appointment_data'] = $appoint;
                    $response_data['total_pages'] = "$total_pages";
                    $response_data['date_offset'] = $date_offset;
                    $page = $page + 1;
                    $response_data['current_page'] = "$page";
                }
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }
	
	function get_admin_appointment_listing_pagewise_v2($api_token = '', $deviceid = '', $device_type = '', $start_date = '', $end_date = '', $pagesize = 10, $page = 0) {

        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');


        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }



        $date_offset = '';
        if (empty($date_offset)) {
            $date_offset = date('Y-m-d H:i:s');
        }

        if ($page > 0) {
            --$page;
        }
        $page = (int) $page;

        $pagesize = (int) $pagesize;
        if ($pagesize <= 0) {
            $pagesize = 1;
        }
        $response_data = array();

        $options = array();



        if (!empty($start_date) && !empty($end_date)) {
            $options['conditions'] = array(
                'Order.appointment_date <=' => "$end_date",
                'Order.appointment_date >=' => "$start_date",
                'Order.status !=' => '-1'
            );
        }



        $total_records = $this->Order->find("count", $options);
		
		/*$dbo = $this->Order->getDatasource();
		$logs = $dbo->getLog();
		$lastLog = end($logs['log']);
		debug( $lastLog['query'] );*/
		
        if ($total_records > 0) {
            $total_pages = ceil($total_records / $pagesize);

            if ($page > $total_pages) {
                $response_data = [];
            } else {

                $offset = $page * $pagesize;
                $limit = $pagesize;

                if (!empty($start_date) && !empty($end_date)) {

                    /*$getAppointmentData = $this->Order->find("all", array('fields' => array(
                            "Order.start_time", "Order.end_time", "Order.customer_id", "Order.is_van_running", 'Order.is_service_running', "Order.id", "Order.appointment_date", 'Order.type', 'Order.appointment_id', 'Order.receipt_num', 'Order.status', 'Customer.f_name', 'Customer.l_name', 'Customer.mobile', 'Customer.email', 'Order.admin_id'), 'conditions' => array('Order.appointment_date <=' => "$end_date", 'Order.appointment_date >=' => "$start_date"), 'conditions' => array('Order.status !=' => '-1'), 'order' => array('Order.appointment_date' => 'ASC'), 'limit' => $limit, 'offset' => $offset));*/
					
					$getAppointmentData = $this->Order->find("all", array('fields' => array(
                            "Order.start_time", "Order.end_time", "Order.customer_id", "Order.is_van_running", 'Order.is_service_running', "Order.id", "Order.appointment_date", 'Order.type', 'Order.appointment_id', 'Order.receipt_num', 'Order.status', 'Customer.f_name', 'Customer.l_name', 'Customer.mobile', 'Customer.email', 'Order.admin_id'), 'conditions' => array('Order.appointment_date <=' => "$end_date", 'Order.appointment_date >=' => "$start_date", 'Order.status !=' => '-1'), 'order' => array('Order.appointment_date' => 'ASC'), 'limit' => $limit, 'offset' => $offset));	
                } else {

                    $getAppointmentData = $this->Order->find("all", array('fields' => array(
                            "Order.start_time", "Order.end_time", "Order.customer_id", "Order.is_van_running", 'Order.is_service_running', "Order.id", "Order.appointment_date", 'Order.type', 'Order.appointment_id', 'Order.receipt_num', 'Order.status', 'Customer.f_name', 'Customer.l_name', 'Customer.mobile', 'Customer.email', 'Order.admin_id'), 'conditions' => array('Order.status !=' => '-1'), 'order' => array('Order.appointment_date' => 'ASC'), 'limit' => $limit, 'offset' => $offset));
                }

                if (is_array($getAppointmentData) && count($getAppointmentData) > 0) {
                    $appoint = array();
                    foreach ($getAppointmentData as $key => $value) {
                        $appoint[$key]['appointment_id'] = $value['Order']['appointment_id'];
                        $appoint[$key]['order_id'] = $value['Order']['id'];
                        $appoint[$key]['appointment_date'] = $value['Order']['appointment_date'];
                        $appoint[$key]['customer_id'] = $value['Order']['customer_id'];
                        $appoint[$key]['start_time'] = $value['Order']['start_time'];
                        $appoint[$key]['status'] = $value['Order']['status'];
                        $appoint[$key]['end_time'] = $value['Order']['end_time'];
                        $appoint[$key]['is_van_running'] = $value['Order']['is_van_running'];
                        $appoint[$key]['is_service_running'] = $value['Order']['is_service_running'];
                        $appoint[$key]['type'] = $value['Order']['type'];
                        $appoint[$key]['order_pdf_link'] = SITE_URL . 'uploads/invoice/' . $value['Order']['receipt_num'] . '.pdf';
                        $appoint[$key]['customer_name'] = $value['Customer']['f_name'] . ' ' . $value['Customer']['l_name'];
                        $appoint[$key]['mobile'] = $value['Customer']['mobile'];
                        $appoint[$key]['email'] = $value['Customer']['email'];
                        $appoint[$key]['admin_id'] = $value['Order']['admin_id'];
                    }

                    $response_data['appointment_data'] = $appoint;
                    $response_data['total_pages'] = "$total_pages";
                    $response_data['date_offset'] = $date_offset;
                    $page = $page + 1;
                    $response_data['current_page'] = "$page";
                }
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }
    public function get_admin_appointment_listing($api_token = '', $start_date = '', $end_date = '', $deviceid = '', $device_type = '') {

        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $response_data = array();


        $options['conditions'] = array(
            'Order.appointment_date <=' => "$end_date",
            'Order.appointment_date >=' => "$start_date",
            'Order.status !=' => '-1'
        );
        $options['fields'] = array(
            "Order.start_time", "Order.end_time", "Order.customer_id", "Order.is_van_running", 'Order.is_service_running', "Order.id", "Order.appointment_date", 'Order.type', 'Order.appointment_id', 'Order.receipt_num', 'Order.status', 'Customer.f_name', 'Customer.l_name', 'Customer.mobile', 'Customer.email', 'Order.admin_id'
        );
        $options['order'] = array('Order.appointment_date' => 'ASC');
        $appointmentData = $this->Order->find("all", $options);



        $appoint = array();

        if (is_array($appointmentData) && count($appointmentData) > 0) {
            foreach ($appointmentData as $key => $value) {
                $appoint[$key]['appointment_id'] = $value['Order']['appointment_id'];
                $appoint[$key]['order_id'] = $value['Order']['id'];
                $appoint[$key]['appointment_date'] = $value['Order']['appointment_date'];
                $appoint[$key]['customer_id'] = $value['Order']['customer_id'];
                $appoint[$key]['start_time'] = $value['Order']['start_time'];
                $appoint[$key]['status'] = $value['Order']['status'];
                $appoint[$key]['end_time'] = $value['Order']['end_time'];
                $appoint[$key]['is_van_running'] = $value['Order']['is_van_running'];
                $appoint[$key]['is_service_running'] = $value['Order']['is_service_running'];
                $appoint[$key]['type'] = $value['Order']['type'];
                $appoint[$key]['order_pdf_link'] = SITE_URL . 'uploads/invoice/' . $value['Order']['receipt_num'] . '.pdf';
                $appoint[$key]['customer_name'] = $value['Customer']['f_name'] . ' ' . $value['Customer']['l_name'];
                $appoint[$key]['mobile'] = $value['Customer']['mobile'];
                $appoint[$key]['email'] = $value['Customer']['email'];
                $appoint[$key]['admin_id'] = $value['Order']['admin_id'];
            }
        }
        if (is_array($appoint) && count($appoint) > 0) {
            //print_r($appoint);exit;
            foreach ($appoint as $key => $value) {
                if ($key == 0) {
                    $dates_response[$value['appointment_date']] = array();
                    array_push($dates_response[$value['appointment_date']], $value);
                } else if ($appoint[$key - 1]['appointment_date'] == $value['appointment_date']) {
                    array_push($dates_response[$value['appointment_date']], $value);
                } else {
                    $dates_response[$value['appointment_date']] = array();
                    array_push($dates_response[$value['appointment_date']], $value);
                }
            }
        }
        if (!empty($dates_response)) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $dates_response
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    public function get_admin_appointment_details($api_token = '', $appointment_id = '', $deviceid = '', $device_type = '') {



        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $response_data = array();


        $options['conditions'] = array(
            'AppointmentDetail.appointment_id' => "$appointment_id",
            'AppointmentDetail.appointment_confirmed' => '1'
        );
        $options['fields'] = array(
            "AppointmentDetail.travel_start_time", "AppointmentDetail.travel_end_time", "AppointmentDetail.clean_start_time", "AppointmentDetail.clean_end_time",
            "AppointmentDetail.start_time", "AppointmentDetail.end_time", "AppointmentDetail.customer_id",
            "AppointmentDetail.appointment_date", "AppointmentDetail.type", "AppointmentDetail.slot_no", "Customer.mobile", "Customer.f_name",
            "Customer.l_name", "Customer.email", 'Appointment.admin_id', 'AppointmentDetail.id', 'AppointmentDetail.guest_id', 'Appointment.id'
        );
        $options['order'] = array('AppointmentDetail.created' => 'ASC');
        $appointmentData = $this->AppointmentDetail->find("all", $options);

        $appoint = array();

        #pr($appointmentData); die('here');

        if (is_array($appointmentData) && count($appointmentData) > 0) {
            $appointments = array();

            foreach ($appointmentData as $key => $value) {

                #_pr($key);
                #_pr($value);
                #die;

                if (isset($value['OrderData'][0]['order_id']) && !empty($value['OrderData'][0]['order_id'])) {
                    $orderStatus = $this->Order->find('first', array('fields' => array('Order.status', 'Order.receipt_num'), 'conditions' => array('Order.id' => $value['OrderData'][0]['order_id'])));

                    $appoint[$key]['status'] = $orderStatus['Order']['status'];
                    $appoint[$key]['order_pdf_link'] = SITE_URL . 'uploads/invoice/' . $orderStatus['Order']['receipt_num'] . '.pdf';
                }


                $appoint[$key]['order_id'] = $value['OrderData'][0]['order_id'];

                #$appoint[$key]['start_time'] = $value['AppointmentDetail']['start_time'];
                #$appoint[$key]['end_time'] = $value['AppointmentDetail']['end_time'];

                $appoint[$key]['travel_start_time'] = $value['AppointmentDetail']['travel_start_time'];
                $appoint[$key]['travel_end_time'] = $value['AppointmentDetail']['travel_end_time'];

                $appoint[$key]['start_time'] = $value['AppointmentDetail']['start_time'];
                $appoint[$key]['end_time'] = $value['AppointmentDetail']['end_time'];

                $appoint[$key]['clean_start_time'] = $value['AppointmentDetail']['clean_start_time'];
                $appoint[$key]['clean_end_time'] = $value['AppointmentDetail']['clean_end_time'];

                $appoint[$key]['admin_id'] = $value['Appointment']['admin_id'];
                $appoint[$key]['appointment_id'] = $value['Appointment']['id'];
                $appoint[$key]['customer_id'] = $value['AppointmentDetail']['customer_id'];
                $appoint[$key]['mobile'] = $value['Customer']['mobile'];
                $appoint[$key]['f_name'] = $value['Customer']['f_name'];
                $appoint[$key]['l_name'] = $value['Customer']['l_name'];
                $appoint[$key]['email'] = $value['Customer']['email'];
                $appoint[$key]['appointment_date'] = $value['AppointmentDetail']['appointment_date'];
                $appoint[$key]['slot_no'] = $value['AppointmentDetail']['slot_no'];
                $appoint[$key]['guest_id'] = $value['AppointmentDetail']['guest_id'];
                $appoint[$key]['appointmentdetail_id'] = $value['AppointmentDetail']['id'];
                $appoint[$key]['service_name'] = "";
                $appoint[$key]['service_name_ar'] = "";
                $appoint[$key]['package_name'] = "";
                $appoint[$key]['package_name_ar'] = "";
                if ($value['AppointmentDetail']['type'] == 0) {
                    $appoint[$key]['type'] = 'Service';
                } else {
                    $appoint[$key]['type'] = 'Package';
                }

                if (is_array($value['Service']) && count($value['Service']) > 0) {
                    $service_name = array();
                    $service_name_ar = array();
                    foreach ($value['Service'] as $y => $l) {
                        array_push($service_name, $l['name']);
                        array_push($service_name_ar, $l['name_ar']);
                    }
                    $appoint[$key]['service_name'] = implode(',', $service_name);
                    $appoint[$key]['service_name_ar'] = implode(',', $service_name_ar);
                } else {
                    if ($value['AppointmentDetail']['type'] == 0) {
                        if (isset($service_name) && is_array($service_name) && count($service_name) > 0) {
                            $appoint[$key]['service_name'] = implode(',', $service_name);
                            $appoint[$key]['service_name_ar'] = implode(',', $service_name_ar);
                        } else {
                            $serviceData = $this->OrderService->find('all', array('conditions' => array('OrderService.guest_id' => $value['AppointmentDetail']['guest_id'], 'OrderService.appointment_id' => $value['Appointment']['id'])));

                            if (isset($serviceData) && is_array($serviceData) && count($serviceData) > 0) {
                                $service_name = array();
                                $service_name_ar = array();

                                foreach ($serviceData as $y => $l) {
                                    array_push($service_name, $l['Service']['name']);
                                    array_push($service_name_ar, $l['Service']['name_ar']);
                                }

                                $appoint[$key]['service_name'] = implode(',', $service_name);
                                $appoint[$key]['service_name_ar'] = implode(',', $service_name_ar);
                            }
                        }
                    }
                }
                if (is_array($value['Package']) && count($value['Package']) > 0) {
                    $package_name = array();
                    $package_name_ar = array();
                    foreach ($value['Package'] as $y => $l) {
                        array_push($package_name, $l['name']);
                        array_push($package_name_ar, $l['name_ar']);
                    }
                    $appoint[$key]['package_name'] = implode(',', $package_name);
                    $appoint[$key]['package_name_ar'] = implode(',', $package_name_ar);
                }


                // if($key == 0) {
                //     $appointment_details = array();
                //     $j = 0;
                //     $appointments[$key]['appointment_id'] = $value['Appointment']['id'];
                //     $appointments[$key]['start_time'] = $value['AppointmentDetail']['start_time'];
                //     $appointments[$key]['end_time'] = $value['AppointmentDetail']['end_time'];
                //     $appointments[$key]['appointment_date'] = $value['AppointmentDetail']['appointment_date'];
                //     //$appointments[$key]['appointment_details'][$j] = $appoint[$key];
                //     array_push($appointment_details,$appoint[$key]);
                //     $j++;
                // } else if($value['Appointment']['id'] == $appointmentData[$key - 1]['Appointment']['id']) {
                //     array_push($appointment_details,$appoint[$key]);
                //     //print_r($appointment_details);
                //     // $appointments[$key]['appointment_details'][$j] = $appoint[$key];
                //     $j++;
                // } else {
                //     $j = 0;
                //     //$appointments[$key]['appointment_details'][$j] = $appointment_details;
                //     $appointment_details = array();
                //     $appointments[$key]['appointment_id'] = $value['Appointment']['id'];
                //     $appointments[$key]['start_time'] = $value['AppointmentDetail']['start_time'];
                //     $appointments[$key -1]['end_time'] = $value['AppointmentDetail']['end_time'];
                //     $appointments[$key]['appointment_date'] = $value['AppointmentDetail']['appointment_date'];
                //   //  $appointments[$key]['appointment_details'][$j] = $appoint[$key];
                //     $j++;
                //     array_push($appointment_details,$appoint[$key]);
                // }
            }
        }

        if (is_array($appoint) && count($appoint) > 0) {

            foreach ($appoint as $key => $value) {
                if ($key == 0) {
                    $dates_response[$value['appointment_date']] = array();
                    array_push($dates_response[$value['appointment_date']], $value);
                } else if ($appoint[$key - 1]['appointment_date'] == $value['appointment_date']) {
                    array_push($dates_response[$value['appointment_date']], $value);
                } else {
                    $dates_response[$value['appointment_date']] = array();
                    array_push($dates_response[$value['appointment_date']], $value);
                }
            }
        }



        if (!empty($appoint)) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $appoint
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['no_slot_available'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_slot_available'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    private function get_day_no($day) {
        if ($day == 'Monday') {
            return 1;
        } else if ($day == 'Tuesday') {
            return 2;
        } else if ($day == 'Wednesday') {
            return 3;
        } else if ($day == 'Thursday') {
            return 4;
        } else if ($day == 'Friday') {
            return 5;
        } else if ($day == 'Saturday') {
            return 6;
        } else if ($day == 'Sunday') {
            return 7;
        }
    }

    private function check_date_is_within_range($start_date, $end_date, $todays_date) {

        $start_timestamp = strtotime($start_date);
        $end_timestamp = strtotime($end_date);
        //$end_timestamp = strtotime("-1 minutes", strtotime($end_date));

        $today_timestamp = strtotime($todays_date);

        return (($today_timestamp >= $start_timestamp) && ($today_timestamp <= $end_timestamp));
    }


	// Appointment time slot listing API, // Added "$guest_data" by Aditya on 12.09.2017
    public function get_appointment_listing($api_token = '', $appointment_date = '', $customer_id = '', $num_of_slots = '', $back_to_back = '', $deviceid = '', $device_type = '', $guest_data = '', $test = '') {
		
		// temp fix
		/*if(!$test){
			$response = array(
				'status' => 1,
				'message' => 'Sorry, please try later. Application is under maintenance.',
				'message_ar' => 'Sorry, please try later. Application is under maintenance.',
				'data' => NULL
			);
			return $this->fail_safe_return($response);
		}*/
		// temp fix
		
		
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }
        if ($num_of_slots == 0) {
            $num_of_slots = 1;
        }
		
		
		if($device_type == 2)
			$appointment_date = date('Y-m-d', strtotime($appointment_date));
			
		
        $schedularData = $this->get_schedular_from_date($appointment_date);
		#pr($schedularData, 1);
		
        if (empty($schedularData)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['no_schedular_set'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_schedular_set'],
                'data' => NULL
            );
            return $this->fail_safe_return($response);
        }

        $schedular_id = $schedularData['Schedular']['id'];
		
		
		// Stop app conditions between two dates by Aditya on 11.04.2018
		$paymentDate = new DateTime($appointment_date); // Today
		#echo $paymentDate->format('d/m/Y'); // echos today! 
		$contractDateBegin = new DateTime('2018-06-13');
		$contractDateEnd  = new DateTime('2018-06-15');
		
		if ( $paymentDate->getTimestamp() > $contractDateBegin->getTimestamp() && $paymentDate->getTimestamp() < $contractDateEnd->getTimestamp()){
			
			$response = array(
				'status' => $GLOBALS["Webservice"]["codes"]['error'],
				'message' => $GLOBALS['Webservice']['messages']['fully_booked_msg'],
				'message_ar' => $GLOBALS['Webservice']['messages_ar']['fully_booked_msg'],
				'data' => array()
			);
			return $this->fail_safe_return($response);
			
		}
		// Stop app conditions between two dates by Aditya on 11.04.2018
		
		
		/*
		if ($appointment_date == date('Y-m-d')) {
			$response = array(
				'status' => $GLOBALS["Webservice"]["codes"]['error'],
				'message' => $GLOBALS['Webservice']['messages']['no_slot_available'],
				'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_slot_available'],
				'data' => array()
			);
			return $this->fail_safe_return($response);
		}
		*/
		
		// Check how many booking of Appts for this day, so don't show the availability
		$apptTtlCnt = $this->Appointment->find('count', array('conditions' => array('Appointment.appointment_date' => $appointment_date, 'Appointment.status' => 1)));
        if ($apptTtlCnt >= 3) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['no_slot_available'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_slot_available'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        }
		// Check how many booking of Appts for this day, so don't show the availability
		
		#echo '<pre>';
		#print_r($apptTtlCnt);
		#die('hel');
		

        /*
          $response_data = array();
          $response = $this->Schedular->find('all');
          if (is_array($response) && count($response) > 0) {

          $schedular_id = 0;
          foreach ($response as $key => $value) {
          $start_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $appointment_date);
          $end_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $appointment_date);
          if ($start_date_fall == true) {
          $schedular_id = $value['Schedular']['id'];
          continue;
          }
          if ($end_date_fall == true) {
          $schedular_id = $value['Schedular']['id'];
          continue;
          }
          }
          }

          #pr($response_data);
          #die('now here!');

          $options['conditions'] = array(
          "Schedular.id" => $schedular_id,
          );
          $schedularData = $this->Schedular->find("first", $options);
         */
        #pr($schedularData);

        $response_data = array();

        if (is_array($schedularData) && count($schedularData) > 0) {
			
			if(empty($guest_data))
				$guest_data = 1;
				
			// Added "* $guest_data" by Aditya on 12.09.2017
            $total_slot_time = $schedularData['Schedular']['travel_time'] + ($schedularData['Schedular']['cleaning_time'] * $guest_data) + ($schedularData['Schedular']['slot_mins'] * $num_of_slots); // "* $num_of_slots" added by Aditya on 06.09.2017
			
			/*
			$seconds = date("Y-m-d H:i:s");
			$plus_one_hour = $seconds + 3600; 
			$next_hour = floor($plus_one_hour / 3600) * 3600; 
			
			echo date("Y-m-d H:i:s", $next_hour);
			die;
			
			echo ceil(date('H')); die;
			echo $time = ceil(time());
			echo '='.date('H:i', strtotime("+240 minutes", strtotime($time)));
			die;
			
			
			if(date('Y-m-d') == $appointment_date){
				$schedStartTime = $start_time = date('H:i', strtotime("+240 minutes", strtotime(date('H:i'))));
			}
			else
				$schedStartTime = $start_time = date('H:i', strtotime($schedularData['Schedular']['start_time']));
			*/	
			
            $schedStartTime = $start_time = date('H:i', strtotime($schedularData['Schedular']['start_time']));
            $endtimes = date('H:i', strtotime($schedularData['Schedular']['end_time']));

            $schedular_id = $schedularData['Schedular']['id'];
            $slot_lunch = $schedularData['Schedular']['slot_lunch'];
            $weekends = $schedularData['Schedular']['weekends'];
            $maximum_slot_in_row = $schedularData['Schedular']['maximum_slot_in_row'];
			$lastStrtTime = null;
			
			if($num_of_slots >= 3)
				$schedularData['Schedular']['maximum_appointment'] = 6;
			
			
            for ($i = 0; $i < $schedularData['Schedular']['maximum_appointment']; $i++) {
				
				$responses[$i]['start_time'] = $start_time;
                $end_time = strtotime("+$total_slot_time minutes", strtotime($start_time));
                $responses[$i]['end_time'] = date('H:i', $end_time);
                $responses[$i]['schedular_id'] = $schedular_id;
                $responses[$i]['appointment_date'] = $appointment_date;
                $responses[$i]['slot_no'] = (string) ($i + 1);
				
                if ($slot_lunch == ($i + 1)) {
                    $responses[$i]['lunch_slot'] = "1";
                } else {
                    $responses[$i]['lunch_slot'] = "0";
                }
				#print_r($_SERVER);
				#echo date("Y-m-d H:i:s");
				if(date('Y-m-d') == $appointment_date) //  or date('Y-n-d') == $appointment_date
					//$dates = strtotime(date("Y-m-d H:i", strtotime('+4 hours'))); #, strtotime('+4 hours')
					// changed with +2 hours to 4+ hours by vishal on 21.1.2018
					$dates = strtotime(date("Y-m-d H:i", strtotime('+2 hours'))); #, strtotime('+2 hours')
				else
					$dates = strtotime(date('Y-m-d H:i'));
				#echo $dates; #die;
				#$dates = strtotime(date('Y-m-d H:i'));
				$start_times = strtotime($appointment_date . ' ' . $start_time);
				
				#echo ' -- -- ';
				#echo ($start_times);
				#echo ' -- -- ';
                #pr($response_data);
				#pr($responses);
				#echo $start_time;
				#pr( $schedStartTime );
				#echo 'laststrttime='.$lastStrtTime;
				
				#echo ' - '. $start_times .' - '. $dates .' - '. $start_time .' - '. $lastStrtTime .'|| <br> \n \t' ; #die;
				
                /*if ($num_of_slots > 1 && $start_times >= $dates) { # && $schedStartTime < $start_time  # $num_of_slots > 1 && 
					echo ' if ';
                    array_push($response_data, $responses[$i]);
                } else if($num_of_slots <= 1){
					echo ' else if ';
					array_push($response_data, $responses[$i]);
				} else {
					echo ' else ';
					//array_push($response_data, $responses[$i]);
					unset($responses[$i]);
				}*/
				
				
                if ($start_times >= $dates) { # && $schedStartTime < $start_time  # $num_of_slots > 1 && 
					#echo ' if ';
                    array_push($response_data, $responses[$i]);
                } 
					
				#array_push($response_data, $responses[$i]);
				#$lastStrtTime = $start_time;
                $start_time = date('H:i', $end_time);
				
				
				
            }
        }
		#die;
		
		#pr($response_data); die;
		
        // commented by Aditya on 12.03.2017
        #pr($response_data);
        if (is_array($response_data) && count($response_data) > 0) {
            $responsess = array();
            $lastSlotStartime = end($response_data);
			$cnt = false;
            foreach ($response_data as $key => $value) {
                if ($lastSlotStartime['start_time'] > $endtimes) {
					#echo 'if';
                    if ($endtimes <= $value['start_time']) {
                        #echo 'if';
                        #pr($value);
                        #pr($responsess);
                        array_push($responsess, $value);
                    }
                } else {
                    #echo 'else';
					#echo $endtimes .'='. $value['start_time'].'<br>';
					#pr($responsess);
					if($endtimes > $value['start_time'] && $cnt == false){
						array_push($responsess, $value);
						#$value = NULL;
						#$cnt = true;
					} else
						$cnt = true;
                }
				//if($cnt == true)
					//continue;
            }
			
        }
        #pr($responsess); #die;
        #$response_data = $responsess;

        $appointmentday = date('l', strtotime($appointment_date));
        #echo $appointmentday; die;

        /*
          $weekend_arrays = explode(',', $weekends);

          if (in_array($this->get_day_no($appointmentday), $weekend_arrays)) {
          $response = array(
          'status' => $GLOBALS["Webservice"]["codes"]['error'],
          'message' => $GLOBALS['Webservice']['messages']['appointment_holiday'],
          'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_holiday'],
          'data' => array()
          );
          return $this->fail_safe_return($response);
          } */

        $packageData = $this->Appointment->find('first', array('conditions' => array('Appointment.appointment_date' => $appointment_date, 'Appointment.type' => '1')));
        #pr($packageData);
        #die;
        // if package booked - don't show the availability
        if (isset($packageData['Appointment']['id']) && $packageData['Appointment']['id'] > 0) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['no_slot_available'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_slot_available'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        }
		
		
		/*
		pr($response_data);
		if (is_array($response_data) && count($response_data) > 0) {

            foreach ($response_data as $key => $value) {
                if (!empty($value['slot_no'])) {
                    //$options['conditions'] = array(
//                        #"AppointmentDetail.slot_no" => $value['slot_no'],
//                        'AppointmentDetail.appointment_date' => $appointment_date,
//                    );
//                    $options['fields'] = array(
//                        "AppointmentDetail.travel_start_time", "AppointmentDetail.clean_end_time",
//						"AppointmentDetail.start_time", "AppointmentDetail.end_time", "AppointmentDetail.customer_id",
//                        "AppointmentDetail.appointment_date", "AppointmentDetail.slot_no", "Customer.mobile", "Customer.f_name",
//                        "Customer.l_name", "Customer.email", 'AppointmentDetail.id', 'AppointmentDetail.guest_id'
//                    );
//                    $appointmentData = $this->AppointmentDetail->find("all", $options);
//					
//					pr($appointmentData);
//					
					
					$options['conditions'] = array(
                        #"AppointmentDetail.slot_no" => $value['slot_no'],
                        'Appointment.appointment_date' => $appointment_date,
                    );
                    $options['fields'] = array(
						'Appointment.id', "Appointment.appointment_date", "Appointment.start_time", "Appointment.end_time", "Appointment.customer_id",
                    );
					$this->Appointment->recursive = -1;
                    $appointmentData = $this->Appointment->find("all", $options);
					
					#pr($appointmentData);
					
					foreach($appointmentData as $ka => $va):
						pr($va);
						
						
						
					endforeach;
					
					
					
                    $response_data[$key]['available_slot'] = "1";
                    if (isset($appointmentData['AppointmentDetail']['id']) || $response_data[$key]['lunch_slot'] == 1) {
                        unset($response_data[$key]);
                    }
                }
            }
            #pr($appointmentData);
            #pr($response_data);
            die('here!!');
			*/
			
			#die('1 here!!');
			
			// Find existing appointments and remove those conflicting slots by Aditya on 13.09.2017
			$options['conditions'] = array(
				#"AppointmentDetail.slot_no" => $value['slot_no'],
				'Appointment.appointment_date' => $appointment_date,
				'Appointment.status !=' => -1, // cond added by Aditya on 04.10.2017
			);
			$options['fields'] = array(
				'Appointment.id', "Appointment.appointment_date", "Appointment.start_time", "Appointment.end_time", "Appointment.customer_id",
			);
			$this->Appointment->recursive = -1;
			$appointmentData = $this->Appointment->find("all", $options);
			
			#pr($appointmentData);
			#pr($response_data);
			
			foreach($appointmentData as $ka => $va):
				#pr($va);
				
				foreach($response_data as $kar => $var):
					#pr($var);
					
					
					$begin = date("H:i", strtotime($va['Appointment']['start_time']));
					$end = date("H:i", strtotime($va['Appointment']['end_time']));
					
					$now = date("H:i", strtotime($var['start_time']));
					$nowend = date("H:i", strtotime($var['end_time']));					
					
					#echo $now.'>='.$begin.' - '.$now.'<='.$end.'  <br>  ';
					
					if ($now >= $begin && $now <= $end)
						unset($response_data[$kar]);
					
				endforeach;
				
				
				
			endforeach;
			// Find existing appointments and remove those conflicting slots by Aditya on 13.09.2017
			
			
				#echo '<pre>';
			#print_r($response_data);
			
			
				
			#pr($response_data); #die;
			
			// remove slots as per Ramadan timings by Aditya on 16.05.2018
			$sltst = false;
			foreach($response_data as $ak => $av):
			
				$now = date("H", strtotime($av['start_time']));
				$nowend = date("H", strtotime($av['end_time']));
				
				// remove slots between 17.00 till 20.00
				if ($now >= 13 && $now <= 16) {
					unset($response_data[$ak]);
					$sltno = $av['slot_no'];
					$sltst = true;
				}
				
			endforeach;
			
			#print_r($response_data);
			
			if($sltst == true){
				$response_data = array_reverse($response_data, true);
					#print_r($response_data);
				#pr($response_data); die;
				
				foreach ($response_data as $ak => $av):
				
					// do something here
					$now = date("H", strtotime($av['start_time']));
					$nowend = date("H", strtotime($av['end_time']));
										
					// remove slots after 02.00 at night
					if ($now > 21) {
						unset($response_data[$ak]);
					
					// will leave the foreach loop and also the if statement
					} else {
						
						break;
					}
				endforeach;
			}
			
			#print_r($response_data); die('hi');
			
			$response_data = array_reverse($response_data, true);
			#pr($response_data); die;

			#print_r($response_data); die('hi');

			// remove slots as per Ramadan timings by Aditya on 16.05.2018
			
			
			
			
			
			
			
			
			//
//			// If no slots, show slots not available by Aditya on 13.09.2017
//			if (!$response_data) {
//				$response = array(
//					'status' => $GLOBALS["Webservice"]["codes"]['error'],
//					'message' => $GLOBALS['Webservice']['messages']['no_slot_available'],
//					'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_slot_available'],
//					'data' => array()
//				);
//				return $this->fail_safe_return($response);
//			}
//			// If no slots, show slots not available by Aditya on 13.09.2017
//			
			#	echo '<pre>';
			#print_r($response_data);
			
			#die('here!!');
			
			
            $response_data = array_values($response_data);


            /*
              // commented by Aditya as it was removing slot - 6 & 7, don't know why on date 23.03.2017
              $avaliale_slot = array();
              if ($num_of_slots > 0 && $back_to_back == 1) {

              $start_time_slot = array();
              if (is_array($response_data) && count($response_data) > 0) {
              foreach ($response_data as $key => $value) {
              array_push($start_time_slot, date('H:i', strtotime($value['start_time'])));
              }
              }

              $maximum_slots = floor($num_of_slots / $schedularData['Schedular']['maximum_slot_in_row']);
              $needed_slot = $num_of_slots + $maximum_slots;
              $total_slots = count($response_data);

              foreach ($response_data as $key => $val) {
              $expected_slot = $needed_slot;
              $add_minutes = ($total_slot_time * $needed_slot) . ' minutes';
              $expected_slot = strtotime($val['start_time'] . "+ $add_minutes");
              $available_ = array();
              $start_times = $val['start_time'];
              for ($i = 0; $i < $needed_slot; $i++) {
              if ($i == 0) {
              $time = strtotime($start_times);
              } else {
              $time = strtotime("+$total_slot_time minutes", strtotime($start_times));
              }
              array_push($available_, date('H:i', $time));
              $start_times = date('H:i', $time);
              }
              if (count(array_diff($available_, $start_time_slot)) == 0) {
              array_push($avaliale_slot, $response_data[$key]);
              }
              }
              }
             */

            $avaliale_slot = $response_data; // added this line because of above lines commented
        #}
		
		
		#echo '<pre>'; print_r($avaliale_slot); die('avaliale_slot');

        $bookedAdminSlot = $this->Order->find('all', array('fields' => array('Order.start_time', 'Order.end_time', 'Order.id', 'Order.appointment_date'), 'conditions' => array('Order.appointment_date' => $appointment_date, 'Order.status !=' => -1, 'Order.admin_id >' => '0')));
        $bookedSlotTime = array();

        #pr($avaliale_slot);
        #pr($response_data);
        #die('hello$');
        $final_slot = array();

        if (isset($avaliale_slot) && is_array($avaliale_slot) && count($avaliale_slot) > 0) {

            foreach ($avaliale_slot as $key => $val) {

                if (is_array($bookedAdminSlot) && count($bookedAdminSlot) > 0) {

                    foreach ($bookedAdminSlot as $k => $value) {
						
                        $value['Order']['start_time'] = date('H:i:s', strtotime($value['Order']['start_time']));
                        $value['Order']['end_time'] = date('H:i:s', strtotime($value['Order']['end_time']));
                        $val['start_time'] = date('H:i:s', strtotime("+0 minutes", strtotime($val['start_time'])));
                        $val['end_time'] = date('H:i:s', strtotime("-1 minutes", strtotime($val['end_time'])));
						
                        $responseStart = $this->check_date_is_within_range($value['Order']['start_time'], $value['Order']['end_time'], $val['start_time']);
                        $responseend = $this->check_date_is_within_range($value['Order']['start_time'], $value['Order']['end_time'], $val['end_time']);
                        if ($val['slot_no'] == 2) {
                            // echo $val['start_time'];
                            // echo $val['end_time'];
                            // var_dump($responseStart);
                            // var_dump($responseend);
                        }

                        if (($value['Order']['start_time'] == $val['start_time']) && ($value['Order']['end_time']) == $val['end_time']) {

                            unset($avaliale_slot[$key]);
                            if (is_array($final_slot) && count($final_slot) > 0)
                                foreach ($final_slot as $index => $data) {
                                    if ($data['slot_no'] == $val['slot_no']) {
                                        #echo $final_slot[$index];
                                        unset($final_slot[$index]);
                                    }
                                }
                        } else if ($responseend || $responseStart) {
                            // if(($value['Order']['start_time'] == $val['start_time']) || ($value['Order']['end_time'] == $val['end_time'])
                            //     || ($value['Order']['start_time'] == $val['end_time']) || ($value['Order']['end_time'] == $val['start_time'])) {
                            //    echo "dsf";exit;
                            //      // if(is_array($final_slot) && count($final_slot) > 0) {
                            //      //     foreach ($final_slot as $index => $data) {
                            //      //         if ($data['slot_no'] == $val['slot_no']) {
                            //      //             unset($final_slot[$index]);
                            //      //         } else {
                            //      //              if(isset($avaliale_slot[$key])) {
                            //      //                 array_push($final_slot,$avaliale_slot[$key]);    
                            //      //              }   
                            //      //         }
                            //      //     }   
                            //      // } else {
                            //      //     if(isset($avaliale_slot[$key])) {
                            //      //         array_push($final_slot,$avaliale_slot[$key]);    
                            //      //     }  
                            //      // }
                            //    if(isset($avaliale_slot[$key])) {
                            //              array_push($final_slot,$avaliale_slot[$key]);    
                            //           }  
                            // } else {
                            unset($avaliale_slot[$key]);
                            if (is_array($final_slot) && count($final_slot) > 0)
                                foreach ($final_slot as $index => $data) {
                                    if ($data['slot_no'] == $val['slot_no']) {
                                        #echo $final_slot[$index];
                                        unset($final_slot[$index]);
                                    }
                                }
                            //}
                        } else {

                            if (isset($avaliale_slot[$key])) {
                                array_push($final_slot, $avaliale_slot[$key]);
                            }
                        }
                    }
                } else {
                    $final_slot = $avaliale_slot;
                }
            }
        }
        #echo '<pre>'; print_r($final_slot); die;
		
        $resp_slots = $final_slot;
        $final_slot=array();
        $continueslot=FALSE;
        // Adding travel time to slot time, by Aditya on 12.03.2017
        foreach ($resp_slots as $k => $v) {
            
			/*
			#pr($resp_slots);
            //Checking for continues slot avalibility check, by KATHAK DABHI 18.05.2017
            if($num_of_slots>1){
                for($i=0;$i<$num_of_slots-1 && $k+$num_of_slots-1<count($resp_slots);$i++){
                    if($resp_slots[$k+$i]['end_time']==$resp_slots[$k+$i+1]['start_time']){
						#echo 'if';
                        $continueslot=TRUE;
                    }else{
						#echo 'else';
                        $continueslot=FALSE;
                    }
                }
                if($continueslot && $num_of_slots>1){
                    $final_slot[$k]=$v;
                    $now = new DateTime($final_slot[$k]['start_time']); //current date/time
                    $now->add(new DateInterval("PT{$schedularData['Schedular']['travel_time']}M"));
                    $final_slot[$k]['start_time'] = $now->format('H:i');
                }
                $continueslot=FALSE;
				
				$final_slot[$k]=$v;
                    $now = new DateTime($final_slot[$k]['start_time']); //current date/time
                    $now->add(new DateInterval("PT{$schedularData['Schedular']['travel_time']}M"));
                    $final_slot[$k]['start_time'] = $now->format('H:i');
            }else{
                $final_slot[$k]=$v;
                $now = new DateTime($final_slot[$k]['start_time']); //current date/time
                $now->add(new DateInterval("PT{$schedularData['Schedular']['travel_time']}M"));
                $final_slot[$k]['start_time'] = $now->format('H:i');
            }
			*/
			
			// By Aditya on 07.09.2017, Above code commented and added below lines to show last slot for appt
			$final_slot[$k]=$v;
			$now = new DateTime($final_slot[$k]['start_time']); //current date/time
			$now->add(new DateInterval("PT{$schedularData['Schedular']['travel_time']}M"));
			$final_slot[$k]['start_time'] = $now->format('H:i');
			
        }
		
		
        // Opened by Aditya on 07.09.2017 - to check the last slot start time is not greater than scheduler end time
		// commented as of now - No usage by Aditya 22.06.2017
        #$final_slot = $resp_slots;
        #pr($final_slot);
        #die;
		$newslots = array();
		$cnt = false;
		foreach ($final_slot as $key => $value) {
			
			if($endtimes > $value['start_time'] && $cnt == false){
				array_push($newslots, $value);
			} else
				$cnt = true;
			
		}
		
		// Sort the unique timeslots by Aditya on 04.10.2017
		#print_r($newslots);
		$tmp = array();
		foreach ($newslots as $item) {
			if (!in_array($item['slot_no'], $tmp)) {
				$unique[] = $item;
				$tmp[] = $item['slot_no'];
			}
		}
		
		// Assigning new slots
		$final_slot = array_values($unique);
        
		#print_r($unique);
        #die('hi11');
		
		
		#$final_slot = array_unique($newslots, SORT_REGULAR);
        #$final_slot = array_values($newslots);
		
		
		
		
        #$final_slot = array_unique($final_slot, SORT_REGULAR);
        #$final_slot = array_values($final_slot);
		
        if (!empty($final_slot)) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $final_slot
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['no_slot_available'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_slot_available'],
                'data' => $final_slot
            );
        }

        return $this->fail_safe_return($response);
    }


	// Appointment time slot listing API, // Added "$guest_data" by Aditya on 12.09.2017
    public function get_appointment_listing_beforeramadan($api_token = '', $appointment_date = '', $customer_id = '', $num_of_slots = '', $back_to_back = '', $deviceid = '', $device_type = '', $guest_data = '', $test = '') {
		
		// temp fix
		/*if(!$test){
			$response = array(
				'status' => 1,
				'message' => 'Sorry, please try later. Application is under maintenance.',
				'message_ar' => 'Sorry, please try later. Application is under maintenance.',
				'data' => NULL
			);
			return $this->fail_safe_return($response);
		}*/
		// temp fix
		
		
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }
        if ($num_of_slots == 0) {
            $num_of_slots = 1;
        }
		
		
		if($device_type == 2)
			$appointment_date = date('Y-m-d', strtotime($appointment_date));
			
		
        $schedularData = $this->get_schedular_from_date($appointment_date);

        if (empty($schedularData)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['no_schedular_set'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_schedular_set'],
                'data' => NULL
            );
            return $this->fail_safe_return($response);
        }

        $schedular_id = $schedularData['Schedular']['id'];


        /*
          $response_data = array();
          $response = $this->Schedular->find('all');
          if (is_array($response) && count($response) > 0) {

          $schedular_id = 0;
          foreach ($response as $key => $value) {
          $start_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $appointment_date);
          $end_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $appointment_date);
          if ($start_date_fall == true) {
          $schedular_id = $value['Schedular']['id'];
          continue;
          }
          if ($end_date_fall == true) {
          $schedular_id = $value['Schedular']['id'];
          continue;
          }
          }
          }

          #pr($response_data);
          #die('now here!');

          $options['conditions'] = array(
          "Schedular.id" => $schedular_id,
          );
          $schedularData = $this->Schedular->find("first", $options);
         */
        #pr($schedularData);

        $response_data = array();

        if (is_array($schedularData) && count($schedularData) > 0) {
			
			if(empty($guest_data))
				$guest_data = 1;
				
			// Added "* $guest_data" by Aditya on 12.09.2017
            $total_slot_time = $schedularData['Schedular']['travel_time'] + ($schedularData['Schedular']['cleaning_time'] * $guest_data) + ($schedularData['Schedular']['slot_mins'] * $num_of_slots); // "* $num_of_slots" added by Aditya on 06.09.2017
			
			/*
			$seconds = date("Y-m-d H:i:s");
			$plus_one_hour = $seconds + 3600; 
			$next_hour = floor($plus_one_hour / 3600) * 3600; 
			
			echo date("Y-m-d H:i:s", $next_hour);
			die;
			
			echo ceil(date('H')); die;
			echo $time = ceil(time());
			echo '='.date('H:i', strtotime("+240 minutes", strtotime($time)));
			die;
			
			
			if(date('Y-m-d') == $appointment_date){
				$schedStartTime = $start_time = date('H:i', strtotime("+240 minutes", strtotime(date('H:i'))));
			}
			else
				$schedStartTime = $start_time = date('H:i', strtotime($schedularData['Schedular']['start_time']));
			*/	
			
            $schedStartTime = $start_time = date('H:i', strtotime($schedularData['Schedular']['start_time']));
            $endtimes = date('H:i', strtotime($schedularData['Schedular']['end_time']));

            $schedular_id = $schedularData['Schedular']['id'];
            $slot_lunch = $schedularData['Schedular']['slot_lunch'];
            $weekends = $schedularData['Schedular']['weekends'];
            $maximum_slot_in_row = $schedularData['Schedular']['maximum_slot_in_row'];
			$lastStrtTime = null;
			
            for ($i = 0; $i < $schedularData['Schedular']['maximum_appointment']; $i++) {
				
				$responses[$i]['start_time'] = $start_time;
                $end_time = strtotime("+$total_slot_time minutes", strtotime($start_time));
                $responses[$i]['end_time'] = date('H:i', $end_time);
                $responses[$i]['schedular_id'] = $schedular_id;
                $responses[$i]['appointment_date'] = $appointment_date;
                $responses[$i]['slot_no'] = (string) ($i + 1);
				
                if ($slot_lunch == ($i + 1)) {
                    $responses[$i]['lunch_slot'] = "1";
                } else {
                    $responses[$i]['lunch_slot'] = "0";
                }
				#print_r($_SERVER);
				#echo date("Y-m-d H:i:s");
				if(date('Y-m-d') == $appointment_date) //  or date('Y-n-d') == $appointment_date
					//$dates = strtotime(date("Y-m-d H:i", strtotime('+4 hours'))); #, strtotime('+4 hours')
					// changed with +2 hours to 4+ hours by vishal on 21.1.2018
					$dates = strtotime(date("Y-m-d H:i", strtotime('+2 hours'))); #, strtotime('+2 hours')
				else
					$dates = strtotime(date('Y-m-d H:i'));
				#echo $dates; #die;
				#$dates = strtotime(date('Y-m-d H:i'));
				$start_times = strtotime($appointment_date . ' ' . $start_time);
				
				#echo ' -- -- ';
				#echo ($start_times);
				#echo ' -- -- ';
                #pr($response_data);
				#pr($responses);
				#echo $start_time;
				#pr( $schedStartTime );
				#echo 'laststrttime='.$lastStrtTime;
				
				#echo ' - '. $start_times .' - '. $dates .' - '. $start_time .' - '. $lastStrtTime .'|| <br> \n \t' ; #die;
				
                /*if ($num_of_slots > 1 && $start_times >= $dates) { # && $schedStartTime < $start_time  # $num_of_slots > 1 && 
					echo ' if ';
                    array_push($response_data, $responses[$i]);
                } else if($num_of_slots <= 1){
					echo ' else if ';
					array_push($response_data, $responses[$i]);
				} else {
					echo ' else ';
					//array_push($response_data, $responses[$i]);
					unset($responses[$i]);
				}*/
				
				
                if ($start_times >= $dates) { # && $schedStartTime < $start_time  # $num_of_slots > 1 && 
					#echo ' if ';
                    array_push($response_data, $responses[$i]);
                } 
					
				#array_push($response_data, $responses[$i]);
				#$lastStrtTime = $start_time;
                $start_time = date('H:i', $end_time);
				
				
				
            }
        }
		#die;
		
		#pr($response_data); die;
		
        // commented by Aditya on 12.03.2017
        #pr($response_data);
        if (is_array($response_data) && count($response_data) > 0) {
            $responsess = array();
            $lastSlotStartime = end($response_data);
			$cnt = false;
            foreach ($response_data as $key => $value) {
                if ($lastSlotStartime['start_time'] > $endtimes) {
					#echo 'if';
                    if ($endtimes <= $value['start_time']) {
                        #echo 'if';
                        #pr($value);
                        #pr($responsess);
                        array_push($responsess, $value);
                    }
                } else {
                    #echo 'else';
					#echo $endtimes .'='. $value['start_time'].'<br>';
					#pr($responsess);
					if($endtimes > $value['start_time'] && $cnt == false){
						array_push($responsess, $value);
						#$value = NULL;
						#$cnt = true;
					} else
						$cnt = true;
                }
				//if($cnt == true)
					//continue;
            }
			
        }
        #pr($responsess); #die;
        #$response_data = $responsess;

        $appointmentday = date('l', strtotime($appointment_date));
        #echo $appointmentday; die;

        /*
          $weekend_arrays = explode(',', $weekends);

          if (in_array($this->get_day_no($appointmentday), $weekend_arrays)) {
          $response = array(
          'status' => $GLOBALS["Webservice"]["codes"]['error'],
          'message' => $GLOBALS['Webservice']['messages']['appointment_holiday'],
          'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_holiday'],
          'data' => array()
          );
          return $this->fail_safe_return($response);
          } */

        $packageData = $this->Appointment->find('first', array('conditions' => array('Appointment.appointment_date' => $appointment_date, 'Appointment.type' => '1')));
        #pr($packageData);
        #die;
        // if package booked - don't show the availability
        if (isset($packageData['Appointment']['id']) && $packageData['Appointment']['id'] > 0) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['no_slot_available'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_slot_available'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        }
		
		
		/*
		pr($response_data);
		if (is_array($response_data) && count($response_data) > 0) {

            foreach ($response_data as $key => $value) {
                if (!empty($value['slot_no'])) {
                    //$options['conditions'] = array(
//                        #"AppointmentDetail.slot_no" => $value['slot_no'],
//                        'AppointmentDetail.appointment_date' => $appointment_date,
//                    );
//                    $options['fields'] = array(
//                        "AppointmentDetail.travel_start_time", "AppointmentDetail.clean_end_time",
//						"AppointmentDetail.start_time", "AppointmentDetail.end_time", "AppointmentDetail.customer_id",
//                        "AppointmentDetail.appointment_date", "AppointmentDetail.slot_no", "Customer.mobile", "Customer.f_name",
//                        "Customer.l_name", "Customer.email", 'AppointmentDetail.id', 'AppointmentDetail.guest_id'
//                    );
//                    $appointmentData = $this->AppointmentDetail->find("all", $options);
//					
//					pr($appointmentData);
//					
					
					$options['conditions'] = array(
                        #"AppointmentDetail.slot_no" => $value['slot_no'],
                        'Appointment.appointment_date' => $appointment_date,
                    );
                    $options['fields'] = array(
						'Appointment.id', "Appointment.appointment_date", "Appointment.start_time", "Appointment.end_time", "Appointment.customer_id",
                    );
					$this->Appointment->recursive = -1;
                    $appointmentData = $this->Appointment->find("all", $options);
					
					#pr($appointmentData);
					
					foreach($appointmentData as $ka => $va):
						pr($va);
						
						
						
					endforeach;
					
					
					
                    $response_data[$key]['available_slot'] = "1";
                    if (isset($appointmentData['AppointmentDetail']['id']) || $response_data[$key]['lunch_slot'] == 1) {
                        unset($response_data[$key]);
                    }
                }
            }
            #pr($appointmentData);
            #pr($response_data);
            die('here!!');
			*/
			
			#die('1 here!!');
			
			// Find existing appointments and remove those conflicting slots by Aditya on 13.09.2017
			$options['conditions'] = array(
				#"AppointmentDetail.slot_no" => $value['slot_no'],
				'Appointment.appointment_date' => $appointment_date,
				'Appointment.status !=' => -1, // cond added by Aditya on 04.10.2017
			);
			$options['fields'] = array(
				'Appointment.id', "Appointment.appointment_date", "Appointment.start_time", "Appointment.end_time", "Appointment.customer_id",
			);
			$this->Appointment->recursive = -1;
			$appointmentData = $this->Appointment->find("all", $options);
			
			#pr($appointmentData);
			#pr($response_data);
			
			foreach($appointmentData as $ka => $va):
				#pr($va);
				
				foreach($response_data as $kar => $var):
					#pr($var);
					
					
					$begin = date("H:i", strtotime($va['Appointment']['start_time']));
					$end = date("H:i", strtotime($va['Appointment']['end_time']));
					
					$now = date("H:i", strtotime($var['start_time']));
					$nowend = date("H:i", strtotime($var['end_time']));					
					
					#echo $now.'>='.$begin.' - '.$now.'<='.$end.'  <br>  ';
					
					if ($now >= $begin && $now <= $end)
						unset($response_data[$kar]);
					
				endforeach;
				
				
				
			endforeach;
			// Find existing appointments and remove those conflicting slots by Aditya on 13.09.2017
			
			//
//			// If no slots, show slots not available by Aditya on 13.09.2017
//			if (!$response_data) {
//				$response = array(
//					'status' => $GLOBALS["Webservice"]["codes"]['error'],
//					'message' => $GLOBALS['Webservice']['messages']['no_slot_available'],
//					'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_slot_available'],
//					'data' => array()
//				);
//				return $this->fail_safe_return($response);
//			}
//			// If no slots, show slots not available by Aditya on 13.09.2017
//			
				
			#pr($response_data);
			
			#die('here!!');
			
			
            $response_data = array_values($response_data);


            /*
              // commented by Aditya as it was removing slot - 6 & 7, don't know why on date 23.03.2017
              $avaliale_slot = array();
              if ($num_of_slots > 0 && $back_to_back == 1) {

              $start_time_slot = array();
              if (is_array($response_data) && count($response_data) > 0) {
              foreach ($response_data as $key => $value) {
              array_push($start_time_slot, date('H:i', strtotime($value['start_time'])));
              }
              }

              $maximum_slots = floor($num_of_slots / $schedularData['Schedular']['maximum_slot_in_row']);
              $needed_slot = $num_of_slots + $maximum_slots;
              $total_slots = count($response_data);

              foreach ($response_data as $key => $val) {
              $expected_slot = $needed_slot;
              $add_minutes = ($total_slot_time * $needed_slot) . ' minutes';
              $expected_slot = strtotime($val['start_time'] . "+ $add_minutes");
              $available_ = array();
              $start_times = $val['start_time'];
              for ($i = 0; $i < $needed_slot; $i++) {
              if ($i == 0) {
              $time = strtotime($start_times);
              } else {
              $time = strtotime("+$total_slot_time minutes", strtotime($start_times));
              }
              array_push($available_, date('H:i', $time));
              $start_times = date('H:i', $time);
              }
              if (count(array_diff($available_, $start_time_slot)) == 0) {
              array_push($avaliale_slot, $response_data[$key]);
              }
              }
              }
             */

            $avaliale_slot = $response_data; // added this line because of above lines commented
        #}
		
		
		#echo '<pre>'; print_r($avaliale_slot); die('avaliale_slot');

        $bookedAdminSlot = $this->Order->find('all', array('fields' => array('Order.start_time', 'Order.end_time', 'Order.id', 'Order.appointment_date'), 'conditions' => array('Order.appointment_date' => $appointment_date, 'Order.status !=' => -1, 'Order.admin_id >' => '0')));
        $bookedSlotTime = array();

        #pr($avaliale_slot);
        #pr($response_data);
        #die('hello$');
        $final_slot = array();

        if (isset($avaliale_slot) && is_array($avaliale_slot) && count($avaliale_slot) > 0) {

            foreach ($avaliale_slot as $key => $val) {

                if (is_array($bookedAdminSlot) && count($bookedAdminSlot) > 0) {

                    foreach ($bookedAdminSlot as $k => $value) {
						
                        $value['Order']['start_time'] = date('H:i:s', strtotime($value['Order']['start_time']));
                        $value['Order']['end_time'] = date('H:i:s', strtotime($value['Order']['end_time']));
                        $val['start_time'] = date('H:i:s', strtotime("+0 minutes", strtotime($val['start_time'])));
                        $val['end_time'] = date('H:i:s', strtotime("-1 minutes", strtotime($val['end_time'])));
						
                        $responseStart = $this->check_date_is_within_range($value['Order']['start_time'], $value['Order']['end_time'], $val['start_time']);
                        $responseend = $this->check_date_is_within_range($value['Order']['start_time'], $value['Order']['end_time'], $val['end_time']);
                        if ($val['slot_no'] == 2) {
                            // echo $val['start_time'];
                            // echo $val['end_time'];
                            // var_dump($responseStart);
                            // var_dump($responseend);
                        }

                        if (($value['Order']['start_time'] == $val['start_time']) && ($value['Order']['end_time']) == $val['end_time']) {

                            unset($avaliale_slot[$key]);
                            if (is_array($final_slot) && count($final_slot) > 0)
                                foreach ($final_slot as $index => $data) {
                                    if ($data['slot_no'] == $val['slot_no']) {
                                        #echo $final_slot[$index];
                                        unset($final_slot[$index]);
                                    }
                                }
                        } else if ($responseend || $responseStart) {
                            // if(($value['Order']['start_time'] == $val['start_time']) || ($value['Order']['end_time'] == $val['end_time'])
                            //     || ($value['Order']['start_time'] == $val['end_time']) || ($value['Order']['end_time'] == $val['start_time'])) {
                            //    echo "dsf";exit;
                            //      // if(is_array($final_slot) && count($final_slot) > 0) {
                            //      //     foreach ($final_slot as $index => $data) {
                            //      //         if ($data['slot_no'] == $val['slot_no']) {
                            //      //             unset($final_slot[$index]);
                            //      //         } else {
                            //      //              if(isset($avaliale_slot[$key])) {
                            //      //                 array_push($final_slot,$avaliale_slot[$key]);    
                            //      //              }   
                            //      //         }
                            //      //     }   
                            //      // } else {
                            //      //     if(isset($avaliale_slot[$key])) {
                            //      //         array_push($final_slot,$avaliale_slot[$key]);    
                            //      //     }  
                            //      // }
                            //    if(isset($avaliale_slot[$key])) {
                            //              array_push($final_slot,$avaliale_slot[$key]);    
                            //           }  
                            // } else {
                            unset($avaliale_slot[$key]);
                            if (is_array($final_slot) && count($final_slot) > 0)
                                foreach ($final_slot as $index => $data) {
                                    if ($data['slot_no'] == $val['slot_no']) {
                                        #echo $final_slot[$index];
                                        unset($final_slot[$index]);
                                    }
                                }
                            //}
                        } else {

                            if (isset($avaliale_slot[$key])) {
                                array_push($final_slot, $avaliale_slot[$key]);
                            }
                        }
                    }
                } else {
                    $final_slot = $avaliale_slot;
                }
            }
        }
        #echo '<pre>'; print_r($final_slot); die;
		
        $resp_slots = $final_slot;
        $final_slot=array();
        $continueslot=FALSE;
        // Adding travel time to slot time, by Aditya on 12.03.2017
        foreach ($resp_slots as $k => $v) {
            
			/*
			#pr($resp_slots);
            //Checking for continues slot avalibility check, by KATHAK DABHI 18.05.2017
            if($num_of_slots>1){
                for($i=0;$i<$num_of_slots-1 && $k+$num_of_slots-1<count($resp_slots);$i++){
                    if($resp_slots[$k+$i]['end_time']==$resp_slots[$k+$i+1]['start_time']){
						#echo 'if';
                        $continueslot=TRUE;
                    }else{
						#echo 'else';
                        $continueslot=FALSE;
                    }
                }
                if($continueslot && $num_of_slots>1){
                    $final_slot[$k]=$v;
                    $now = new DateTime($final_slot[$k]['start_time']); //current date/time
                    $now->add(new DateInterval("PT{$schedularData['Schedular']['travel_time']}M"));
                    $final_slot[$k]['start_time'] = $now->format('H:i');
                }
                $continueslot=FALSE;
				
				$final_slot[$k]=$v;
                    $now = new DateTime($final_slot[$k]['start_time']); //current date/time
                    $now->add(new DateInterval("PT{$schedularData['Schedular']['travel_time']}M"));
                    $final_slot[$k]['start_time'] = $now->format('H:i');
            }else{
                $final_slot[$k]=$v;
                $now = new DateTime($final_slot[$k]['start_time']); //current date/time
                $now->add(new DateInterval("PT{$schedularData['Schedular']['travel_time']}M"));
                $final_slot[$k]['start_time'] = $now->format('H:i');
            }
			*/
			
			// By Aditya on 07.09.2017, Above code commented and added below lines to show last slot for appt
			$final_slot[$k]=$v;
			$now = new DateTime($final_slot[$k]['start_time']); //current date/time
			$now->add(new DateInterval("PT{$schedularData['Schedular']['travel_time']}M"));
			$final_slot[$k]['start_time'] = $now->format('H:i');
			
        }
		
		
        // Opened by Aditya on 07.09.2017 - to check the last slot start time is not greater than scheduler end time
		// commented as of now - No usage by Aditya 22.06.2017
        #$final_slot = $resp_slots;
        #pr($final_slot);
        #die;
		$newslots = array();
		$cnt = false;
		foreach ($final_slot as $key => $value) {
			
			if($endtimes > $value['start_time'] && $cnt == false){
				array_push($newslots, $value);
			} else
				$cnt = true;
			
		}
		
		// Sort the unique timeslots by Aditya on 04.10.2017
		#print_r($newslots);
		$tmp = array();
		foreach ($newslots as $item) {
			if (!in_array($item['slot_no'], $tmp)) {
				$unique[] = $item;
				$tmp[] = $item['slot_no'];
			}
		}
		
		// Assigning new slots
		$final_slot = array_values($unique);
        
		#print_r($unique);
        #die('hi11');
		
		
		#$final_slot = array_unique($newslots, SORT_REGULAR);
        #$final_slot = array_values($newslots);
		
		
		
		
        #$final_slot = array_unique($final_slot, SORT_REGULAR);
        #$final_slot = array_values($final_slot);
		
        if (!empty($final_slot)) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $final_slot
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['no_slot_available'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_slot_available'],
                'data' => $final_slot
            );
        }

        return $this->fail_safe_return($response);
    }


    function get_order_confirm_details($api_token = '', $customer_id = '', $deviceid = '', $device_type = '') {
		
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }
        $response_data = array();
        $appointmentData = $this->AppointmentDetail->find('all', array('conditions' => array('AppointmentDetail.appointment_confirmed' => 0,
                'AppointmentDetail.customer_id' => $customer_id), 'group' => array('AppointmentDetail.guest_id', 'AppointmentDetail.appointment_date')));



        if (is_array($appointmentData) && count($appointmentData) > 0) {
            $appoint_confrim = array();
            $s_price = '0.000';
            foreach ($appointmentData as $key => $value) {
                $confirm['appointment_date'] = date('d-m-Y', strtotime($value['AppointmentDetail']['appointment_date']));
                $confirm['appointmentdetail_id'] = $value['AppointmentDetail']['id'];
                $confirm['appointment_id'] = $value['AppointmentDetail']['appointment_id'];
                $confirm['appointment_day'] = date("l", strtotime($value['AppointmentDetail']['appointment_date']));
				
				$confirm['ispaid'] = $value['AppointmentDetail']['ispaid']; // added by Aditya 21.06.2017
				
                #$confirm['start_time'] = date('H:i', strtotime($value['AppointmentDetail']['start_time']));
                #$confirm['end_time'] = date('H:i', strtotime($value['AppointmentDetail']['end_time']));

                $confirm['travel_start_time'] = date('H:i', strtotime($value['AppointmentDetail']['travel_start_time']));
                $confirm['travel_end_time'] = date('H:i', strtotime($value['AppointmentDetail']['travel_end_time']));

                $confirm['start_time'] = date('H:i', strtotime($value['AppointmentDetail']['start_time']));
                $confirm['end_time'] = date('H:i', strtotime($value['AppointmentDetail']['end_time']));

                $confirm['clean_start_time'] = date('H:i', strtotime($value['AppointmentDetail']['clean_start_time']));
                $confirm['clean_end_time'] = date('H:i', strtotime($value['AppointmentDetail']['clean_end_time']));
				
                $confirm['guest_id'] = $value['AppointmentDetail']['guest_id'];
                $confirm['f_name'] = $value['Customer']['f_name'];
                $confirm['l_name'] = $value['Customer']['l_name'];
                $confirm['mobile'] = $value['Customer']['mobile'];
                $confirm['email'] = $value['Customer']['email'];
                $confirm['service_details'] = array();


                $appointmentDetails = $this->AppointmentDetail->find('all', array('conditions' => array('AppointmentDetail.appointment_confirmed' => 0,
                        'AppointmentDetail.appointment_date' => $value['AppointmentDetail']['appointment_date'], 'AppointmentDetail.customer_id' => $customer_id, 'AppointmentDetail.guest_id' => $value['AppointmentDetail']['guest_id'])));

                if (is_array($appointmentDetails) && count($appointmentDetails) > 0) {

                    $service_price = '0.000';

                    $check = false;
                    foreach ($appointmentDetails as $key => $value) {

                        // print_r($appointmentDetails);exit;

                        if (is_array($value['Service']) && count($value['Service']) > 0) {
                            $service = array();
                            foreach ($value['Service'] as $y => $l) {
                                $service_price = $service_price + $l['price'];
                                $s_price = $s_price + $l['price'];
                                array_push($service, array('service_name' => $l['name'], 'service_name_ar' => $l['name_ar'], 'service_price' => $l['price']));
                            }
                            $confirm['total_price'] = number_format((float) $service_price, 3);
							
                            $confirm['service_details'] = $service;
                            $confirm['type'] = 'Service';
                            $confirm['package_details'] = new stdClass();
                        }

                        if (is_array($value['Package']) && count($value['Package']) > 0) {
                            $service = array();
                            if (!empty($value['Package'][0])) {
                                $service_price = $value['Package'][0]['price'];
                                if ($check === false) {
                                    $check = true;
                                    $s_price = $s_price + $value['Package'][0]['price'];
                                }

                                $confirm['type'] = 'Package';
                                $confirm['total_price'] = number_format((float) $service_price, 3);
                                array_push($service, array('service_name' => $value['Package'][0]['name'], 'service_name_ar' => $value['Package'][0]['name_ar'], 'service_price' => $service_price));
                                $confirm['service_details'] = $service;
                                $confirm['package_details'] = new stdClass();
                            }
                        }
                    }
                }
                array_push($appoint_confrim, $confirm);
            }
			

			// get min price from Business Rules by Aditya 22.06.2017
			$getBusinessRuleData = $this->Businessrule->find('first', array('conditions' => array('Businessrule.name' => 'minimum_order', 'Businessrule.id' => 1)));
			$minimum_order = $getBusinessRuleData['Businessrule']['value'];
			//$confirm['minimum_order_price'] = number_format((float) $minimum_order, 3);
			
			
            $response_data = $appoint_confrim;
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'total_price' => number_format((float) $s_price, 3),
				'minimum_order_price' => number_format((float) $minimum_order, 3),
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data,
                'total_price' => '0.000',
				'minimum_order_price' => '0.000'
            );
        }
        return $this->fail_safe_return($response);
    }

    function checkout_order($api_token = '', $deviceid = '', $device_type = '', $user_id = '', $admin_id = '', $address_id = '', $appointment_id = '', $guest_id = '', $payment_method = '', $total_amount = '', $remaining_amount = '', $transaction_id = '', $timezone_time = '') {
		
		#echo $api_token . '=' . $deviceid . '=' . $device_type . '=' . $user_id . '=' . $admin_id . '=' . $address_id . '=' . $appointment_id . '=' . $guest_id . '=' . $payment_method . '=' . $total_amount . '=' . $remaining_amount . '=' . $transaction_id . '=' . $timezone_time ;
		#die;
		
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');
		
        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }
		#echo '<br> 3 <br>';
		/*
        if (is_numeric($user_id)) {
            if (!$this->Customer->exists($user_id)) {
                $response['is_blocked'] = "2";
                $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                $response['message'] = $GLOBALS["Webservice"]["messages"]['invalid_user'];
                $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['invalid_user'];
                return $this->fail_safe_return($response);
            }
        }*/
		
        $customerData = $this->Customer->find('first', array('conditions' => array('Customer.id' => $user_id)));
        #pr($customerData); die('nw here!');
		
        if ($customerData['Customer']['status'] == 2) {
			#pr($customerData); die('2 inside nw here!');
            $response['status'] = "1";
            $response['is_blocked'] = "2";
            $response['message'] = $GLOBALS["Webservice"]["messages"]['customer_blocked'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['customer_blocked'];
            return $this->fail_safe_return($response);
        }
		
		#pr($customerData); die('1 inside nw here!');
		
        $response_data = array();

        $time = rand(0, time());
        $receipt_num = "ROLL" . $time;
        $area = $this->CustomerAddress->find('first', array('fields' => array('area_id'), 'conditions' => array('CustomerAddress.id' => $address_id)));
        #pr($area);
        #die;

        $getBusinessRuleData = $this->Businessrule->find('first', array('conditions' => array('Businessrule.name' => 'minimum_order', 'Businessrule.id' => 1)));
        $minimum_order = $getBusinessRuleData['Businessrule']['value'];

        $appointmentData = $this->Appointment->find('first', array('conditions' => array('Appointment.id' => $appointment_id)));


        $service = array();
        $appointments_data = array();
        $this->Order->create();
        $this->request->data['Order']['id'] = '';
        $this->request->data['Order']['customer_id'] = $user_id;
        $this->request->data['Order']['payment_method'] = $payment_method;
        $this->request->data['Order']['receipt_num'] = $receipt_num;
        $this->request->data['Order']['admin_id'] = $admin_id;
        $this->request->data['Order']['area_id'] = $area['CustomerAddress']['area_id'];
        $this->request->data['Order']['customer_address_id'] = $address_id;
        $this->request->data['Order']['guest_id'] = 1;
		$this->request->data['Order']['total_guests'] = count($guest_id); // new column by Aditya 28.06.2017
        $this->request->data['Order']['appointment_id'] = $appointment_id;
        $this->request->data['Order']['appointment_date'] = $appointmentData['Appointment']['appointment_date'];
        $this->request->data['Order']['start_time'] = $appointmentData['Appointment']['start_time'];
        $this->request->data['Order']['end_time'] = $appointmentData['Appointment']['end_time'];
        $this->request->data['Order']['type'] = $appointmentData['Appointment']['type'];
		
		// Adding patch for order amount less than 20 to show as 20 kd minimum order wise by Aditya on 23.07.2017
		#$total_amount = ($total_amount < 20) ? 20.000 : $total_amount;
		
        $this->request->data['Order']['total'] = number_format((float) $total_amount, 3);

        if ($this->Order->save($this->request->data['Order'])) {
			
            $guest_id = explode(',', $guest_id);

            $appointmentdetail_ids = array();
            if ($admin_id > 0) {
                $appointmentdetailData = $this->AppointmentDetail->find('all', array('conditions' => array('AppointmentDetail.guest_id' => $guest_id, 'AppointmentDetail.appointment_id' => $appointment_id)));
            } else {
                $appointmentdetailData = $this->AppointmentDetail->find('all', array('conditions' => array('AppointmentDetail.guest_id' => $guest_id, 'AppointmentDetail.appointment_id' => $appointment_id, 'AppointmentDetail.appointment_confirmed != ' => '1')));
            }


			

            foreach ($appointmentdetailData as $key => $value) {

                $this->OrderData->create();
                $this->request->data['OrderData']['id'] = '';
                $this->request->data['OrderData']['slot_no'] = $value['AppointmentDetail']['slot_no'];
                $this->request->data['OrderData']['order_id'] = $this->Order->id;
                $this->request->data['OrderData']['receipt_num'] = $receipt_num;

                $this->request->data['OrderData']['travel_start_time'] = $value['AppointmentDetail']['travel_start_time'];
                $this->request->data['OrderData']['travel_end_time'] = $value['AppointmentDetail']['travel_end_time'];
                $this->request->data['OrderData']['clean_start_time'] = $value['AppointmentDetail']['clean_start_time'];
                $this->request->data['OrderData']['clean_end_time'] = $value['AppointmentDetail']['clean_end_time'];

                $this->request->data['OrderData']['start_time'] = $value['AppointmentDetail']['start_time'];
                $this->request->data['OrderData']['end_time'] = $value['AppointmentDetail']['end_time'];
                $this->request->data['OrderData']['type'] = $value['AppointmentDetail']['type'];
                $this->request->data['OrderData']['appointmentdetail_id'] = $value['AppointmentDetail']['id'];
                $this->request->data['OrderData']['appointment_id'] = $value['AppointmentDetail']['appointment_id'];
                $this->request->data['OrderData']['guest_id'] = $value['AppointmentDetail']['guest_id'];
                $this->request->data['OrderData']['appointment_date'] = $value['AppointmentDetail']['appointment_date'];
                $this->request->data['OrderData']['customer_id'] = $user_id;

                if ($this->OrderData->save($this->request->data)) {
					#die('31 inside nw here!');
                    array_push($appointmentdetail_ids, $value['AppointmentDetail']['id']);

                    if (is_array($value['Service']) && count($value['Service']) > 0) {

                        foreach ($value['Service'] as $y => $l) {
                            $this->OrderService->create();
                            $orderService = array();
                            $orderService['service_id'] = $l['id'];
                            $orderService['order_data_id'] = $this->OrderData->id;
                            $orderService['guest_id'] = $value['AppointmentDetail']['guest_id'];
                            $orderService['appointment_id'] = $value['AppointmentDetail']['appointment_id'];
                            $orderService['price'] = $l['price'];
                            $this->OrderService->save($orderService);
                        }
                    }

                    if (is_array($value['Package']) && count($value['Package']) > 0) {

                        foreach ($value['Package'] as $s => $t) {
                            $this->OrderPackage->create();
                            $orderPackage = array();
                            $orderPackage['package_id'] = $t['id'];
                            $orderPackage['order_data_id'] = $this->OrderData->id;
                            $this->OrderPackage->save($orderPackage);
                        }
                    }

                    $appointments_data[$key]['service'] = $service;
                    $customerAddress = $this->CustomerAddress->find('first', array('conditions' => array('CustomerAddress.id' => $address_id)));
                    $this->Governorate->unbindModel(array('hasMany' => array('Area')));
                    #pr($customerAddress);

                    $governorate_name = $this->Governorate->find('first', array('fields' => array('Governorate.name_ar', 'Governorate.name'), 'conditions' => array('Governorate.id' => $customerAddress['Area']['governorates_id'])));
                    #pr($governorate_name);
                    #die('here now!');

                    if ($admin_id > 0) {
                        $response_data = array('order_id' => $this->Order->id, 'customer_id' => $user_id, 'order_pdf_link' => SITE_URL . 'uploads/invoice/' . $receipt_num . '.pdf');
                    } else {
                        if ($this->Transaction->updateAll(array('Transaction.order_id' => $this->Order->id), array('Transaction.id' => $transaction_id))) {
                            $response_data = array('order_id' => $this->Order->id, 'customer_id' => $user_id, 'order_pdf_link' => SITE_URL . 'uploads/invoice/' . $receipt_num . '.pdf');
                            $transactionData = $this->Transaction->find('first', array('fields' => array('PaymentID', 'Result', 'TranID', 'Auth', 'Ref', 'TrackID', 'PostDate'), 'conditions' => array('Transaction.id' => $transaction_id)));
                            #$response_data['transction'] = array($transactionData['Transaction']);
							$response_data = $transactionData['Transaction'];
                        }
                    }
                }
            }
            #die('outside!');

            $this->AppointmentDetail->updateAll(array('AppointmentDetail.appointment_confirmed' => 1), array('AppointmentDetail.id' => $appointmentdetail_ids));
			
			

            // Get Appointment Details information
            $appointmentData = $this->OrderData->find('all', array('conditions' => array('OrderData.order_id' => $this->Order->id, 'OrderData.appointment_id' => $appointment_id), 'group' => 'OrderData.guest_id'));
            #pr($appointmentData); #die;

			#die('212 inside nw here!');

            if (is_array($appointmentData) && count($appointmentData) > 0) {

                $appoint_confrim = array();
                $s_price = '0.000';

                foreach ($appointmentData as $key => $value) {

                    $confirm['appointment_date'] = date('d-m-Y', strtotime($value['OrderData']['appointment_date']));
                    $confirm['OrderData_id'] = $value['OrderData']['id'];
                    $confirm['appointment_id'] = $value['OrderData']['appointment_id'];
                    $confirm['appointment_day'] = date("l", strtotime($value['OrderData']['appointment_date']));

                    $confirm['travel_start_time'] = date('H:i:s', $value['AppointmentDetail']['travel_start_time']);
                    $confirm['travel_end_time'] = date('H:i:s', $value['AppointmentDetail']['travel_end_time']);
                    $confirm['clean_start_time'] = date('H:i:s', $value['AppointmentDetail']['clean_start_time']);
                    $confirm['clean_end_time'] = date('H:i:s', $value['AppointmentDetail']['clean_end_time']);

                    $confirm['start_time'] = date('H:i:s', strtotime($value['OrderData']['start_time']));
                    $confirm['end_time'] = date('H:i:s', strtotime($value['OrderData']['end_time']));
                    $confirm['guest_id'] = $value['OrderData']['guest_id'];

                    $confirm['service_details'] = array();

                    $appointmentDetails = $this->OrderData->find('all', array('conditions' => array('OrderData.guest_id' => $value['OrderData']['guest_id'], 'OrderData.order_id' => $this->Order->id, 'OrderData.appointment_id' => $appointment_id)));
                    // echo ""; print_r($appointmentDetails); exit;

                    $check = false;

                    if (is_array($appointmentDetails) && count($appointmentDetails) > 0) {

                        $service = array();
                        $service_price = '0.000';
                        foreach ($appointmentDetails as $key => $value) {
                            if (is_array($value['Service']) && count($value['Service']) > 0) {
                                foreach ($value['Service'] as $y => $l) {
                                    $service_price = $service_price + $l['price'];
                                    $s_price = $s_price + $l['price'];
                                    array_push($service, array('type' => '1', 'service_name' => $l['name'], 'service_name_ar' => $l['name_ar'], 'service_price' => $l['price']));
                                }
                                $confirm['total_price'] = number_format((float) $service_price, 3);
                                $confirm['service_details'] = $service;
                                $confirm['type'] = '1';
                            }

                            if (is_array($value['Package']) && count($value['Package']) > 0) {
                                if (!empty($value['Package'][0])) {

                                    if ($check === false) {
                                        $service_price = $value['Package'][0]['price'];
                                        $check = true;
                                        $s_price = $s_price + $value['Package'][0]['price'];
                                        array_push($service, array('type' => '2', 'service_name' => $value['Package'][0]['name'], 'service_name_ar' => $value['Package'][0]['name_ar'], 'service_price' => $service_price));
                                    }

                                    $confirm['service_details'] = $service;
                                    $confirm['type'] = '2';
                                }
                            }
                        }
                    }
                    $total_amount = number_format((float) $s_price, 3);

                    array_push($appoint_confrim, $confirm);
                }

				#die('21 inside nw here!');

                // added code inside loop to overcome blank email issue by Aditya on 02.05.2017
                if ($total_amount < $minimum_order) {
                    $remaining_amount = $getBusinessRuleData['Businessrule']['value'] - $total_amount;
                }
                $this->Order->updateAll(array('Order.total' => $total_amount, 'Order.remaining_amount' => $remaining_amount), array('Order.id' => $this->Order->id));
                $final_amount = $remaining_amount + $total_amount;
						
                #pr($customerData);
                #pr($appoint_confrim);
                #die('here now!');
                // pdf generation
                $paymentID = '';
                $tranID = '';

                $html = '';

                $html .= '<!DOCTYPE><html><head><style>
									table.order-detail{border: 0px solid #ddd;margin-bottom: 10px;}
									table.order-detail th, table.order-detail td{border: 1px solid #ddd;}
								  .img-responsive{width:120px;max-width:100%;}
								  .pad-top{ padding-top:0px !important}
								  address, dl {margin-bottom: 0px;}
								  .head-title{line-height:40px;}
								  .h4, h4 {
										font-size: 14px;
									 }
	
								  </style>
							  </head><body>
							  <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:14px;">
							   
	
								<tr height="auto">
								  <td align="left" valign="top" width=35%>
									<img height="48px" width="165px" class="img-responsive" src="http://www.rollingscissors.com/admin/app/webroot/files/rslogo.png">
									<br>
									<span><strong><br>CUSTOMER DETAILS</strong></span><br>';
                $html .= $customerData['Customer']['f_name'] . ' ' . $customerData['Customer']['l_name'] . '<br>';
                $html .= $customerData['Customer']['mobile'] . '<br>';
                $html .= $customerData['Customer']['email'] . '<br><br>';
                $html .= 'Kuwait' . '<br>';
                $html .= $governorate_name['Governorate']['name'] . '<br>';
                $html .= $customerAddress['Area']['areaname'] . '<br>';
                if (!empty($customerAddress['CustomerAddress']['block'])) {
                    $html .= 'Block: ' . $customerAddress['CustomerAddress']['block'] . '<br>';
                }
                if (!empty($customerAddress['CustomerAddress']['street'])) {
                    $html .= 'Street: ' . $customerAddress['CustomerAddress']['street'] . '<br>';
                }
                if (!empty($customerAddress['CustomerAddress']['judda'])) {
                    $html .='Judda: ' . $customerAddress['CustomerAddress']['judda'] . '<br>';
                }
                if (!empty($customerAddress['CustomerAddress']['building'])) {
                    if ($customerAddress['CustomerAddress']['house_type'] == 0) {
                        $html .='House: ' . $customerAddress['CustomerAddress']['building'] . '<br>';
                    } else if ($customerAddress['CustomerAddress']['house_type'] == 1) {
                        $html .='Building: ' . $customerAddress['CustomerAddress']['building'] . '<br>';
                    } else {
                        $html .= 'office: ' . $customerAddress['CustomerAddress']['building'] . '<br>';
                    }
                }
                if (!empty($customerAddress['CustomerAddress']['floor'])) {
                    $html .='Avenue(jadda): ' . $customerAddress['CustomerAddress']['floor'] . '<br>';
                }
                if (!empty($customerAddress['CustomerAddress']['comments'])) {
                    $html .='Extra Direction: ' . $customerAddress['CustomerAddress']['comments'] . '<br>';
                }
                $html .='</td>
								  <td align="center" valign="top">
									<span style="font-size:16px;"><strong>INVOICE</strong></span>
								  </td>
									<td  align="right"  valign="top">
	
									<span><strong>RECEIPT NUMBER</strong></span><br>
									' . $this->Order->id . '
									<br>
									' . date('d-m-Y') . '
										 <br> <br>
	
									<span><strong>PAYMENT METHOD</strong></span><br>';

                $html .= 'KNET: ' . number_format((float) $final_amount, 3) . ' KD<br>';
                if (!empty($transactionData['Transaction']['PaymentID'])) {
                    $html .= '<br><strong>Payment ID</strong><br>';
                    $html .= $transactionData['Transaction']['PaymentID'];
                }
                if (!empty($transactionData['Transaction']['TranID'])) {
                    $html .= '<br><strong> Transaction ID</strong><br>';
                    $html .= $transactionData['Transaction']['TranID'];
                }
                $html .='<br><span><strong>Minimum Order</strong></span><br>';
                $html .=number_format((float) $minimum_order, 3);
                $html .='</td>
								</tr>
							  </table>
							  <table class="order-detail" cellpadding="5" cellspacing="0" border="1" bordercolor="#ddd" width="100%" style="font-size:10px;">
								<tr>
									<td class="" colspan="3" align="center" bgcolor="#f5f5f5" bordercolor="#ddd"><strong>ORDER SUMMARY</strong></td>
								</tr>
								<tr>
									<th  class="item-size" width="15%"><strong>Guest No</strong></th>
									<th class="item-size" align="left" width="55%"><strong>Detail Information</strong></th>
									<th class="text-right price-size" align="right" width="30%"><strong>Price(KD)</strong></th>
								</tr>';
                if (is_array($appoint_confrim) && count($appoint_confrim) > 0) {
                    foreach ($appoint_confrim as $k => $value) {
                        // commented by Aditya on 10.04.2017
                        $startime = date('g:i A', strtotime($value['start_time'] . "$timezone_time second"));
                        #$startime = date('g:i A');
                        $html .= '<tr>
										 <td align="center">' . $value['guest_id'] . '</td>';
                        if ($value['type'] == 1) {
                            $html .='<td class="price-size" align="left" valign="middle"><b>Date</b>: ' . $value['appointment_date'] . '<br><b>Start Time:</b> ' . $value['appointment_day'] . ' ' . $startime . '<br><br><b>Service Name</b><br>';
                        } else {
                            $html .='<td class="price-size" align="left" valign="middle"><b>Date</b>: ' . $value['appointment_date'] . '<br><b>Start Time:</b> ' . $value['appointment_day'] . ' ' . $startime . '<br><br><b>Package Name</b><br>';
                        }


                        if (is_array($value['service_details']) && count($value['service_details']) > 0) {
                            foreach ($value['service_details'] as $k => $v) {
                                $html .= $v['service_name'] . '<br>';
                            }
                        }
                        $html .='</td>
										<td class="text-right total-size" align="right" valign="middle"><br><br><br><br><br>';
                        if (is_array($value['service_details']) && count($value['service_details']) > 0) {
                            foreach ($value['service_details'] as $k => $v) {
                                $html .= $v['service_price'] . '<br>';
                            }
                        }
                        $html .='</td>
									 </tr>';
                    }
                }

                $html .= '<tr>
									<td colspan="2" class="thick-line text-right qty-size" align="right"><strong>Order Amount(KD)</strong></td>
									<td class="thick-line text-right total-size" align="right"><strong>' . number_format((float) $total_amount, 3) . '</strong></td>
								</tr>
								<tr>
									<td colspan="2" class="thick-line text-right qty-size" align="right"><strong>Extra Amount(KD)</strong></td>
									<td class="thick-line text-right total-size" align="right"><strong>' . number_format((float) $remaining_amount, 3) . '</strong></td>
								</tr>
								<tr>
									<td colspan="2" class="thick-line text-right qty-size" align="right"><strong>Total Amount(KD)</strong></td>
									<td class="thick-line text-right total-size" align="right"><strong>' . number_format((float) $final_amount, 3) . '</strong></td>
								</tr>
							
							  </table><br/><br/>
	
							  <table cellpadding="5" cellspacing="0" width="100%" style="font-size:10px;">
								<tr>
									<td align="right"><a title="Toffaha iPhone App" download="myimage" style="margin-right:5px;" href="#"><img border="0" width="97" height="32" src="http://www.rollingscissors.com/admin/assets/images/app-store-btn.png" alt="Rolling Scissors iPhone App" title="Rolling Scissors iPhone App"></a></td>
									<td align="left"><a title="Toffaha Android App" download="myimage" style="margin-left:5px;" href="#"><img border="0" width="97" height="32" src="http://www.rollingscissors.com/admin/assets/images/app-google-btn.png" alt="Rolling Scissors App" title="Rolling Scissors Android App"></a></td>
								</tr>
	
							  </table>
	
							  
							<br/>
							<div style="text-align:center;">
								
								@ ' . date('Y') . ' <strong>ROLLING SCISSORS</strong>, All Rights Reserved. 
								<br />
								
								<a href="http://www.rollingscissors.com" target="_blank">Terms and Conditions</a>
								| 
								<a href="http://www.rollingscissors.com" target="_blank">FAQs</a>
								| 
								<a href="http://www.rollingscissors.com" target="_blank">Contact Us</a>
								
							</div>';


                $html .='</body>
							  </html>';


				#die('12 inside nw here!');

                // echo $html; exit;
                $filename = $receipt_num;
                // echo $html;exit;
                $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                $pdf->setPrintHeader(false);
                $pdf->setPrintFooter(false);
                $pdf->AddPage();
                $pdf->setRTL(false);
                $pdf->setFont('dejavusans', '', 11, '', true);
                $pdf->WriteHTML($html, true, 0, true, 0);

                $pdf->Output(INVOICE_UPLOAD . $filename . '.pdf', 'F');

                if (!empty($customerData['Customer']['email'])) {
                    $this->sendEmail($html, $customerData['Customer']['email'], 'Rolloing Scissors Order', true);
                }
                // added code inside loop to overcome blank email issue by Aditya on 02.05.2017
            }
        }
		
		#pr($response_data);
		
		#die('4 inside nw here!');
		
		if($customerData['Customer']['user_type'] == 2)
			$sttus = 2;
		else
			$sttus = 0;
			
        if (!empty($response_data)) {
            $response = array(
                'status' => $sttus,
                #'message' => $GLOBALS['Webservice']['messages']['order_success'] . ' Recepit ID: ' . $this->Order->id . '.',
                #'message_ar' => $GLOBALS['Webservice']['messages_ar']['order_success'] . ' رقم الفاتورة ' . $this->Order->id . '.',
				
				'message' => 'Your payment is successful for '. ($final_amount) .'KD. Your receipt ID is ' . $this->Order->id . '.',
                'message_ar' => 'تم بنجاح دفع '. ($final_amount) .'KD. رقم الفاتورة ' . $this->Order->id . '.',
				
                'data' => $response_data,
                'is_blocked' => "1"
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data,
                'is_blocked' => "1"
            );
        }

        return $this->fail_safe_return($response);
    }

    function minimun_order_require($api_token, $deviceid, $device_type, $total_amount) {
		
		

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $getBusinessRuleData = $this->Businessrule->find('first', array('conditions' => array('Businessrule.name' => 'minimum_order', 'Businessrule.id' => 1)));
        $response_data['success'] = true;
        if ($total_amount < $getBusinessRuleData['Businessrule']['value']) {
            $remaining_amount = $getBusinessRuleData['Businessrule']['value'] - $total_amount;
            $response_data['success'] = false;
        }

        if ($response_data['success']) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['no_minimun'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_minimun'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
				'message' => $GLOBALS['Webservice']['messages']['minimum_order_validation'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['minimum_order_validation'],
                #'message' => 'Minimum charge is KD 20. Please choose an additional service or tap OK to proceed.',
                #'message_ar' => 'Minimum charge is KD 20. Please choose an additional service or tap OK to proceed.',
                #'message' => 'You need to take minimun ' . number_format((float) $getBusinessRuleData['Businessrule']['value'], 3) . ' KD services  otherwise you need to pay this much extra ' . number_format((float) $remaining_amount, 3) . ' KD',
                #'message_ar' => 'You need to take minimun ' . number_format((float) $getBusinessRuleData['Businessrule']['value'], 3) . ' KD services  otherwise you need to pay this much extra ' . number_format((float) $remaining_amount, 3) . ' KD',
                'data' => $response_data
            );
        }
        return $this->fail_safe_return($response);
    }

    function my_appointment($api_token = '', $customer_id = '', $deviceid = '', $device_type = '') {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }
        $response_data = array();
        $appointmentData = $this->OrderData->find('all', array('conditions' => array(
                'OrderData.customer_id' => $customer_id, 'Order.status !=' => '-1'), 'group' => array('OrderData.appointment_id'), 'order' => array('OrderData.appointment_date' => 'DESC')));

        //    print_r($appointmentData);exit;

        if (is_array($appointmentData) && count($appointmentData) > 0) {

            $appoint_confrim = array();
            $s_price = '0.000';
            $confirm = array();

            foreach ($appointmentData as $key => $value) {

                $confirm['appointment_date'] = date('d-m-Y', strtotime($value['OrderData']['appointment_date']));
                $confirm['OrderData_id'] = $value['OrderData']['id'];
                $confirm['appointment_id'] = $value['OrderData']['appointment_id'];
                $confirm['is_tracking'] = $value['Order']['is_van_running'];

                $confirm['start_time'] = date('H:i', strtotime($value['OrderData']['start_time']));
                $confirm['end_time'] = date('H:i', strtotime($value['OrderData']['end_time']));
                $confirm['guest_id'] = $value['OrderData']['guest_id'];
                $confirm['appointment_day'] = date("l", strtotime($value['OrderData']['appointment_date']));
                $confirm['f_name'] = $value['Customer']['f_name'];
                $confirm['l_name'] = $value['Customer']['l_name'];
                $confirm['mobile'] = $value['Customer']['mobile'];
                $confirm['email'] = $value['Customer']['email'];

                if (isset($value['Order']['status'])) {
                    $confirm['order_status'] = $value['Order']['status'];
                    $confirm['order_id'] = $value['Order']['id'];
                    $confirm['is_rated'] = $value['Order']['is_rated'];
                    $confirm['order_pdf_link'] = SITE_URL . 'uploads/invoice/' . $value['Order']['receipt_num'] . '.pdf';
                }


                $confirm['service_details'] = array();


                $appointmentDetails = $this->OrderData->find('all', array('conditions' => array(
                        'OrderData.customer_id' => $customer_id,
                        'OrderData.appointment_id' => $value['OrderData']['appointment_id'],
                        'OrderData.appointment_date' => $value['OrderData']['appointment_date']),
                    'order' => array('OrderData.created' => 'ASC')));

                #echo '<pre>';
                #print_r($appointmentDetails);
                #die;

                $service = array();
                $sName = $sArName = '';
                if (is_array($appointmentDetails) && count($appointmentDetails) > 0) {

                    $service_price = '0.000';
                    foreach ($appointmentDetails as $key => $value) {
                        if ($key == 0) {
                            $start_times = $value['OrderData']['start_time'];
                            $end_times = $value['OrderData']['end_time'];
                        } else if ($appointmentDetails[$key - 1]['OrderData']['guest_id'] != $value['OrderData']['guest_id']) {
                            $start_times = $value['OrderData']['start_time'];
                            $end_times = $value['OrderData']['end_time'];
                        }
                        if (is_array($value['Service']) && count($value['Service']) > 0) {

                            #print_r($value['Service']);

                            foreach ($value['Service'] as $y => $l) {

                                #print_r($l);

                                $service_price = $service_price + $l['price'];
                                $s_price = $s_price + $l['price'];


                                #$sName .= $l['name'].', ';
                                #$sArName .= $l['name_ar'].', ';
                                #substr($sName, 0, -2)
                                #substr($sArName, 0, -2)
                                #if ($y == 0) {
                                array_push($service, array(
                                    'start_time' => date('H:i', strtotime($start_times)),
                                    'end_time' => date('H:i', strtotime($end_times)),
                                    'appointment_id' => $value['OrderData']['appointment_id'],
                                    'appointment_day' => date("l", strtotime($value['OrderData']['appointment_date'])),
                                    'appointment_date' => date('d-m-Y', strtotime($value['OrderData']['appointment_date'])),
                                    'guest_id' => $value['OrderData']['guest_id'],
                                    'service_name' => $l['name'], 'service_name_ar' => $l['name_ar'], 'service_price' => $l['price']
                                        )
                                );
                                #}
                            }

                            #print_r($service);

                            $confirm['total_price'] = number_format((float) $service_price, 3);
                            #$confirm['service_details'] = $service[count($service)-1];
                            $confirm['service_details'] = $service;
                            $confirm['type'] = 'Service';
                            $confirm['package_details'] = array();
                        }

                        if (is_array($value['Package']) && count($value['Package']) > 0) {

                            foreach ($value['Package'] as $s => $t) {
                                $confirm['total_price'] = number_format((float) $t['price'], 3);
                                array_push($service, array('end_time' => $confirm['end_time'], 'start_time' => $confirm['start_time'],
                                    'appointment_day' => date("l", strtotime($value['OrderData']['appointment_date'])), 'appointment_date' => date('d-m-Y', strtotime($value['OrderData']['appointment_date'])), 'appointment_id' => $value['OrderData']['appointment_id'], 'guest_id' => $value['OrderData']['guest_id'], 'service_name' => $t['name'], 'service_name_ar' => $t['name_ar'], 'service_price' => $t['price']));
                            }
                            $confirm['service_details'] = array($service[0]);
                            $confirm['package_details'] = array();

                            $confirm['type'] = 'Package';
                        }
                    }
                }
                array_push($appoint_confrim, $confirm);
            }

            $response_data = $appoint_confrim;
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data,
                'total_price' => '0.000'
            );
        }
        return $this->fail_safe_return($response);
    }
	

    function cancel_appointment($api_token, $order_id, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }


        $updateOrderCancel = $this->Order->updateAll(array('Order.status' => '2'), array('Order.id' => $order_id));
        
        if ($updateOrderCancel) {
        	$orderDetail = $this->Order->find('first', array('conditions' => array('Order.id' => $order_id)));

            if (isset($orderDetail['Order']['customer_id']) && $orderDetail['Order']['customer_id'] > 0) {
            	$this->sendPushNotiBuddy("Dear Client, Your appointment is cancelled.", "عزيزي العميل، يتم إلغاء موعدك.", $orderDetail['Order']['customer_id'], NULL, NULL, NULL);
            }
            // $deleteAppointment = $this->AppointmentDetail->deleteAll(array('AppointmentDetail.appointment_id'=>$appointment_id,'AppointmentDetail.guest_id'=>$guest_id));
            // if($deleteAppointment) {
            //      
            // }  

            $response_data['success'] = true;
        }

        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['appointment_cancelled_sccuess'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_cancelled_sccuess'],
                'data' => array()
            );
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['appointment_cancelled_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_cancelled_error'],
                'data' => array()
            );
        }
        return $this->fail_safe_return($response);
    }
	
	function cancel_appointment_v2($api_token, $order_id, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }


        $updateOrderCancel = $this->Order->updateAll(array('Order.status' => '2','Order.is_van_running' => '0','Order.is_service_running' => '0'), array('Order.id' => $order_id));
        
        if ($updateOrderCancel) {
        	$orderDetail = $this->Order->find('first', array('conditions' => array('Order.id' => $order_id)));

            if (isset($orderDetail['Order']['customer_id']) && $orderDetail['Order']['customer_id'] > 0) {
            	$this->sendPushNotiBuddy("Dear Client, Your appointment is cancelled.", "عزيزي العميل، يتم إلغاء موعدك.", $orderDetail['Order']['customer_id'], NULL, NULL, NULL);
            }
            // $deleteAppointment = $this->AppointmentDetail->deleteAll(array('AppointmentDetail.appointment_id'=>$appointment_id,'AppointmentDetail.guest_id'=>$guest_id));
            // if($deleteAppointment) {
            //      
            // }  

            $response_data['success'] = true;
        }

        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['appointment_cancelled_sccuess'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_cancelled_sccuess'],
                'data' => array()
            );
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['appointment_cancelled_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_cancelled_error'],
                'data' => array()
            );
        }
        return $this->fail_safe_return($response);
    }

    function complete_appointment($api_token, $order_id, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }


        $updateOrderCancel = $this->Order->updateAll(array('Order.status' => '3'), array('Order.id' => $order_id));
        if ($updateOrderCancel) {
            // $deleteAppointment = $this->AppointmentDetail->deleteAll(array('AppointmentDetail.appointment_id'=>$appointment_id,'AppointmentDetail.guest_id'=>$guest_id));
            // if($deleteAppointment) {
            //      
            // }  
        	/*$orderDetail = $this->Order->find('first', array('conditions' => array('Order.id' => $order_id)));

            if (isset($orderDetail['Order']['customer_id']) && $orderDetail['Order']['customer_id'] > 0) {
            	$this->sendPushNotiBuddy("Your appointment has been completed", "تم الانتهاء من موعدك", $orderDetail['Order']['customer_id'], NULL, NULL, NULL);
            }*/
            $response_data['success'] = true;
        }

        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['appointment_completed_sccuess'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_completed_sccuess'],
                'data' => array()
            );
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['appointment_completed_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_completed_error'],
                'data' => array()
            );
        }
        return $this->fail_safe_return($response);
    }

    function get_order_listing($api_token = '', $deviceid = '', $device_type = '', $start_date = '', $end_date = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');


        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        if (!empty($start_date) && !empty($end_date)) {
            $options['conditions'] = array(
                'Order.created <=' => "$end_date",
                'Order.created >=' => "$start_date"
            );
        } else {
            $options['conditions'] = array();
        }
		
		// cond added by Aditya on 04.10.2017
		$options['conditions'] = array('Order.status !=' => -1);
		
        $date_offset = '';
        if (empty($date_offset)) {
            $date_offset = date('Y-m-d H:i:s');
        }

        if ($page > 0) {
            --$page;
        }
        $page = (int) $page;

        $pagesize = (int) $pagesize;
        if ($pagesize <= 0) {
            $pagesize = 1;
        }
        $response_data = array();




        $total_records = $this->Order->find("count", $options);

        //print_r($this->getLastQuery('Order'));exit;


        if ($total_records > 0) {
            $total_pages = ceil($total_records / $pagesize);

            if ($page > $total_pages) {
                $response_data['order_data'] = [];
            } else {

                $offset = $page * $pagesize;
                $limit = $pagesize;
                $options['order'] = array('Order.created' => 'desc');
                $options['limit'] = $limit;
                $options['offset'] = $offset;
                //print_r($options);exit;
                $getOrder = $this->Order->find("all", $options);
                //  print_r($this->getLastQuery('Order'));exit;
                if (is_array($getOrder) && count($getOrder) > 0) {

                    foreach ($getOrder as $key => $value) {
						
						// Wrong link commented by Aditya on 03.10.2017
                        #$getOrder[$key]['Order']['order_pdf_link'] = INOVICE_IMG_PATH . '/' . $value['Order']['receipt_num'] . '.pdf';
						$getOrder[$key]['Order']['order_pdf_link'] = SITE_URL . 'uploads/invoice/' . $value['Order']['receipt_num'] . '.pdf'; 
                        $getOrder[$key]['Order']['created'] = date('d-m-Y', strtotime($value['Order']['created']));
                        $getOrder[$key]['Order']['modified'] = date('d-m-Y', strtotime($value['Order']['modified']));
                    }
                    //print_r($getOrder);exit;
                    $getOrder = Set::extract('/Order/.', $getOrder);
                    $response_data['order_data'] = $getOrder;

                    $response_data['total_pages'] = "$total_pages";
                    $response_data['date_offset'] = $date_offset;
                    $page = $page + 1;
                    $response_data['current_page'] = "$page";
                }
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }
	
	function get_order_listing_v2($api_token = '', $deviceid = '', $device_type = '', $start_date = '', $end_date = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');


        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        if (!empty($start_date) && !empty($end_date)) {
            $options['conditions'] = array(
                'Order.created <=' => "$end_date",
                'Order.created >=' => "$start_date",
				'Order.status !=' => -1
            );
        } else {
            //$options['conditions'] = array();
			$options['conditions'] = array('Order.status !=' => -1);
        }
		
		// cond added by Aditya on 04.10.2017
		//$options['conditions'] = array('Order.status !=' => -1);
		
        $date_offset = '';
        if (empty($date_offset)) {
            $date_offset = date('Y-m-d H:i:s');
        }

        if ($page > 0) {
            --$page;
        }
        $page = (int) $page;

        $pagesize = (int) $pagesize;
        if ($pagesize <= 0) {
            $pagesize = 1;
        }
        $response_data = array();




        $total_records = $this->Order->find("count", $options);

        //print_r($this->getLastQuery('Order'));exit;


        if ($total_records > 0) {
            $total_pages = ceil($total_records / $pagesize);

            if ($page > $total_pages) {
                $response_data['order_data'] = [];
            } else {

                $offset = $page * $pagesize;
                $limit = $pagesize;
                $options['order'] = array('Order.created' => 'desc');
                $options['limit'] = $limit;
                $options['offset'] = $offset;
                //print_r($options);exit;
                $getOrder = $this->Order->find("all", $options);
                //print_r($this->getLastQuery('Order'));exit;
                if (is_array($getOrder) && count($getOrder) > 0) {

                    foreach ($getOrder as $key => $value) {
						
						// Wrong link commented by Aditya on 03.10.2017
                        #$getOrder[$key]['Order']['order_pdf_link'] = INOVICE_IMG_PATH . '/' . $value['Order']['receipt_num'] . '.pdf';
						$getOrder[$key]['Order']['order_pdf_link'] = SITE_URL . 'uploads/invoice/' . $value['Order']['receipt_num'] . '.pdf'; 
                        $getOrder[$key]['Order']['created'] = date('d-m-Y', strtotime($value['Order']['created']));
                        $getOrder[$key]['Order']['modified'] = date('d-m-Y', strtotime($value['Order']['modified']));
                    }
                    //print_r($getOrder);exit;
                    $getOrder = Set::extract('/Order/.', $getOrder);
                    $response_data['order_data'] = $getOrder;

                    $response_data['total_pages'] = "$total_pages";
                    $response_data['date_offset'] = $date_offset;
                    $page = $page + 1;
                    $response_data['current_page'] = "$page";
                }
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }
	
    function get_transaction_listing($api_token = '', $deviceid = '', $device_type = '', $start_date = '', $end_date = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');


        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }





        $date_offset = '';
        if (empty($date_offset)) {
            $date_offset = date('Y-m-d H:i:s');
        }

        if ($page > 0) {
            --$page;
        }
        $page = (int) $page;

        $pagesize = (int) $pagesize;
        if ($pagesize <= 0) {
            $pagesize = 1;
        }
        $response_data = array();

        if (!empty($start_date) && !empty($end_date)) {
            $options['conditions'] = array(
                'Transaction.created <=' => "$end_date",
                'Transaction.created >=' => "$start_date"
            );
        } else {
            $options['conditions'] = array();
        }


        $total_records = $this->Transaction->find("count", $options);


        if ($total_records > 0) {
            $total_pages = ceil($total_records / $pagesize);

            if ($page > $total_pages) {
                $response_data['transaction_data'] = [];
            } else {

                $offset = $page * $pagesize;
                $limit = $pagesize;

                $options['order'] = array('Transaction.created' => 'desc');
                $options['limit'] = $limit;
                $options['offset'] = $offset;
                $getCustomer = $this->Transaction->find("all", $options);

                if (is_array($getCustomer) && count($getCustomer) > 0) {

                    $getCustomer = Set::extract('/Transaction/.', $getCustomer);
                    $response_data['transaction_data'] = $getCustomer;

                    $response_data['total_pages'] = "$total_pages";
                    $response_data['date_offset'] = $date_offset;
                    $page = $page + 1;
                    $response_data['current_page'] = "$page";
                }
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function add_rating_reviews($api_token, $customer_id, $review, $comments, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $rating = array();
        $rating['id'] = NULL;
        $rating['customer_id'] = $customer_id;
        $rating['comments'] = $comments;
        $rating['review'] = $review;

        if ($this->RatingReview->save($rating)) {
            #die('3 here!');
            $ratingReviewData = $this->RatingReview->find('first', array('fields' => array('count(RatingReview.id) as ratingcount', 'sum(RatingReview.review) as sumrating')));
            #die('2 here!');
            #pr($ratingReviewData);
            #die;

            $avgRating = $ratingReviewData[0]['sumrating'] / $ratingReviewData[0]['ratingcount'];
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['rating_review_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['rating_review_success'],
                'data' => array('average_ratings' => (string) round($avgRating, 2))
            );
        } else {

            #die('1 here! 1');

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['rating_review_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['rating_review_error'],
                'data' => array()
            );
        }
        return $this->fail_safe_return($response);
    }

    function get_rating_reviews($api_token, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $ratingReviewData = $this->RatingReview->find('first', array('fields' => array('count(RatingReview.id) as ratingcount', 'sum(RatingReview.review) as sumrating')));
        $avgRating = $ratingReviewData[0]['sumrating'] / $ratingReviewData[0]['ratingcount'];
        $response = array(
            'status' => $GLOBALS["Webservice"]["codes"]['success'],
            'message' => $GLOBALS['Webservice']['messages']['rating_review_success'],
            'message_ar' => $GLOBALS['Webservice']['messages_ar']['rating_review_success'],
            'data' => array('average_ratings' => (string) round($avgRating, 2))
        );
        return $this->fail_safe_return($response);
    }

    function get_parent_service($api_token, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $get_service_data = $this->Service->find('all', array('fields' => array('Service.name', 'Service.name_ar', 'Service.id', 'Service.service_type'), 'conditions' => array('Service.parent_id' => '0')));

        // if(is_array($get_service_data) && count($get_service_data) > 0) {
        //     $response_data = $get_service_data;
        //     $response_data = Set::extract('/Service/.', $response_data);
        // }

        if (is_array($get_service_data) && count($get_service_data) > 0) {
            $g = 0;
            $y = 0;
            foreach ($get_service_data as $key => $value) {
                // echo '<pre>'; print_r($value);
                if ($value['Service']['service_type'] == '0') {
                    $response_data['gentelman'][$g]['id'] = $value['Service']['id'];
                    $response_data['gentelman'][$g]['name'] = $value['Service']['name'];
                    $response_data['gentelman'][$g]['name_ar'] = $value['Service']['name_ar'];
                    $response_data['gentelman'][$g]['service_type'] = $value['Service']['service_type'];
                    $g++;
                } else {
                    $response_data['young'][$y]['id'] = $value['Service']['id'];
                    $response_data['young'][$y]['name'] = $value['Service']['name'];
                    $response_data['young'][$y]['name_ar'] = $value['Service']['name_ar'];
                    $response_data['young'][$y]['service_type'] = $value['Service']['service_type'];
                    $y++;
                }
            }
        }

        // $log = $this->Service->getDataSource()->getLog(false, false); echo "<pre>"; print_r($log); exit;
        // echo '<pre>'; print_r($response_data);
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => array()
            );
        }

        return $this->fail_safe_return($response);
    }

    function get_sub_service($api_token, $deviceid, $device_type, $parent_id) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        if (empty($package_id)) {
            $get_service_data = $this->Service->find('all', array('conditions' => array('Service.parent_id !=' => '0')));
        } else {
            $get_service_data = $this->Service->find('all', array('conditions' => array('Service.parent_id' => $parent_id)));
        }


        if (is_array($get_service_data) && count($get_service_data) > 0) {
            $response_data = $get_service_data;
            $response_data = Set::extract('/Service/.', $response_data);
        }

        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => array()
            );
        }

        return $this->fail_safe_return($response);
    }

    function delete_appointment($api_token, $order_id, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $orderDetail = $this->Order->find('first', array('conditions' => array('Order.id' => $order_id)));

        if (isset($orderDetail['Order']['id']) && $orderDetail['Order']['id'] > 0) {
            $updateOrderDelete = $this->Order->updateAll(array('Order.status' => '-1'), array('Order.id' => $order_id));

            if ($updateOrderDelete) {
                $this->AppointmentDetail->updateAll(array('AppointmentDetail.appointment_confirmed' => '-1'), array('AppointmentDetail.appointment_id' => $orderDetail['Order']['appointment_id'], 'AppointmentDetail.guest_id' => $orderDetail['Order']['guest_id']));
                $this->Appointment->updateAll(array('Appointment.status' => '-1'), array('Appointment.id' => $orderDetail['Order']['appointment_id']));

            	/*$orderDetail = $this->Order->find('first', array('conditions' => array('Order.id' => $order_id)));

                if (isset($orderDetail['Order']['customer_id']) && $orderDetail['Order']['customer_id'] > 0) {
                	$this->sendPushNotiBuddy("Your appointment has been deleted", "تم حذف موعدك", $orderDetail['Order']['customer_id'], NULL, NULL, NULL);
                }*/
                $response_data['success'] = true;
            }

            if (!empty($response_data)) {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['appointment_delete_sccuess'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_delete_sccuess'],
                    'data' => array()
                );
            } else {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['appointment_delete_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_delete_error'],
                    'data' => array()
                );
            }
            return $this->fail_safe_return($response);
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['order_no_found'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['order_no_found'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        }
    }

    function get_admin_services_listing($api_token = '', $deviceid = '', $device_type = '', $coloum_name = '', $search_text = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');


        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }
        $date_offset = '';
        if (empty($date_offset)) {
            $date_offset = date('Y-m-d H:i:s');
        }

        if ($page > 0) {
            --$page;
        }
        $page = (int) $page;

        $pagesize = (int) $pagesize;
        if ($pagesize <= 0) {
            $pagesize = 1;
        }
        $response_data = array();

        if (!empty($coloum_name)) {
            $options['conditions'] = array("Service.$coloum_name LIKE" => "%$search_text%", "Service.status" => "1", 'Service.parent_id' => "0");
        } else {
            $options['conditions'] = array("Service.status" => "1", 'Service.parent_id' => "0");
        }



        $total_records = $this->Service->find("count", $options);


        if ($total_records > 0) {
            $total_pages = ceil($total_records / $pagesize);

            if ($page > $total_pages) {
                $response_data['transaction_data'] = [];
            } else {

                $offset = $page * $pagesize;
                $limit = $pagesize;

                if (!empty($coloum_name)) {
                    $options['conditions'] = array("Service.$coloum_name LIKE" => "%$search_text%", "Service.status" => "1", 'Service.parent_id' => "0");
                } else {
                    $options['conditions'] = array("Service.status" => "1", 'Service.parent_id' => "0");
                }
                $options['order'] = array('Service.created' => 'desc');
                $options['limit'] = $limit;
                $options['offset'] = $offset;

                if (!empty($coloum_name)) {
                    $getServices = $this->Service->find("all", $options);
                } else {
                    $getServices = $this->Service->find("all", $options);
                }

                if (is_array($getServices) && count($getServices) > 0) {

                    foreach ($getServices as $key => $value) {
                        if (!empty($coloum_name)) {
                            $get_sub_service = $this->Service->find('all', array('conditions' => array("Service.$coloum_name LIKE" => "%$search_text%", "Service.status" => "1", 'Service.parent_id' => $value['Service']['id'])));
                        } else {
                            $get_sub_service = $this->Service->find('all', array('conditions' => array("Service.status" => "1", 'Service.parent_id' => $value['Service']['id'])));
                        }

                        if (is_array($get_sub_service) && count($get_sub_service) > 0) {
                            foreach ($get_sub_service as $k => $v) {
                                $get_sub_service[$k]['Service']['image'] = SERVICES_IMG_PATH . $v['Service']['image'];
                            }
                        }

                        $get_sub_service = Set::extract('/Service/.', $get_sub_service);

                        $getServices[$key]['Service']['subservices'] = $get_sub_service;
                    }



                    $getServices = Set::extract('/Service/.', $getServices);
                    $response_data['service_data'] = $getServices;

                    $response_data['total_pages'] = "$total_pages";
                    $response_data['date_offset'] = $date_offset;
                    $page = $page + 1;
                    $response_data['current_page'] = "$page";
                }
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }
        return $this->fail_safe_return($response);
    }

    function delete_appoitment_slot($api_token, $customer_id, $appointment_data, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $appointment_data = json_decode($appointment_data, true);

        if (is_array($appointment_data) && count($appointment_data) > 0) {
            foreach ($appointment_data as $key => $value) {
                $this->AppointmentDetail->deleteAll(array('AppointmentDetail.appointment_id' => $value['appointment_id'], 'AppointmentDetail.guest_id' => $value['guest_id']));
            }
            $response_data['success'] = true;
        }

        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['appointment_slot_deleted'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_slot_deleted'],
            );
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['appointment_slot_deleted'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_slot_deleted'],
            );
        }
        return $this->fail_safe_return($response);
    }

    public function update_guest_user($api_token, $user_id, $deviceid, $device_type, $password, $email, $user_type) {
        $api_auth_token = 'skip_auth_token';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $user_id, $api_auth_token);

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        if (is_numeric($user_id)) {
            if (!$this->Customer->exists($user_id)) {
                $response['is_blocked'] = "2";
                $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                $response['message'] = $GLOBALS["Webservice"]["messages"]['invalid_user'];
                $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['invalid_user'];
                return $this->fail_safe_return($response);
            }
        }

        if ($password == '') {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
            $response['message'] = $GLOBALS["Webservice"]["messages"]['password_require'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['password_require'];
            return $this->fail_safe_return($response);
        }

        if (!empty($email)) {
            $emailcount = $this->Customer->find('count', array('conditions' => array('Customer.email' => $email, 'Customer.id !=' => $user_id)));
        } else {
            $emailcount = 0;
        }

        if ($emailcount == 0) {

            $profile_data = array();
            $profile_data['id'] = $user_id;
            $profile_data['user_type'] = $user_type;
            $profile_data['password'] = $password;
            $profile_data['email'] = $email;

            $profile_data = $this->security_save($profile_data);

            if ($this->Customer->save($profile_data)) {
                $response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
                $response['message'] = $GLOBALS['Webservice']['messages']['registration_success'];
                $response['message_ar'] = $GLOBALS['Webservice']['messages_ar']['registration_success'];
            } else {
                $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                $response['message'] = $GLOBALS['Webservice']['messages']['registration_error'];
                $response['message_ar'] = $GLOBALS['Webservice']['messages_ar']['registration_error'];
            }
        } else {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
            $response['message'] = $GLOBALS["Webservice"]["messages"]['email_address_alreay_exists'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['email_address_alreay_exists'];
        }

        return $this->fail_safe_return($response);
    }

    function add_vantracks($api_token, $appointment_id, $order_id, $lat, $long, $deviceid, $device_type) {

        $rating['appointment_id'] = $appointment_id;
        $rating['lat'] = $lat;
        $rating['long'] = $long;
        $rating['order_id'] = $order_id;
		/*
		if($this->Vantrack->save($rating))
			echo 'if !';
		else
			echo 'else !';
		die;
		*/
        if ($this->Vantrack->save($rating)) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['tacking_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['tacking_success'],
                'data' => array()
            );
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['tacking_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['tacking_error'],
                'data' => array()
            );
        }
        return $this->fail_safe_return($response);
    }

    function get_appointment_ratings_question($api_token, $deviceid, $device_type) {
        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $get_questions = $this->AppointmentRatingsQuestion->find('all', array('fields' => array('id', 'question'), 'conditions' => array('AppointmentRatingsQuestion.status' => 1)));

        $get_questions = Set::extract('/AppointmentRatingsQuestion/.', $get_questions);

        if (!empty($get_questions)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $get_questions
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => array()
            );
        }

        return $this->fail_safe_return($response);
    }

    function add_appointment_ratings($api_token, $order_id, $ratings, $ratings_questions, $ratings_id, $deviceid, $device_type) {

        $rating['id'] = $order_id;
        $rating['is_rated'] = "1";
        $rating['ratings'] = $ratings;
        $rating['ratings_question'] = $ratings_questions;
        $rating['ratings_id'] = $ratings_id;

        if ($this->Order->save($rating)) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['rating_review_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['rating_review_success'],
                'data' => array()
            );
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['rating_review_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['rating_review_error'],
                'data' => array()
            );
        }
        return $this->fail_safe_return($response);
    }

    function get_vantracking($api_token, $deviceid, $device_type, $appointment_id) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $get_service_data = $this->Vantrack->find('first', array('conditions' => array('Vantrack.appointment_id' => $appointment_id),
            'order' => array('Vantrack.created' => 'desc')));


        $getDestinationAddress = $this->CustomerAddress->find('first', array('fields' => array('id', 'latitude', 'longitude'),
            'conditions' => array('CustomerAddress.id' => $get_service_data['Order']['customer_address_id'])));





        if (is_array($get_service_data) && count($get_service_data) > 0) {
            $response_data['lat'] = $get_service_data['Vantrack']['lat'];
            $response_data['long'] = $get_service_data['Vantrack']['long'];
            $response_data['appointment_id'] = $get_service_data['Vantrack']['appointment_id'];
            $response_data['id'] = $get_service_data['Vantrack']['id'];
        }
        if (isset($getDestinationAddress['CustomerAddress']['id'])) {
            $response_data['destinationLatitude'] = $getDestinationAddress['CustomerAddress']['latitude'];
            $response_data['destinationLongitude'] = $getDestinationAddress['CustomerAddress']['longitude'];
        }

        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => array()
            );
        }

        return $this->fail_safe_return($response);
    }

    function add_vanlogs($api_token, $appointment_id, $order_id, $van_status, $deviceid, $device_type) {



        $ischeckRunning = $this->Order->find('first', array('conditions' => array('Order.appointment_id !=' => $appointment_id, 'OR' => array('Order.is_service_running' => '1', 'Order.is_van_running' => '1'))));

        if (isset($ischeckRunning['Order']['id'])) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['van_running_status'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['van_running_status'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        }

        $van_log['appointment_id'] = $appointment_id;
        $van_log['order_id'] = $order_id;
        $van_log['van_status'] = $van_status;


        if ($this->VanLog->save($van_log)) {
            if ($van_status == 1) {
            	$orderDetail = $this->Order->find('first', array('conditions' => array('Order.id' => $order_id)));
        	    if (isset($orderDetail['Order']['customer_id']) && $orderDetail['Order']['customer_id'] > 0) {
        	    	$this->sendPushNotiBuddy("Dear Client, We are on our way to your home. Open Application -> My Appointments -> Track Van", "عزيزي العميل، نحن في طريقنا إلى منزلك. فتح التطبيق -> تعييناتي -> المسار فان", $orderDetail['Order']['customer_id'], NULL, NULL, NULL);
        	    }
                $this->Order->updateAll(array('Order.is_van_running' => '1'), array('Order.appointment_id' => $appointment_id));
            } else if ($van_status == 2) {
    	    	$orderDetail = $this->Order->find('first', array('conditions' => array('Order.id' => $order_id)));
    		    if (isset($orderDetail['Order']['customer_id']) && $orderDetail['Order']['customer_id'] > 0) {
    		    	$this->sendPushNotiBuddy("Dear Client, We reached your home!", "عزيزي العميل، وصلنا منزلك!", $orderDetail['Order']['customer_id'], NULL, NULL, NULL);
    		    }
                $this->Order->updateAll(array('Order.is_van_running' => '0'), array('Order.appointment_id' => $appointment_id));
            } else if ($van_status == 3) {
    	    	$orderDetail = $this->Order->find('first', array('conditions' => array('Order.id' => $order_id)));
    		    if (isset($orderDetail['Order']['customer_id']) && $orderDetail['Order']['customer_id'] > 0) {
    		    	$this->sendPushNotiBuddy("Dear Client, Your grooming service has been started", "عزيزي العميل، تم بدء خدمة التهيأ", $orderDetail['Order']['customer_id'], NULL, NULL, NULL);
    		    }
                $this->Order->updateAll(array('Order.is_van_running' => '0'), array('Order.appointment_id' => $appointment_id));
                $this->Order->updateAll(array('Order.is_service_running' => '1'), array('Order.appointment_id' => $appointment_id));
            } else if ($van_status == 4) {
    	    	$orderDetail = $this->Order->find('first', array('conditions' => array('Order.id' => $order_id)));
    		    if (isset($orderDetail['Order']['customer_id']) && $orderDetail['Order']['customer_id'] > 0) {
    		    	$this->sendPushNotiBuddy("Dear Client, Thank you for using the service, Please rate the service. Open Application -> My Appointments -> Rate", "عزيزي العميل، أشكركم على استخدام الخدمة، يرجى تقييم الخدمة. فتح التطبيق -> تعييناتي -> معدل", $orderDetail['Order']['customer_id'], NULL, NULL, NULL);
    		    }
                $this->Order->updateAll(array('Order.is_service_running' => '0'), array('Order.appointment_id' => $appointment_id));
            }

            $appointmentData = $this->Order->find('first', array('conditions' => array('Order.appointment_id' => $appointment_id)));

            $appoint = array();
            if (isset($appointmentData['Order']['id']) && $appointmentData['Order']['id'] > 0) {
                $appoint['appointment_id'] = $appointmentData['Order']['appointment_id'];
                $appoint['order_id'] = $appointmentData['Order']['id'];
                $appoint['appointment_date'] = $appointmentData['Order']['appointment_date'];
                $appoint['customer_id'] = $appointmentData['Order']['customer_id'];
                $appoint['start_time'] = $appointmentData['Order']['start_time'];
                $appoint['status'] = $appointmentData['Order']['status'];
                $appoint['end_time'] = $appointmentData['Order']['end_time'];
                $appoint['is_van_running'] = $appointmentData['Order']['is_van_running'];
                $appoint['is_service_running'] = $appointmentData['Order']['is_service_running'];
                $appoint['type'] = $appointmentData['Order']['type'];
                $appoint['order_pdf_link'] = SITE_URL . 'uploads/invoice/' . $appointmentData['Order']['receipt_num'] . '.pdf';
                $appoint['customer_name'] = $appointmentData['Customer']['f_name'] . ' ' . $appointmentData['Customer']['l_name'];
                $appoint['mobile'] = $appointmentData['Customer']['mobile'];
                $appoint['email'] = $appointmentData['Customer']['email'];
                $appoint['admin_id'] = $appointmentData['Order']['admin_id'];
            }
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['van_log_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['van_log_success'],
                'data' => $appoint
            );
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['van_log_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['van_log_error'],
                'data' => $appoint
            );
        }
        return $this->fail_safe_return($response);
    }

    function get_admin_appointment_rated_listing($api_token = '', $deviceid = '', $device_type = '', $start_date = '', $end_date = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');


        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }



        $date_offset = '';
        if (empty($date_offset)) {
            $date_offset = date('Y-m-d H:i:s');
        }

        if ($page > 0) {
            --$page;
        }
        $page = (int) $page;

        $pagesize = (int) $pagesize;
        if ($pagesize <= 0) {
            $pagesize = 1;
        }
        $response_data = array();

        $options = array();



        if (!empty($start_date) && !empty($end_date)) {
            $options['conditions'] = array(
                'Order.appointment_date <=' => "$end_date",
                'Order.appointment_date >=' => "$start_date",
                'Order.status !=' => '-1',
                'Order.is_rated' => '1'
            );
        }



        $total_records = $this->Order->find("count", $options);


        if ($total_records > 0) {
            $total_pages = ceil($total_records / $pagesize);

            if ($page > $total_pages) {
                $response_data = [];
            } else {

                $offset = $page * $pagesize;
                $limit = $pagesize;



                if (!empty($start_date) && !empty($end_date)) {

                    $getAppointmentData = $this->Order->find("all", array('fields' => array(
                            "Order.start_time", "Order.end_time", "Order.customer_id", "Order.is_van_running", 'Order.is_service_running', "Order.id", 'Order.ratings', 'Order.ratings_question', 'Order.ratings_id', "Order.appointment_date", 'Order.type', 'Order.appointment_id', 'Order.receipt_num', 'Order.status', 'Customer.f_name', 'Customer.l_name', 'Customer.mobile', 'Customer.email', 'Order.admin_id'), 'conditions' => array('Order.is_rated' => '1', 'Order.appointment_date <=' => "$end_date", 'Order.appointment_date >=' => "$start_date"), 'conditions' => array('Order.status !=' => '-1'), 'order' => array('Order.appointment_date' => 'ASC'), 'limit' => $limit, 'offset' => $offset));
                } else {

                    $getAppointmentData = $this->Order->find("all", array('fields' => array(
                            'Order.ratings', 'Order.ratings_question', 'Order.ratings_id', "Order.start_time", "Order.end_time", "Order.customer_id", "Order.is_van_running", 'Order.is_service_running', "Order.id", "Order.appointment_date", 'Order.type', 'Order.appointment_id', 'Order.receipt_num', 'Order.status', 'Customer.f_name', 'Customer.l_name', 'Customer.mobile', 'Customer.email', 'Order.admin_id'), 'conditions' => array('Order.is_rated' => '1', 'Order.status !=' => '-1'), 'order' => array('Order.appointment_date' => 'ASC'), 'limit' => $limit, 'offset' => $offset));
                }

                if (is_array($getAppointmentData) && count($getAppointmentData) > 0) {
                    $appoint = array();
                    foreach ($getAppointmentData as $key => $value) {
                        $appoint[$key]['appointment_id'] = $value['Order']['appointment_id'];
                        $appoint[$key]['ratings_questions'] = $value['Order']['ratings_question'];
                        $appoint[$key]['ratings'] = $value['Order']['ratings'];
                        $appoint[$key]['ratings_id'] = $value['Order']['ratings_id'];
                        $appoint[$key]['order_id'] = $value['Order']['id'];
                        $appoint[$key]['appointment_date'] = $value['Order']['appointment_date'];
                        $appoint[$key]['customer_id'] = $value['Order']['customer_id'];
                        $appoint[$key]['start_time'] = $value['Order']['start_time'];
                        $appoint[$key]['status'] = $value['Order']['status'];
                        $appoint[$key]['end_time'] = $value['Order']['end_time'];
                        $appoint[$key]['is_van_running'] = $value['Order']['is_van_running'];
                        $appoint[$key]['is_service_running'] = $value['Order']['is_service_running'];
                        $appoint[$key]['type'] = $value['Order']['type'];
                        $appoint[$key]['order_pdf_link'] = SITE_URL . 'uploads/invoice/' . $value['Order']['receipt_num'] . '.pdf';
                        $appoint[$key]['customer_name'] = $value['Customer']['f_name'] . ' ' . $value['Customer']['l_name'];
                        $appoint[$key]['mobile'] = $value['Customer']['mobile'];
                        $appoint[$key]['email'] = $value['Customer']['email'];
                        $appoint[$key]['admin_id'] = $value['Order']['admin_id'];
                    }

                    $response_data['appointment_data'] = $appoint;
                    $response_data['total_pages'] = "$total_pages";
                    $response_data['date_offset'] = $date_offset;
                    $page = $page + 1;
                    $response_data['current_page'] = "$page";
                }
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function get_areas_v2($api_token = '', $deviceid = '', $device_type = '', $type = '', $pagesize = 10, $page = 0, $governate_id) {

        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $response_data = $area_list = array();
        if ($type == 0) {
            $date_offset = '';
            if (empty($date_offset)) {
                $date_offset = date('Y-m-d H:i:s');
            }

            if ($page > 0) {
                --$page;
            }

            $page = (int) $page;

            $pagesize = (int) $pagesize;
            if ($pagesize <= 0) {
                $pagesize = 1;
            }
            $response_data = array();
            if (!empty($governate_id) && is_numeric($governate_id)) {
                $options = array('conditions' => array('Governorate.' . $this->Governorate->primaryKey => $governate_id));
            } else {
                $options = array('conditions' => array('Governorate.status' => '1'));
            }

            $total_records = $this->Governorate->find('count', $options);

            if ($total_records > 0) {
                $total_pages = ceil($total_records / $pagesize);

                if ($page > $total_pages) {
                    //$response_data['service_data'] = [];
                } else {

                    $offset = $page * $pagesize;
                    $limit = $pagesize;
                    if (!empty($governate_id) && is_numeric($governate_id)) {
                        $options = array('conditions' => array(
                                'Governorate.' . $this->Governorate->primaryKey => $governate_id,
                                "Governorate.status" => 1
                            ),
                            'limit' => $limit,
                            'offset' => $offset
                        );
                    } else {
                        $options = array('conditions' => array("Governorate.status" => 1),
                            'limit' => $limit,
                            'offset' => $offset
                        );
                    }


                    $response_data_all = $this->Governorate->find('all', $options);

                    if (!empty($response_data_all)) {

                        foreach ($response_data_all as $master_key => $governorate) {
                            $g_name_en = $governorate['Governorate']['name'];
                            $g_name_ar = $governorate['Governorate']['name_ar'];
                            $g_id = $governorate['Governorate']['id'];

                            if (isset($governorate['Area']) && !empty($governorate['Area'])) {
                                foreach ($governorate['Area'] as $key => $area) {
                                    $area_list[$key]['governorate_id'] = $g_id;
                                    $area_list[$key]['area_id'] = $area['id'];
                                    $area_list[$key]['governorate_en'] = $g_name_en;
                                    $area_list[$key]['governorate_ar'] = $g_name_ar;
                                    $area_list[$key]['areaname_en'] = $area['areaname'];
                                    $area_list[$key]['areaname_ar'] = $area['areaname_ar'];
                                    if ($area['status'] != 1) {
                                        unset($area_list[$key]);
                                    }
                                }
                                $response_data['governorates_data'][$master_key] = $area_list;
                            }
                        }
                        $response_data['total_pages'] = strval($total_pages);
                        $response_data['date_offset'] = $date_offset;
                        $response_data['current_page'] = strval($page + 1);
                    }
                }
            }
        } else {

            if (!empty($governate_id) && is_numeric($governate_id)) {
                $options = array('conditions' => array('Governorate.' . $this->Governorate->primaryKey => $governate_id));
            } else {
                $options = array('conditions' => array('Governorate.status' => '1'));
            }
            $response_data_all = $this->Governorate->find('all', $options);
            if (!empty($response_data_all)) {
                foreach ($response_data_all as $master_key => $governorate) {
                    $g_name_en = $governorate['Governorate']['name'];
                    $g_name_ar = $governorate['Governorate']['name_ar'];
                    $g_id = $governorate['Governorate']['id'];

                    if (isset($governorate['Area']) && !empty($governorate['Area'])) {
                        foreach ($governorate['Area'] as $key => $area) {
                            $area_list[$key]['governorate_id'] = $g_id;
                            $area_list[$key]['area_id'] = $area['id'];
                            $area_list[$key]['governorate_en'] = $g_name_en;
                            $area_list[$key]['governorate_ar'] = $g_name_ar;
                            $area_list[$key]['areaname_en'] = $area['areaname'];
                            $area_list[$key]['areaname_ar'] = $area['areaname_ar'];
                            if ($area['status'] != 1) {
                                unset($area_list[$key]);
                            }
                        }
                        $response_data['governorates_data'][$master_key] = $area_list;
                    }
                }
            }
        }


        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function admin_login_v2($api_token = '', $username = '', $password = '', $deviceid = '', $device_type = '') {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $response = array();
        $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];

        if (empty($username) || empty($password)) {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
            $response['message'] = $GLOBALS["Webservice"]["messages"]['auth_Empty_error'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['auth_Empty_error'];
            return $this->fail_safe_return($response);
        }

        if ($this->Customer->validates()) {
            App::uses('Sha256PasswordHasher', 'Controller/Component');
            $passwordHasher = new Sha256PasswordHasher();
            $password = $passwordHasher->hash($password);

            $options['conditions'] = array(
                "User.email" => $username,
                'User.password' => $password
            );


            $check_user = $this->User->find('first', $options);
            if (!empty($check_user['User']['id'])) {
                if ($check_user['User']['status'] == 1) {
                    $user_permissions = json_decode($check_user['User']['user_permissions']);
                    $check_user['User']['user_permissions'] = $this->strip_slashes_recursive($user_permissions);
                    $response['status'] = $GLOBALS["Webservice"]["codes"]['success'];
                    $response['message'] = $GLOBALS["Webservice"]["messages"]['auth_success'];
                    $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['auth_success'];
                    $response['data'] = $check_user;
                } else {
                    $response['status'] = "1";
                    $response['message'] = $GLOBALS["Webservice"]["messages"]['customer_blocked'];
                    $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['customer_blocked'];
                }
            } else {
                $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
                $response['message'] = $GLOBALS["Webservice"]["messages"]['auth_error'];
                $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['auth_error'];
            }
        } else {
            $response['message'] = $this->setServerError($this->Customer->validationErrors);
            $response['message_ar'] = ERROR_INPUT_DATA_AR;
            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
            if ($response['message'] == '') {
                $response['message'] = $GLOBALS["Webservice"]["messages"]['auth_error'];
            }
        }
        $response = $this->setEmptyStringSimple($response);

        return $this->fail_safe_return($response);
    }

    function add_admin_users_v2($api_token, $f_name, $l_name, $email, $mobile, $username, $password, $address, $admin_type, $deviceid, $device_type, $user_permissions, $admin_id) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        if (
                empty($f_name) ||
                empty($l_name) ||
                empty($email) ||
                empty($mobile) ||
                empty($username) ||
                empty($password) ||
                empty($address) ||
                empty($admin_id) ||
                !is_numeric($admin_id)
        ) {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
            $response['message'] = $GLOBALS["Webservice"]["messages"]['required_field_error'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['required_field_error'];
            return $this->fail_safe_return($response);
        }

        if ($admin_id != 1) {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['invalid_access'];
            $response['message'] = $GLOBALS["Webservice"]["messages"]['no_rights_assigned'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['no_rights_assigned'];
            return $this->fail_safe_return($response);
        }

        // check and validate admin permissions data
        $permission_data = json_decode($user_permissions, TRUE);
        if (empty($permission_data) || count($permission_data) == 0) {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['error'];
            $response['message'] = $GLOBALS["Webservice"]["messages"]['invalid_permission_format'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['invalid_permission_format'];
            return $this->fail_safe_return($response);
        }

        $user['f_name'] = $f_name;
        $user['l_name'] = $l_name;
        $user['email'] = $email;
        $user['mobile'] = $mobile;
        $user['username'] = $username;
        $user['password'] = $password;
        $user['address'] = $address;
        $user['device_type'] = $device_type;
        $user['device_id'] = $deviceid;
        $user['user_permissions'] = $user_permissions;
        $user['created_by'] = $admin_id;
        $user['type'] = $admin_type;

        if (!empty($type) && is_numeric($type)) {
            $user['type'] = $admin_type;
        }

        $checkexists = $this->User->find('first', array('conditions' => array('OR' => array('email' => $email, 'username' => $username))));

        if (isset($checkexists['User']['id']) && $checkexists['User']['id'] > 0) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['user_exists'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['user_exists'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        } else {
            if ($this->User->save($user)) {

                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['user_add'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['user_add'],
                    'data' => array()
                );
            } else {
                $response = array(
                    'status' => $GLOBALS["Webservice"]["codes"]['success'],
                    'message' => $GLOBALS['Webservice']['messages']['user_error'],
                    'message_ar' => $GLOBALS['Webservice']['messages_ar']['user_error'],
                    'data' => array()
                );
            }
            return $this->fail_safe_return($response);
        }
    }

    function edit_admin_users_v2($api_token, $f_name, $l_name, $mobile, $address, $deviceid, $user_id, $device_type, $user_permissions, $admin_id) {

        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        if (
                empty($f_name) ||
                empty($l_name) ||
                empty($mobile) ||
                empty($address) ||
                empty($user_id) ||
                empty($admin_id) ||
                !is_numeric($user_id) ||
                !is_numeric($admin_id)
        ) {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['required_field'];
            $response['message'] = $GLOBALS["Webservice"]["messages"]['required_field_error'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['required_field_error'];
            return $this->fail_safe_return($response);
        }

        if ($admin_id != 1 || $user_id == 1) {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['invalid_access'];
            $response['message'] = $GLOBALS["Webservice"]["messages"]['invalid_request'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['invalid_request'];
            return $this->fail_safe_return($response);
        }

        // check and validate admin permissions data
        $permission_data = json_decode($user_permissions, TRUE);
        if (empty($permission_data) || count($permission_data) == 0) {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['required_field'];
            $response['message'] = $GLOBALS["Webservice"]["messages"]['invalid_permission_format'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['invalid_permission_format'];
            return $this->fail_safe_return($response);
        }

        $user['f_name'] = $f_name;
        $user['l_name'] = $l_name;
        $user['mobile'] = $mobile;
        $user['address'] = $address;
        $user['device_type'] = $device_type;
        $user['device_id'] = $deviceid;
        $user['user_permissions'] = $user_permissions;
        $user['created_by'] = $admin_id;
        $user['id'] = $user_id;

        if ($this->User->save($user)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['user_edit'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['user_edit'],
                'data' => array()
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['user_edit_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['user_edit_error'],
                'data' => array()
            );
        }
        return $this->fail_safe_return($response);
    }

    function get_admin_users_v2($api_token = '', $deviceid = '', $device_type = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $date_offset = '';
        if (empty($date_offset)) {
            $date_offset = date('Y-m-d H:i:s');
        }

        if ($page > 0) {
            --$page;
        }
        $page = (int) $page;

        $pagesize = (int) $pagesize;
        if ($pagesize <= 0) {
            $pagesize = 1;
        }
        $response_data = array();

        $options['conditions'] = array('User.type !=' => 1);

        $total_records = $this->User->find("count", $options);
        if ($total_records > 0) {
            $total_pages = ceil($total_records / $pagesize);

            if ($page > $total_pages) {
                $response_data['user_data'] = [];
            } else {
                $offset = $page * $pagesize;
                $limit = $pagesize;
                $getUser = $this->User->find("all", array('conditions' => array('User.type !=' => 1),
                    'order' => array('User.created' => 'desc'), 'limit' => $limit, 'offset' => $offset));

                if (is_array($getUser) && count($getUser) > 0) {

                    $getUser = Set::extract('/User/.', $getUser);
                    foreach ($getUser as &$value) {
                        $user_permissions = json_decode($value['user_permissions']);
                        $value['user_permissions'] = $this->strip_slashes_recursive($user_permissions);
                    }

                    $response_data['admin_data'] = $getUser;
                    $response_data['total_pages'] = "$total_pages";
                    $response_data['date_offset'] = $date_offset;
                    $page = $page + 1;
                    $response_data['current_page'] = "$page";
                }
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }

    function delete_admin_users_v2($api_token, $user_id, $deviceid, $device_type) {

        $customerid = 'skip_user_id';
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, $customerid, 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        if ($user_id == 1) {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['invalid_access'];
            $response['message'] = $GLOBALS["Webservice"]["messages"]['invalid_request'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['invalid_request'];
            return $this->fail_safe_return($response);
        }

        if ($this->User->delete($user_id)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['user_delete'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['user_delete'],
                'data' => array()
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['service_delete_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['service_delete_error'],
                'data' => array()
            );
        }
        return $this->fail_safe_return($response);
    }

    public function get_package_available_time_slots($api_token = '', $appointment_date = '', $customer_id = '', $num_of_slots = '', $back_to_back = '', $deviceid = '', $device_type = '') {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');

        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }
        if ($num_of_slots == 0) {
            $num_of_slots = 1;
        }
        $response_data = array();
        $response = $this->Schedular->find('all');
        //$this->_px($response);
        if (is_array($response) && count($response) > 0) {

            $schedular_id = 0;
            foreach ($response as $key => $value) {
                $start_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $appointment_date);
                $end_date_fall = $this->check_in_range($value['Schedular']['start_date'], $value['Schedular']['end_date'], $appointment_date);
                if ($start_date_fall == true) {
                    $schedular_id = $value['Schedular']['id'];
                    continue;
                }
                if ($end_date_fall == true) {
                    $schedular_id = $value['Schedular']['id'];
                    continue;
                }
            }
        }

        if ($schedular_id == 0) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['no_schedular_set'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_schedular_set'],
                'data' => $response_data
            );
            return $this->fail_safe_return($response);
        }


        $options['conditions'] = array(
            "Schedular.id" => $schedular_id,
        );
        $schedularData = $this->Schedular->find("first", $options);
        //$this->_px($schedularData);
        if (is_array($schedularData) && count($schedularData) > 0) {

            $total_slot_time = $schedularData['Schedular']['travel_time'] + $schedularData['Schedular']['cleaning_time'] + $schedularData['Schedular']['slot_mins'];
            // 3998

            $start_time = date('H:i', strtotime($schedularData['Schedular']['start_time']));
            $endtimes = date('H:i', strtotime($schedularData['Schedular']['end_time']));

            $schedular_id = $schedularData['Schedular']['id'];
            $slot_lunch = $schedularData['Schedular']['slot_lunch'];
            $weekends = $schedularData['Schedular']['weekends'];
            $maximum_slot_in_row = $schedularData['Schedular']['maximum_slot_in_row'];
            for ($i = 0; $i < $schedularData['Schedular']['maximum_appointment']; $i++) {
                $end_time = strtotime("+$total_slot_time minutes", strtotime($start_time));
                $responses[$i]['start_time'] = $start_time;
                $responses[$i]['end_time'] = date('H:i', $end_time);
                $responses[$i]['schedular_id'] = $schedular_id;
                $responses[$i]['appointment_date'] = $appointment_date;
                $responses[$i]['slot_no'] = (string) ($i + 1);
                if ($slot_lunch == ($i + 1)) {
                    $responses[$i]['lunch_slot'] = "1";
                } else {
                    $responses[$i]['lunch_slot'] = "0";
                }
                $dates = strtotime(date('Y-m-d H:i'));
                $start_times = strtotime($appointment_date . ' ' . $start_time);
                if ($start_times >= $dates) {
                    array_push($response_data, $responses[$i]);
                }
                $start_time = date('H:i', $end_time);
            }
        }
        if (is_array($response_data) && count($response_data) > 0) {
            $responsess = array();
            $lastSlotStartime = end($response_data);
            foreach ($response_data as $key => $value) {
                if ($lastSlotStartime['start_time'] > $endtimes) {
                    if ($endtimes <= $value['start_time']) {
                        array_push($responsess, $value);
                    }
                } else {
                    array_push($responsess, $value);
                }
            }
        }
        $response_data = $responsess;
        $appointmentday = date('l', strtotime($appointment_date));

        /*
          $weekend_arrays = explode(',', $weekends);

          if (in_array($this->get_day_no($appointmentday), $weekend_arrays)) {
          $response = array(
          'status' => $GLOBALS["Webservice"]["codes"]['error'],
          'message' => $GLOBALS['Webservice']['messages']['appointment_holiday'],
          'message_ar' => $GLOBALS['Webservice']['messages_ar']['appointment_holiday'],
          'data' => array()
          );
          return $this->fail_safe_return($response);
          } */

        $packageData = $this->Appointment->find('first', array('conditions' => array('Appointment.appointment_date' => $appointment_date, 'Appointment.type' => '1')));

        if (isset($packageData['Appointment']['id']) && $packageData['Appointment']['id'] > 0) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['no_slot_available'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_slot_available'],
                'data' => array()
            );
            return $this->fail_safe_return($response);
        }

        if (is_array($response_data) && count($response_data) > 0) {

            foreach ($response_data as $key => $value) {
                if (!empty($value['slot_no'])) {
                    $options['conditions'] = array(
                        "AppointmentDetail.slot_no" => $value['slot_no'],
                        'AppointmentDetail.appointment_date' => $appointment_date,
                    );
                    $options['fields'] = array(
                        "AppointmentDetail.start_time", "AppointmentDetail.end_time", "AppointmentDetail.customer_id",
                        "AppointmentDetail.appointment_date", "AppointmentDetail.slot_no", "Customer.mobile", "Customer.f_name",
                        "Customer.l_name", "Customer.email", 'AppointmentDetail.id', 'AppointmentDetail.guest_id'
                    );
                    $appointmentData = $this->AppointmentDetail->find("first", $options);
                    $response_data[$key]['available_slot'] = "1";
                    if (isset($appointmentData['AppointmentDetail']['id']) || $response_data[$key]['lunch_slot'] == 1) {
                        unset($response_data[$key]);
                    }
                }
            }

            $response_data = array_values($response_data);


            $avaliale_slot = array();
            if ($num_of_slots > 0 && $back_to_back == 1) {

                $start_time_slot = array();
                if (is_array($response_data) && count($response_data) > 0) {
                    foreach ($response_data as $key => $value) {
                        array_push($start_time_slot, date('H:i', strtotime($value['start_time'])));
                    }
                }

                $maximum_slots = floor($num_of_slots / $schedularData['Schedular']['maximum_slot_in_row']);
                $needed_slot = $num_of_slots + $maximum_slots;
                $total_slots = count($response_data);

                foreach ($response_data as $key => $val) {
                    $expected_slot = $needed_slot;
                    $add_minutes = ($total_slot_time * $needed_slot) . ' minutes';
                    $expected_slot = strtotime($val['start_time'] . "+ $add_minutes");
                    $available_ = array();
                    $start_times = $val['start_time'];
                    for ($i = 0; $i < $needed_slot; $i++) {
                        if ($i == 0) {
                            $time = strtotime($start_times);
                        } else {
                            $time = strtotime("+$total_slot_time minutes", strtotime($start_times));
                        }
                        array_push($available_, date('H:i', $time));
                        $start_times = date('H:i', $time);
                    }
                    if (count(array_diff($available_, $start_time_slot)) == 0) {
                        array_push($avaliale_slot, $response_data[$key]);
                    }
                }
            }
        }
        $bookedAdminSlot = $this->Order->find('all', array('fields' => array('Order.start_time', 'Order.end_time', 'Order.id', 'Order.appointment_date'), 'conditions' => array('Order.appointment_date' => $appointment_date, 'Order.admin_id >' => '0')));
        $bookedSlotTime = array();


        $final_slot = array();

        if (isset($avaliale_slot) && is_array($avaliale_slot) && count($avaliale_slot) > 0) {

            foreach ($avaliale_slot as $key => $val) {

                if (is_array($bookedAdminSlot) && count($bookedAdminSlot) > 0) {
                    foreach ($bookedAdminSlot as $k => $value) {


                        $value['Order']['start_time'] = date('H:i:s', strtotime($value['Order']['start_time']));
                        $value['Order']['end_time'] = date('H:i:s', strtotime($value['Order']['end_time']));
                        $val['start_time'] = date('H:i:s', strtotime("+0 minutes", strtotime($val['start_time'])));
                        $val['end_time'] = date('H:i:s', strtotime("-1 minutes", strtotime($val['end_time'])));

                        $responseStart = $this->check_date_is_within_range($value['Order']['start_time'], $value['Order']['end_time'], $val['start_time']);
                        $responseend = $this->check_date_is_within_range($value['Order']['start_time'], $value['Order']['end_time'], $val['end_time']);
                        if ($val['slot_no'] == 2) {
                            // echo $val['start_time'];
                            // echo $val['end_time'];
                            // var_dump($responseStart);
                            // var_dump($responseend);
                        }

                        if (($value['Order']['start_time'] == $val['start_time']) && ($value['Order']['end_time']) == $val['end_time']) {

                            unset($avaliale_slot[$key]);
                            if (is_array($final_slot) && count($final_slot) > 0)
                                foreach ($final_slot as $index => $data) {
                                    if ($data['slot_no'] == $val['slot_no']) {
                                        unset($final_slot[$index]);
                                    }
                                }
                        } else if ($responseend || $responseStart) {
                            // if(($value['Order']['start_time'] == $val['start_time']) || ($value['Order']['end_time'] == $val['end_time'])
                            //     || ($value['Order']['start_time'] == $val['end_time']) || ($value['Order']['end_time'] == $val['start_time'])) {
                            //    echo "dsf";exit;
                            //      // if(is_array($final_slot) && count($final_slot) > 0) {
                            //      //     foreach ($final_slot as $index => $data) {
                            //      //         if ($data['slot_no'] == $val['slot_no']) {
                            //      //             unset($final_slot[$index]);
                            //      //         } else {
                            //      //              if(isset($avaliale_slot[$key])) {
                            //      //                 array_push($final_slot,$avaliale_slot[$key]);    
                            //      //              }   
                            //      //         }
                            //      //     }   
                            //      // } else {
                            //      //     if(isset($avaliale_slot[$key])) {
                            //      //         array_push($final_slot,$avaliale_slot[$key]);    
                            //      //     }  
                            //      // }
                            //    if(isset($avaliale_slot[$key])) {
                            //              array_push($final_slot,$avaliale_slot[$key]);    
                            //           }  
                            // } else {
                            unset($avaliale_slot[$key]);
                            if (is_array($final_slot) && count($final_slot) > 0)
                                foreach ($final_slot as $index => $data) {
                                    if ($data['slot_no'] == $val['slot_no']) {
                                        unset($final_slot[$index]);
                                    }
                                }
                            //}
                        } else {

                            if (isset($avaliale_slot[$key])) {
                                array_push($final_slot, $avaliale_slot[$key]);
                            }
                        }
                    }
                } else {
                    $final_slot = $avaliale_slot;
                }
            }
        }
        //print_r($slot_array);
        $final_slot = array_unique($final_slot, SORT_REGULAR);
        $final_slot = array_values($final_slot);
        if (!empty($final_slot)) {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $final_slot
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['no_slot_available'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['no_slot_available'],
                'data' => $final_slot
            );
        }

        return $this->fail_safe_return($response);
    }

    private function get_schedular_from_date($date = '') {

        /*
          $getSchedular = $this->Schedular->find('first', array('conditions' => array('Schedular.start_date <=' => $date,'Schedular.end_date >=' => $date), 'order' => 'Schedular.schedular_type desc'));

          $response_data = array();

          if (is_array($getSchedular) && count($getSchedular) > 0) {
          $response_data = $getSchedular;
          }

          return $response_data;
         */


        // Get the day of the week.
        $dayofweek = (int) date('w', strtotime($date));
        #echo $dayofweek;
        if ($dayofweek === 0) {
            $dayofweek = 7;
        }

        // Check if the date is falling between start and end date. 
        $chkrecord = $this->Schedular->find('all', array('conditions' => array('Schedular.start_date <=' => $date, 'Schedular.end_date >=' => $date), 'order' => 'Schedular.schedular_type desc'));
        #pr($chkrecord);die();
        $temp = array();

        // Check if records are available or not        
        if (is_array($chkrecord) && count($chkrecord) > 0) {

            $temp = Set::extract('/Schedular/.', $chkrecord);

            foreach ($temp as $data) {

                $response_data = array();

                //Check if the record is for Holidays, if the record is for holiday then set the record in reponse data.  
                if ((int) $data['schedular_type'] === 3) {

                    $response_data['Schedular'] = $data;
                    #pr($response_data); die('if !');
                } else {

                    //check if the day of week of the date is falling in weekday or weekend and then set the record in reponse data. 
                    $schedularData = $this->Schedular->find('first', array('conditions' => array('Schedular.start_date <=' => $date, 'Schedular.end_date >=' => $date, 'FIND_IN_SET("' . $dayofweek . '",weekends)'), 'order' => 'Schedular.schedular_type desc'));
                    #pr($schedularData);
                    #$getSchedular = Set::extract('/Schedular/.', $schedularData);
                    $response_data = $schedularData;

                    #pr($response_data); die('else !');

                    /* if (is_array($schedularData) && count($schedularData) > 0) {

                      $getSchedular = Set::extract('/Schedular/.', $schedularData);
                      $response_data['Schedular'] = $getSchedular;

                      } */
                }

                return $response_data;

                if (!empty($response_data)) {
                    $response = array(
                        'status' => $GLOBALS["Webservice"]["codes"]['success'],
                        'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                        'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                        'data' => $response_data
                    );
                } else {
                    $response = array(
                        'status' => $GLOBALS["Webservice"]["codes"]['error'],
                        'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                        'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                        'data' => $response_data
                    );
                }

                return $this->fail_safe_return($response);
            }
        } else {

            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }
        return $this->fail_safe_return($response);
    }

    /*
     * Added by Alpesh Trivedi (Derivedweb) on 10 March 2017 Guided by Aditya Bhatt
     * Start of get_schedular_by_date
     */

    private function get_schedular_by_date_old($api_token = '', $deviceid = '', $device_type = '', $date_by_user = '') {

        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');
        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }

        $getSchedular = $this->Schedular->find('first', array('conditions' => array('Schedular.start_date <=' => $date_by_user, 'Schedular.end_date >=' => $date_by_user), 'order' => 'Schedular.schedular_type desc'));

        $response_data = array();

        if (is_array($getSchedular) && count($getSchedular) > 0) {
            $response_data = $getSchedular;
        }

        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        

        return $this->fail_safe_return($response);
    }

    /*
     * Added by Alpesh Trivedi (Derivedweb) on 14 March 2017 Guided by Aditya Bhatt
     * Start of get_schedular_by_date
     */

    public function get_schedular_by_date($api_token = '', $deviceid = '', $device_type = '', $date_by_user = '') {

        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');
        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }
        //Get the day of the week.
        $dayofweek = (int) date('w', strtotime($date_by_user));
        if ($dayofweek === 0) {
            $dayofweek = 7;
        }
        //Check if the date is falling between start and end date. 
        $chkrecord = $this->Schedular->find('all', array('conditions' => array('Schedular.start_date <=' => $date_by_user, 'Schedular.end_date >=' => $date_by_user), 'order' => 'Schedular.schedular_type desc'));

        $temp = array();
        //check if records are available or not        
        if (is_array($chkrecord) && count($chkrecord) > 0) {
            $temp = Set::extract('/Schedular/.', $chkrecord);
            foreach ($temp as $data) {
                $response_data = array();
                //Check if the record is for Holidays, if the record is for holiday then set the record in reponse data.  
                if ((int) $data['schedular_type'] === 3) {
                    $response_data['schedular_data'][] = $data;
                } else {
                    //check if the day of week of the date is falling in weekday or weekend and then set the record in reponse data. 
                    $schedularData = $this->Schedular->find('first', array('conditions' => array('Schedular.start_date <=' => $date_by_user, 'Schedular.end_date >=' => $date_by_user, 'FIND_IN_SET("' . $dayofweek . '",weekends)'), 'order' => 'Schedular.schedular_type desc'));
                    if (is_array($schedularData) && count($schedularData) > 0) {
                        $getSchedular = Set::extract('/Schedular/.', $schedularData);
                        $response_data['schedular_data'] = $getSchedular;
                    }
                }

                if (!empty($response_data)) {
                    $response = array(
                        'status' => $GLOBALS["Webservice"]["codes"]['success'],
                        'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                        'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                        'data' => $response_data
                    );
                } else {
                    $response = array(
                        'status' => $GLOBALS["Webservice"]["codes"]['error'],
                        'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                        'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                        'data' => $response_data
                    );
                }
                return $this->fail_safe_return($response);
            }
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }
        return $this->fail_safe_return($response);
    }
    public function delete_customer($api_token = '', $deviceid = '', $device_type = '',$key='',$value=''){
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');
        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }
        if ($key == '' || !in_array($key,['customer_id','mobile','email'])) {
            $response['status'] = $GLOBALS["Webservice"]["codes"]['invalid_access'];
            $response['message'] = $GLOBALS["Webservice"]["messages"]['invalid_request'];
            $response['message_ar'] = $GLOBALS["Webservice"]["messages_ar"]['invalid_request'];
            return $this->fail_safe_return($response);
        }
        $field=($key=='customer_id')?'id':$key;
        $user_data = $this->Customer->find('first',array('conditions' =>array($field=>$value)));
        $delete_customer = $this->Customer->deleteAll(array($field=>$value));
        $this->CustomerAddress->deleteAll(array('customer_id'=>$user_data['Customer']['id']));
        if($delete_customer){
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['customer_delete'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['customer_delete'],
                'data' => array()
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['customer_delete'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['customer_delete'],
                'data' => array()
            );
        }
        return $this->fail_safe_return($response);
    }


	
	function get_schedular_v3($api_token = '', $schedular_type = '', $deviceid = '', $device_type = '', $start_date = '', $end_date = '', $pagesize = 10, $page = 0) {
        $error_response = $this->common_checks($api_token, $deviceid, $device_type, 'skip_user_id', 'skip_auth_token');
        if ($pagesize === '') {
            $pagesize = 10;
        }
        if (!empty($error_response)) {
            return $this->fail_safe_return($error_response);
        }



        $date_offset = '';
        if (empty($date_offset)) {
            $date_offset = date('Y-m-d H:i:s');
        }

        if ($page > 0) {
            --$page;
        }
        $page = (int) $page;
        $pagesize = (int) $pagesize;
        if ($pagesize <= 0) {
            $pagesize = 1;
        }
        $response_data = array();

        $options = array();

        if (!empty($start_date) && !empty($end_date)) {
            $options['conditions'] = array(
                'Schedular.start_date <=' => "$end_date",
                'Schedular.start_date >=' => "$start_date"
            );
        }

        $total_records = $this->Schedular->find("count", $options);


        if ($total_records > 0) {
            $total_pages = ceil($total_records / $pagesize);

            if ($page > $total_pages) {
                $response_data['customer_data'] = [];
            } else {

                $offset = $page * $pagesize;
                $limit = $pagesize;

                if (!empty($start_date) && !empty($end_date)) {
                    $getSchedular = $this->Schedular->find("all", array('fields' => array('*', 'DATEDIFF(`start_date`, CURDATE()) AS diff'), 'conditions' => array('Schedular.start_date <=' => "$end_date", 'Schedular.start_date >=' => "$start_date"), 'order' => array('Schedular.schedular_type' => 'asc', 'abs(unix_timestamp(start_date) - unix_timestamp(now()))'), 'limit' => $limit, 'offset' => $offset));
                } else {
                    $getSchedular = $this->Schedular->find("all", array('fields' => array('*', 'DATEDIFF(`start_date`, CURDATE()) AS diff'), 'order' => array('Schedular.schedular_type' => 'asc', 'abs(unix_timestamp(start_date) - unix_timestamp(now()))'), 'limit' => $limit, 'offset' => $offset));
                }
				//echo '<pre>'; print_r($getSchedular); exit;
				
                if (is_array($getSchedular) && count($getSchedular) > 0) {
					$i=0;
					foreach($getSchedular as $s_data){
						$schedular_id = $s_data['Schedular']['id'];
						$getSchedularData  = $this->Schedular->findById($schedular_id);
						$schedular_start_time = $getSchedularData['Schedular']['start_time'];
						$schedular_end_time = $getSchedularData['Schedular']['end_time'];
						$getAppointmentData = $this->Order->find("all", array('fields' => array(
                            "Appointment.schedular_id","Order.start_time", "Order.end_time", "Order.customer_id", "Order.is_van_running", 'Order.is_service_running', "Order.id", "Order.appointment_date", 'Order.type', 'Order.appointment_id', 'Order.receipt_num', 'Order.status', 'Customer.f_name', 'Customer.l_name', 'Customer.mobile', 'Customer.email', 'Order.admin_id'), 'conditions' => array('Order.status !=' => '-1','Appointment.schedular_id' => $schedular_id,'or'=>array('Order.start_time <'=>$schedular_start_time,'Order.end_time >'=>$schedular_end_time)), 'order' => array('Order.appointment_date' => 'ASC')));
						
						 if (is_array($getAppointmentData) && count($getAppointmentData) > 0) {
							 $getSchedular[$i]['Schedular']['conflicted']= 'Yes';
						 }
						 else{
							 $getSchedular[$i]['Schedular']['conflicted']= 'No';
						 }
						
						$i++;
					}
					
                    $getSchedular = Set::extract('/Schedular/.', $getSchedular);
                    $response_data['schedular_data'] = $getSchedular;

                    $response_data['total_pages'] = "$total_pages";
                    $response_data['date_offset'] = $date_offset;
                    $page = $page + 1;
                    $response_data['current_page'] = "$page";
                }
            }
        }
        if (!empty($response_data)) {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['success'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_success'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_success'],
                'data' => $response_data
            );
        } else {
            $response = array(
                'status' => $GLOBALS["Webservice"]["codes"]['error'],
                'message' => $GLOBALS['Webservice']['messages']['data_defination_error'],
                'message_ar' => $GLOBALS['Webservice']['messages_ar']['data_defination_error'],
                'data' => $response_data
            );
        }

        return $this->fail_safe_return($response);
    }
}

?>
