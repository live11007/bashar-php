<div class="page-content-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet light portlet-toggler">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-user-md font-green-sharp"></i>
								<span class="caption-subject font-green-sharp bold uppercase"><?php echo $headTitle; ?></span>
								<span class="caption-helper">manage records...</span>
							</div>

							<?php if (in_array("add", $permissions)) {?>
							<div class="actions">
                <a href="javascript:;" data-id="0" title="Add Moderator" class="btn btn-circle btn-default btnEdit" data-type="form" data-url="<?php echo base_url(ADM_URL . 'departments'); ?>">
                  <i class="fa fa-plus"></i>
                  <span class="hidden-480">Add </span>
                </a>
              </div>
              <?php }?>
              
							<div class="clearfix"></div>
							<?php echo $bradcrumb; ?>
						</div>
						<div class="portlet-body">

							<div class="portlet-body portlet-toggler">
							<table class="table table-striped table-bordered table-hover" id="example"></table>
						</div>
						<div class="clearfix"></div>
						</div>
					</div>
					<div class="portlet light portlet-body portlet-toggler pageform" style="display:none;">
					</div>
				<!-- End: life time stats -->
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	oTable = $('#example').dataTable({
		bProcessing: true,
		bServerSide: true,
		responsive: true,
		sAjaxSource: ADMIN_URL +'departments/lists',
		"order": [[0, "asc"]] ,
		fnServerData: function (sSource, aoData, fnCallback){
			$.ajax({
			   dataType: 'json',
			   type: "POST",
			   url: sSource,
			   data: aoData,
			   success: fnCallback
			});
		 },
		 aoColumns: [
		 	{ sName: "erp_dept_code", sTitle : 'ERP Code'},
			{ sName: "dept_name_en", sTitle : 'Name'},
      		{ sName: "dept_image", sTitle : 'Image'},
			{ sName: "dept_order", sTitle : 'Order'},
			{ sName: "eStatus", sTitle : 'Status'},
			{ sName: "operation", sTitle : 'Operation' ,bSortable:false,bSearchable:false}
		],
		fnServerParams: function(aoData){setTitle(aoData, this)},
		fnDrawCallback: function( oSettings ) {
			$('.make-switch').bootstrapSwitch();
			$('.make-switch').bootstrapSwitch('setOnClass', 'success');
			$('.make-switch').bootstrapSwitch('setOffClass', 'danger');
		}
	});
});
</script>