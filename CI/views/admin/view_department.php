<div class="portlet-title">
  <div class="caption"> <i class="fa fa-user-md font-green-sharp"></i> <span class="caption-subject font-green-sharp bold uppercase"><?php echo $headTitle; ?></span> </div>
  <div class="clearfix"></div>
  <?php echo $bradcrumb; ?> </div>
<div class="portlet-body form view">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12 profile-info">
          <h1></h1>
          <table class="table table-bordered table-striped">
            <tbody>
			  <tr>
                <td>Erp Code</td>
                <td><div> <?php echo $departments->erp_dept_code;;?> </div></td>
              </tr>
			  <tr>
                <td>Department name (English) </td>
                <td><div> <?php echo $departments->dept_name_en;;?> </div></td>
              </tr>
              <tr>
                <td>Department name (Arabic) </td>
                <td><div> <?php echo $departments->dept_name_ar;;?> </div></td>
              </tr>
			  <tr>
                <td>Image</td>
                <td><div> <img src="<?php echo checkImage(3,$departments->dept_image); ?>" class="img-responsive" alt="" width="150"/> </div></td>
              </tr>
              <tr>
                <td> Dispaly Order </td>
                <td><div> <?php echo $departments->dept_order;?> </div></td>
              </tr>
              <tr>
                <td> Status </td>
                <td><div> <?php echo ($departments->eStatus == 'y'?'Active':($departments->eStatus=='n'?'In Active':''));?> </div></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
	  <div class="row">
	  <div class="col-md-12 pull-right"><button class="btn default btn-toggler">Back</button></div>
	  </div>
      <!--end row-->
    </div>
  </div>
</div>
