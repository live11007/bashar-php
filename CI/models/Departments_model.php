<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Departments_model extends MY_Model {

	protected $_table_name = 'departments';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_by = "id DESC";
	protected $_fields = '*';

	public function __construct() {
		parent::__construct();
	}

	function get_single_departments($array) {
		$query = parent::get_single($array, $this->_fields);
		return $query;
	}
	
	function get_single_departments_order($array) {
		$query = parent::get_single($array, $this->_fields);
		return $query;
	}

	function update_department($data, $id = NULL) {	
		
		
		$this->_table_name = 'departments';
		$this->db->set($data);
		parent::update($data, $id);
		return $id;
	}
	
	function update_department_order($data, $id = NULL) {
		$query = parent::get_single($array, $this->_fields);
		for($i=0 ; $i < count($query);$i++)
		{
			if( $data['dept_order'] <= $query[$i]->dept_order )
			{
			
				$data_1['id'] = $query[$i]->id; 
				$data_1['dept_order']=$query[$i]->dept_order + 1; 
				$this->db->set($data_1);
				$this->db->where('id',$query[$i]->id);
				$this->db->update('departments',$data_1);
			}
		}
		//print_r($data);
		$this->db->where('id',$id);
		$this->_table_name = 'departments';		
		$this->db->set($data);		
		parent::update($data);
	
		return $id;
	}
	
	function delete_departments($id = NULL){		
		return parent::delete($id);
	}

	function insert_departments($array) {

		$this->_table_name = 'departments';
		$id = parent::insert($array);

		if($id > 0)
		{
			$permission = array();
			if ($array['vUserType'] == 'moderator') {
				$permission = array(
					array(
						'id' => $id,
						'iModuleId' => 8,
						'vPermissions' => 'access,add,edit,delete,status,view',
					)
				);
			}
			if(count($permission))
			{
				$this->_table_name = 'tbl_user_permissions';
				parent::insert_batch($permission);
			}
			
		}	
		return $id;
	}
	
	/*function get_department(){
		return $this->db->select('*')
		->from($this->_table_name.' as d')
		->where('d.eStatus','y')
		->order_by('d.dept_order')
		->get()->result_object();	
	}*/
	
	
	//For Doctor module
	function get_department_doctor(){
		return $this->db->select('*')
		->from($this->_table_name.' as d')
		->where('d.eStatus!=','d')
		->order_by('d.dept_order')
		->get()->result_array();	
	}
	//End
	
	function get_doctors_dept($dept_id){
			
			$query = $this->db->where('doctor_dept_id',$dept_id)
							  ->where('eStatus!=','d')
							  ->order_by('doctor_order')
							  ->get('doctors');
			$res   = $query->result_array(); 
			//echo "<pre>";print_r($res); exit;
			return $res;
	}
	
	
	function get_dept_banner(){
		return $this->db->select('*')
		->from('cms_pages')
		->where('cms_label','department')
		->get()->row();	
	}
		
}