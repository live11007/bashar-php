<?php
defined('BASEPATH') OR exit('No direct script acceess allowed');
class Departments extends Admin_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('Departments_model','departments_model');
	}
	function index() {

		//$id = (int) $this->input->post('id');
		if ($this->data['action'] == 'delete') {

			$postArray['eStatus'] = 'd';
			$check = $this->departments_model->update_departments($postArray, (int) $this->input->post('id'));
			//$check = $this->departments_model->delete_departments((int) $this->input->post('id'));
			if ($this->db->affected_rows() > 0) {
				$this->data['content']['status'] = 200;
				$this->data['content']['message'] = $this->lang->language['succ_rec_deleted'];
			}
			echo json_encode($this->data['content']);
			exit;

		} else if ($this->data['action'] == 'change_status') {

			$postArray['eStatus'] = $this->input->post('value');
			$check = $this->departments_model->update_departments($postArray, (int) $this->input->post('id'));
			if ($this->db->affected_rows() > 0) {
				$this->data['content']['status'] = 200;
				$this->data['content']['message'] = $this->lang->language['succ_status_changed'];
			}
			echo json_encode($this->data['content']);
			exit;
		} else {
			$this->data['headTitle'] = 'Departments';
			$this->data['module'] = 'list_departments';
			$this->data['bradcrumb'] = breadcrumb(array("home" => base_url() . ADM_URL,$this->data['headTitle'] => ""));
			$this->load->view('admin/mainpage', $this->data);
		}
		

	}

	public function dept_lists() {
		$content = $operation = $whereCond = $totalRow = NULL;
		$result = $tmp_rows = $row_data = array();

		$offset = $this->input->post('iDisplayStart') > 0 ? $this->input->post('iDisplayStart') : 0;
		$limit = $this->input->post('iDisplayLength') > 0 ? $this->input->post('iDisplayLength') : 10;
		$sort = $this->input->post('iSortTitle_0') != "" ? $this->input->post('iSortTitle_0') : NULL;
		$order = $this->input->post('sSortDir_0') != "" ? $this->input->post('sSortDir_0') : NULL;
		$chr = $this->input->post('sSearch') != "" ? $this->input->post('sSearch') : NULL;
		$sEcho = $this->input->post('sEcho') > 0 ? $this->input->post('sEcho') : 1;
		$chr = str_replace(array('_', '%'), array('\_', '\%'), $chr);

	
			
		$this->db
			->from('departments as u');
			
		if (isset($chr) && $chr != '') {
			$this->db->group_start()
				->like('u.erp_dept_code', $chr)
				->or_like('u.dept_name_en', $chr)
				->or_like('u.dept_order', $chr)
				->group_end();
		}
		$totalRow = $this->db->count_all_results();

		if (isset($sort)) {
			$sorting = $sort . ' ' . $order;
		} else {
			$sorting = "u.id ASC";
		}

		
		$this->db->select('u.id,u.erp_dept_code,u.dept_name_en,u.dept_name_ar,u.dept_image,u.dept_order,u.eStatus')
			->from('departments as u')
			->where('u.eStatus !=', 'd');
			
		if (isset($chr) && $chr != '') {
			$this->db->group_start()
				->like('u.erp_dept_code', $chr)
				->or_like('u.dept_name_en', $chr)
				->or_like('u.dept_order', $chr)
				->group_end();
		}
		$qrySel = $this->db->order_by($sort, $order)
			->group_by('u.id')
			->limit($limit, $offset)->get()->result();

		$url = base_url() .ADM_URL. 'departments';
		foreach ($qrySel as $fetchRes) {
			$id = $fetchRes->id;
			$status = ($fetchRes->eStatus == 'y') ? "checked" : "";
			
			$operation = '';
			$disabled = '';
			
			
			if (in_array('view', $this->data['permissions'])) {
				$operation .= '<button title="View" data-type="view" data-url="' . $url . '" data-id="' . $id . '" class="btn btn-info btn-xs btnView">View</button>';
			}
			if (in_array('edit', $this->data['permissions'])) {
				$operation .= '<button title="Edit" data-type="form" data-url="' . $url . '" data-id="' . $id . '" class="btn btn-warning btn-xs btnEdit">Edit</button>';
			}
			if (in_array('delete', $this->data['permissions'])) {
				$operation .= '<button title="Delete" data-url="' . $url . '" data-id="' . $id . '" class="btn btn-danger btn-xs btnDelete">Delete</button>';
			}

			if (!in_array('status', $this->data['permissions'])) {
				$disabled = 'disabled=disabled';
			}
			$switch = '<div class="switch-small"><input type="checkbox" data-id="' . $id . '" data-getaction="tbl_slider" data-url="' . $url . '" ' . $status . ' class="make-switch" ' . $disabled . ' data-on-label="<i class=\'fa fa-check\'></i>" data-off-label="<i class=\'fa fa-times\' ></i>" ></div>';

			$dept_image = base_url().'uploads/'.$fetchRes->dept_image;
			$row_data[] = array($fetchRes->erp_dept_code, $fetchRes->dept_name_en, '<img width="100" src="'.$dept_image.'">', $fetchRes->dept_order, $switch, $operation);
		}
		$result["sEcho"] = $sEcho;
		$result["iTotalRecords"] = (int) $totalRow;
		$result["iTotalDisplayRecords"] = (int) $totalRow;
		$result["aaData"] = $row_data;
		echo json_encode($result);
		exit;
	}

	public function dept_view(){
		$id = (int) $this->input->post('id');
		$this->data['departments'] = $this->departments_model->get_single_departments(array('id' => $id));
		if (count($this->data['departments']) > 0) {
			$this->data['headTitle'] = "Departments detail";
			$this->data['content']['status'] = 200;
			$this->data['bradcrumb'] = breadcrumb(array("home" => base_url() . ADM_URL, "Departments" => "javascript:back_portlet();", $this->data['headTitle'] => ""));
			$this->data['content']['html'] = $this->load->view('admin/view_department', $this->data, true);
		}
		else
		{
			$this->data['coetent']['message'] = "Record not found";
		    $this->data['content']['status'] = 404;
		}

		echo json_encode($this->data['content']);
		exit;


		
	}			
	public function form() {
		$id = (int) $this->input->post('id');
		$this->data['departments'] = $this->departments_model->get_single_departments(array('id' => $id));
		if (count($this->data['departments']) <= 0) {
			$this->data['departments'] = get_column('departments');			
		}
		
		$this->data['headTitle'] = ($id > 0 ? 'Edit' : 'Add New') . " Departments";
		$this->data['bradcrumb'] = breadcrumb(array("home" => base_url() . ADM_URL, "Departments" => "javascript:back_portlet();", $this->data['headTitle'] => ""));
		$this->data['content']['html'] = $this->load->view('admin/form_department', $this->data, true);
		$this->data['content']['status'] = 200;
		echo json_encode($this->data['content']);
		exit;

	}
	public function form_departments() {
	
		$this->form_validation->set_rules($this->departments_rules($this->input->post('id')));
		if ($this->form_validation->run() == FALSE) {
			$this->data['content']['message'] = validation_errors();	

		} else {
			$id = (int) $this->input->post('id');
			$postArray = $this->input->post();
			
			if ($id > 0) { // edit record
				$this->data['departments'] = $this->departments_model->get_single_departments(array('id' => $id));
				
				
				// display order		
				$departments_order_new = (int) $this->input->post('dept_order');
				$this->data['departments'] = $this->departments_model->get_single_departments_order(array('dept_order' => $departments_order_new));	
				$exit_order = $this->data['departments']->dept_order;				
				
				if(!empty($this->data['departments'])){
									
						$updated_order = $exit_order ;
						$id = $this->data['departments']->id;
						
						$this->data['departments']->dept_order = $updated_order;
						$postArray['dept_order'] = $this->data['departments']->dept_order;
						$check = $this->departments_model->update_departments_order($postArray,$id);								
						
				} else {
					$postArray['dept_order'] = $departments_order_new;
				}
				//end
				
				
				
				//profile upload
				//English
				$file_upload = parent::saveFile('dept_image', 'uploads/departments/');
				if ($file_upload['file_name'] != "") {
					$postArray['dept_image'] = 'departments/'. $file_upload['file_name'];
					unlink_file(FCPATH . SITE_UPD . $this->data['departments']->dept_image);
				} else if ($file_upload['errors'] != "") {
					$this->data['content']['message'] = strip_tags($this->upload->display_errors());
				}	
				//profile upload end			

				$this->departments_model->update_departments($postArray, $id);
				
				if ($this->db->affected_rows() > 0) {
					$this->data['content']['status'] = 200;
					$this->data['content']['message'] = $this->lang->language["succ_record_update"];
				}
			}
			else // add record
			{		
				// display order		
				$departments_order_new = (int) $this->input->post('dept_order');
				$this->data['departments'] = $this->departments_model->get_single_departments_order(array('dept_order' => $departments_order_new));	
				$exit_order = $this->data['departments']->dept_order;				
				
				if(!empty($this->data['departments'])){
									
						$updated_order = $exit_order ;
						$id = $this->data['departments']->id;
						
						$this->data['departments']->dept_order = $updated_order;
						$postArray['dept_order'] = $this->data['departments']->dept_order;
						$check = $this->departments_model->update_departments_order($postArray,$id);								
						
				} else {
					$postArray['dept_order'] = $departments_order_new;
				}
				//end
				
				
				$id = $this->departments_model->insert_departments($postArray);				
				if($id > 0)
				{
					$postArray = array();
					$postArray['eStatus'] = 'y';
					if ($this->db->affected_rows() > 0) {
						$this->data['content']['status'] = 200;
				    	$this->data['content']['message'] = $this->lang->language["succ_record_insert"];
					}
					
					//profile upload
					//English
					$file_upload = parent::saveFile('dept_image', 'uploads/departments/');
					if ($file_upload['file_name'] != "") {
						$postArray['dept_image'] = 'departments/'. $file_upload['file_name'];						
					} else if ($file_upload['errors'] != "") {
						$this->data['content']['message'] = strip_tags($this->upload->display_errors());
					}
					//profile upload end
					
					$this->departments_model->update_departments($postArray, $id);
					if ($this->db->affected_rows() > 0) {
						$this->data['content']['status'] = 200;
						$this->data['content']['message'] = $this->lang->language["succ_record_update"];
					}
				}
			}
			
			
		}
		echo json_encode($this->data['content']);
		exit;
	}

	protected function departments_rules($id = 0) {
		$rules = array(
			array(
				'field' => 'erp_dept_code',
				'label' => $this->lang->language["erp_dept_code"],
				'rules' => 'required',
			),
			array(
				'field' => 'dept_name_en',
				'label' => $this->lang->language["dept_name_en"],
				'rules' => 'required',
			),
			array(
				'field' => 'dept_name_ar',
				'label' => $this->lang->language["dept_name_ar"],
				'rules' => 'required',
			),
			array(
				'field' => 'dept_desc_en',
				'label' => $this->lang->language["dept_desc_en"],
				'rules' => 'required',
			),
			array(
				'field' => 'dept_desc_ar',
				'label' => $this->lang->language["dept_desc_ar"],
				'rules' => 'required',
			),
			array(
				'field' => 'dept_order',
				'label' => $this->lang->language["dept_order"],
				'rules' => 'required',
			),
		);
		
		return $rules;
	}
}

?>