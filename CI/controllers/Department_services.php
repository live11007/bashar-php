<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Department_Services extends Front_Controller
{
  public function __construct()
    {
      
       parent::__construct();
	   $this->load->model('Departments_model','departments_model'); 
    }
   function index()
   {
    
      $this->data['module']="department_services";
	  $this->data['department'] = $this->departments_model->get_department();
	  $this->data['department_doct'] = $this->departments_model->get_department_doctor();
	  //$this->data['department_banner'] = $this->departments_model->get_dept_banner();	 
	  foreach($this->data['department_doct'] as $key=>$dept){
	  		$this->data['department_doct'][$key]['doctors'] = $this->departments_model->get_doctors_dept($dept['id']);
	  }
	  //echo "<pre>";print_r($this->data['department']);echo "</pre>";exit;
      $this->load->view('front/mainpage',$this->data);
   }
   
}
